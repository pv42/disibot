package disibot.tasks;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class TaskRunner {
    private static final Logger LOG = LoggerFactory.getLogger(TaskRunner.class);
    private final List<Task> tasks = new ArrayList<>();
    private final ReadWriteLock tasksLock = new ReentrantReadWriteLock();
    private final Thread runnerThread;
    private Map<Task,Long> runTimes;
    private boolean shouldStop;

    public TaskRunner() {
        runnerThread = new Thread(this::run);
        runnerThread.setName("TaskRunner");
        LOG.debug("Task runner created");
        runTimes = new HashMap<>();
        // the thread is not started until it has at least one task
    }

    public void addTask(Task task) {
        tasksLock.writeLock().lock();
        tasks.add(task);
        if (tasks.size() == 1) {
            LOG.info("Started task runner");
            runnerThread.start();
        } else {
            runnerThread.interrupt();
        }
        runTimes.put(task, 0L);
        tasksLock.writeLock().unlock();
    }

    public void shutdown() {
        shouldStop = true;
        runnerThread.interrupt();
        LOG.info("Shutting down task runner");
    }

    private void run() {
        sortTasks();
        while (!shouldStop) {
            // execute job if ready
            if (getTimeToNextJob().toMillis() < 100) {
                Task task = getFirstTask();
                try {
                    Runnable runnable = task.getNextJob();
                    LocalDateTime start = LocalDateTime.now();
                    runnable.run();
                    LocalDateTime end = LocalDateTime.now();
                    runTimes.put(task, runTimes.get(task) + Duration.between(start,end).toNanos());
                } catch (Exception e) {
                    LOG.error("Uncaught exception during the execution of " + task.getName() + ":\n" + e.getMessage(), e);
                }
            }
            sortTasks();
            // interruptable wait until next job
            try {
                Duration timeToNextJob = getTimeToNextJob();
                if (!timeToNextJob.isNegative()) {
                    Thread.sleep(timeToNextJob.toMillis());
                }
            } catch (InterruptedException e) {
                LOG.debug("interrupted");
            }
        }
        LOG.info("Task runner stopped gracefully");
    }

    private Duration getTimeToNextJob() {
        return Duration.between(LocalDateTime.now(), getFirstTask().getNextDeadline());
    }

    private void sortTasks() {
        tasksLock.writeLock().lock();
        tasks.sort((o1, o2) -> {
            if (o1.equals(o2)) return 0;
            return o1.getNextDeadline().isAfter(o2.getNextDeadline()) ? 1 : -1;
        });
        tasksLock.writeLock().unlock();
    }

    public List<Task> getTasks() {
        tasksLock.readLock().lock();
        List<Task> taskListCopy = new ArrayList<>(tasks);
        tasksLock.readLock().unlock();
        return taskListCopy;
    }

    public void removeTask(Task task) {
        tasksLock.writeLock().lock();
        tasks.remove(task);
        tasksLock.writeLock().unlock();
    }

    private Task getFirstTask() {
        tasksLock.readLock().lock();
        Task firstTask = tasks.get(0);
        tasksLock.readLock().unlock();
        return firstTask;
    }

    public long getTotalExecutionTime(Task task) {
        return runTimes.get(task);
    }
}
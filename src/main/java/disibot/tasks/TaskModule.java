package disibot.tasks;

import disibot.context.ContextStorage;
import disibot.modules.DisiBotModule;
import disibot.modules.DisiBotModuleInterface;

import javax.annotation.Nonnull;

public class TaskModule extends DisiBotModule {
    private final TaskCommandHandler commandHandler;

    public TaskModule(ContextStorage contextStorage, TaskRunner runner) {
        this.commandHandler = new TaskCommandHandler(contextStorage, runner);
    }

    @Override
    public void load(@Nonnull DisiBotModuleInterface moduleInterface) {
        moduleInterface.addSlashCommandHandler(this, commandHandler);
    }
}

package disibot.tasks;

import disibot.DisiBot;
import disibot.command.NotAServerException;
import disibot.context.ContextStorage;
import disibot.newcommand.SlashCommandHandler;
import disibot.newcommand.SlashCommandUtil;
import disibot.util.DiscordUtil;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.interactions.commands.OptionMapping;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.build.CommandData;
import net.dv8tion.jda.api.interactions.commands.build.SubcommandData;

import java.util.ArrayList;
import java.util.List;

public class TaskCommandHandler implements SlashCommandHandler {
    private final ContextStorage contexts;
    private final TaskRunner runner;

    public TaskCommandHandler(ContextStorage contexts, TaskRunner runner) {
        this.contexts = contexts;
        this.runner = runner;
    }

    @Override
    public void handle(SlashCommandEvent ev) {
        String sub = ev.getSubcommandName();
        if (sub == null) {
            ev.reply(DisiBot.getString("internal_error")).queue();
            return;
        }
        switch (sub) {
            case "list":
                list(ev);
                break;
            case "run":
                try {
                    runTask(ev);
                } catch (NotAServerException e) {
                    ev.reply(DisiBot.getString("server_only_command")).queue();
                }
                break;
            default:
                break;
        }
    }

    @Override
    public CommandData getCommand() {
        List<SubcommandData> subcommands = new ArrayList<>();
        SubcommandData listSubCommand = new SubcommandData("list", "List all tasks");
        SubcommandData runSubCommand = new SubcommandData("run", "runs a singe job of a task");
        runSubCommand.addOption(OptionType.STRING, "taskname", "name of the task to run", true);
        subcommands.add(listSubCommand);
        subcommands.add(runSubCommand);
        CommandData data = new CommandData("task", "Controls task executed by Disibot");
        data.addSubcommands(subcommands);
        return data;
    }

    private void runTask(SlashCommandEvent ev) throws NotAServerException {
        if (!SlashCommandUtil.getServer(ev, contexts).canExecuteAdminCommands(ev.getUser())) {
            ev.reply(DisiBot.getString("permission_denied")).queue();
            return;
        }
        OptionMapping nameOption = ev.getOption("taskname");
        if (nameOption == null) {
            ev.reply(DisiBot.getString("internal_error")).queue();
            return;
        }
        String taskName = nameOption.getAsString();
        Task match = null;
        for (Task t : runner.getTasks()) {
            if (t.getName().equals(taskName)) {
                match = t;
                break;
            }
        }
        if (match == null) {
            ev.reply(String.format(DisiBot.getString("task_no_name_match"), taskName)).queue();
            return;
        }
        ev.reply(String.format(DisiBot.getString("task_execution_single_job"), match.getName())).queue();
        try {
            match.runDetached();
        } catch (Exception e) {
            ev.getChannel().sendMessage(String.format(DisiBot.getString("task_exception_in_task"), match.getName())).queue();
            DiscordUtil.sendException(e, ev.getChannel());
        }
    }

    private void list(SlashCommandEvent ev) {
        StringBuilder response = new StringBuilder(String.format("```%-27s| %-10s|%-12s| %s\n",
                DisiBot.getString("task_task_name"),
                DisiBot.getString("task_job_number"),
                DisiBot.getString("task_execution_time"),
                DisiBot.getString("task_next_execution")));
        for (Task task : runner.getTasks()) {
            response.append(String.format("%-27s| %9d | % 5.5f | %s\n", task.getName(), task.getJobNumber(), (float) runner.getTotalExecutionTime(task) / 1000000000f, task.getNextDeadline().toString()));
        }
        response.append("```");
        ev.reply(response.toString()).queue();
    }
}

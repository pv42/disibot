package disibot.tasks;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.chrono.ChronoPeriod;
import java.time.temporal.TemporalAmount;

public class Task {
    private static final Logger LOG = LoggerFactory.getLogger(Task.class);
    private final LocalDateTime phase;
    private final TemporalAmount period;
    private final Runnable executable;
    private final String name;
    private int instanceNum;

    public Task(String name, TemporalAmount period, LocalDateTime phase, Runnable executable) {
        this.phase = phase;
        this.period = period;
        this.executable = executable;
        this.name = name;
    }

    public Task(String name, TemporalAmount period, Runnable executable ) {
        this.period = period;
        this.executable = executable;
        this.name = name;
        this.phase = LocalDateTime.now();
    }

    public Runnable getNextJob() {
        instanceNum ++;
        return executable;
    }

    public void runDetached() {
        executable.run();
    }

    public LocalDateTime getNextDeadline() {
        return phase.plus(mul(period, instanceNum + 1));
    }

    private static TemporalAmount mul(TemporalAmount t, int n) {
        if(t instanceof Duration) {
            return ((Duration) t).multipliedBy(n);
        } else if(t instanceof Period) {
            return ((Period) t).multipliedBy(n);
        } else if(t instanceof ChronoPeriod) {
            return ((ChronoPeriod) t).multipliedBy(n);
        } else {
            LOG.error("Unknown type of temporal amout: \""+ t.getClass().getName()+"\".");
            return Duration.ofSeconds(0);
        }
    }

    public String getName() {
        return name;
    }

    public int getJobNumber() {
        return instanceNum;
    }
}

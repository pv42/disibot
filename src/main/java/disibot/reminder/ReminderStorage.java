package disibot.reminder;

import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ReminderStorage {
    public static final Logger LOG = LoggerFactory.getLogger(ReminderStorage.class);
    private final JDA jda;
    private ReminderStorageData data;

    public ReminderStorage(JDA jda) {
        this.jda = jda;
        loadFromDisk();
    }

    public void addReminder(Reminder reminder) {
        data.getReminders().add(reminder);
        saveToDisk();
    }

    public List<Reminder> getAndRemoveDueReminders(@Nonnull Date date) {
        List<Reminder> dueReminders = new ArrayList<>();
        for (Reminder reminder : data.getReminders()) {
            if (reminder.getReminderTimestamp().before(date)) {
                dueReminders.add(reminder);
            }
        }
        for (Reminder reminder : dueReminders) {
            data.getReminders().remove(reminder);
        }
        if (dueReminders.size() > 0) saveToDisk();
        return dueReminders;
    }

    public List<Reminder> getReminders() {
        return data.getReminders();
    }

    public List<Reminder> getRemindersByUser(User user) {
        List<Reminder> userReminders = new ArrayList<>();
        for (Reminder reminder : data.getReminders()) {
            if (user.equals(reminder.getUser())) userReminders.add(reminder);
        }
        return userReminders;
    }

    private synchronized void saveToDisk() {
        ReminderStorageData.save("reminders.json", new ReminderStorageData(data), jda);
        LOG.info("Reminders saved to disk");
    }

    private void loadFromDisk() {
        data = ReminderStorageData.load("reminders.json", jda);
        LOG.info("Reminders save file loaded");
    }

    protected void clear() {
        data.getReminders().clear();
        saveToDisk();
    }

}

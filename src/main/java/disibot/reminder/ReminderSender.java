package disibot.reminder;

import disibot.DisiBot;
import net.dv8tion.jda.api.entities.PrivateChannel;
import net.dv8tion.jda.api.entities.User;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;

public class ReminderSender implements Runnable {

    private static final Logger LOG = LoggerFactory.getLogger(ReminderSender.class);
    private ReminderStorage storage;

    public ReminderSender(@NotNull ReminderStorage storage) {
        this.storage = storage;
    }

    @Override
    public void run() {
        for (Reminder reminder : storage.getAndRemoveDueReminders(new Date())) {
            User user = reminder.getUser();
            if (user == null) {
                LOG.error("User in reminder is null");
                continue;
            }
            PrivateChannel channel = user.openPrivateChannel().complete();
            String yourMessageString;

            if (reminder.getMessage() == null){
                yourMessageString = DisiBot.getString("reminder_reminder_message_content");
            } else {
                yourMessageString = String.format(DisiBot.getString("reminder_reminder_message_about_content"), reminder.getMessage());
            }
            channel.sendMessage(String.format(DisiBot.getString("reminder_reminder_message"),yourMessageString,reminder.getCreationTimestamp().toString(), reminder.getCreationPlace())).queue();
        }
    }
}

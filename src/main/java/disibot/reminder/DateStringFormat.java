package disibot.reminder;

import java.text.ParseException;
import java.util.Date;

public interface DateStringFormat {
    Date getDate(String data) throws ParseException;
}

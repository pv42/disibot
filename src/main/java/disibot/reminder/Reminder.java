package disibot.reminder;

import net.dv8tion.jda.api.entities.User;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Date;
import java.util.Objects;

public class Reminder {
    private final Date reminderTimestamp;
    private final Date creationTimestamp;
    private final String creationPlace; //channelName
    private final String message;
    private final User user;

    public Reminder(@NotNull User user, @NotNull Date reminderTimestamp, @NotNull Date creationTimestamp, @NotNull String creationPlace, @Nullable String message) {
        this.user = user;
        this.reminderTimestamp = reminderTimestamp;
        this.creationTimestamp = creationTimestamp;
        this.creationPlace = creationPlace;
        this.message = message;
    }

    public Reminder(@NotNull User user, @NotNull Date reminderTimestamp, @NotNull Date creationTimestamp, @NotNull String creationPlace) {
        this(user,reminderTimestamp, creationTimestamp,creationPlace,null);
    }

    public Date getReminderTimestamp() {
        return reminderTimestamp;
    }

    public String getMessage() {
        return message;
    }

    public User getUser() {
        return user;
    }

    public Date getCreationTimestamp() {
        return creationTimestamp;
    }

    public String getCreationPlace() {
        return creationPlace;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Reminder reminder = (Reminder) o;
        if (!Objects.equals(reminderTimestamp, reminder.reminderTimestamp)) return false;
        if (!Objects.equals(message, reminder.message)) return false;
        if (!Objects.equals(creationTimestamp, reminder.creationTimestamp)) return false;
        if (!Objects.equals(creationPlace, reminder.creationPlace)) return false;
        return Objects.equals(user, reminder.user);
    }

    @Override
    public int hashCode() {
        int result = reminderTimestamp != null ? reminderTimestamp.hashCode() : 0;
        result = 31 * result + (message != null ? message.hashCode() : 0);
        result = 31 * result + (user != null ? user.hashCode() : 0);
        result = 31 * result + (creationPlace != null ? creationPlace.hashCode() : 0);
        result = 31 * result + (creationTimestamp != null ? creationTimestamp.hashCode() : 0);
        return result;
    }
}

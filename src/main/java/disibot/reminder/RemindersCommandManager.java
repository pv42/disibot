package disibot.reminder;

import disibot.newcommand.SlashCommandHandler;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.interactions.commands.build.CommandData;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class RemindersCommandManager implements SlashCommandHandler {
    private static final Logger LOG = LoggerFactory.getLogger(RemindersCommandManager.class);
    private final ReminderStorage storage;

    public RemindersCommandManager(ReminderStorage reminderStorage) {
        this.storage = reminderStorage;
    }

    @Override
    public void handle(@NotNull SlashCommandEvent ev) {
        List<Reminder> reminders = storage.getRemindersByUser(ev.getUser());
        StringBuilder remindersTable = new StringBuilder("You have " + reminders.size() + " reminders");
        if (reminders.size() == 0) {
            remindersTable.append(".");
        } else {
            remindersTable.append(":\n```  #|Date                         |Message    |Type\n");
            int i = 0;
            for (Reminder reminder : reminders) {
                String message = reminder.getMessage();
                message = message == null ? "" : message;
                String type = "private";
                remindersTable.append(String.format("%3d|%s|%-11.11s|%s\n", i, reminder.getReminderTimestamp(), message, type));
                i++;
            }
            remindersTable.append("```");
        }
        ev.reply(remindersTable.toString()).queue();
    }

    @Override
    public CommandData getCommand() {
        return new CommandData("reminders" ,"Lists all your current reminders");
    }
}

package disibot.reminder;

import disibot.DisiBot;
import disibot.newcommand.SlashCommandHandler;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.interactions.commands.OptionMapping;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.build.CommandData;
import net.dv8tion.jda.api.interactions.commands.build.OptionData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.UnknownFormatConversionException;

import static disibot.newcommand.SlashCommandUtil.getChannel;

public class RemindMeCommandHandler implements SlashCommandHandler {
    private static final Logger LOG = LoggerFactory.getLogger(RemindMeCommandHandler.class);
    private final ReminderStorage storage;

    public RemindMeCommandHandler(ReminderStorage storage) {
        this.storage = storage;
    }

    @Override
    public CommandData getCommand() {
        List<OptionData> options = new ArrayList<>();
        OptionData timeOption = new OptionData(OptionType.STRING, "time", "Time when to remind you, supports a lot of formats", true);
        timeOption.setRequired(true);
        OptionData textOption = new OptionData(OptionType.STRING, "text", "Reminders message");
        options.add(timeOption);
        options.add(textOption);
        CommandData data = new CommandData("remindme", "Set up a reminder at a given time");
        data.addOptions(options);
        return data;
    }

    @Override
    public void handle(@Nonnull SlashCommandEvent ev) {
        OptionMapping timeOption = ev.getOption("time");
        OptionMapping textOption = ev.getOption("text");
        if(timeOption == null) {
            ev.reply(DisiBot.getString("internal_error")).queue();
            LOG.warn("missing option for remindme");
            return;
        }
        String dateString = timeOption.getAsString();
        Date date;
        try {
            date = parseTimeString(dateString);
        } catch (ParseException e) {
            ev.reply(String.format(DisiBot.getString("reminder_time_parse_error") , e.getMessage())).queue();
            return;
        } catch (UnknownFormatConversionException e) {
            ev.reply(String.format(DisiBot.getString("reminder_date_format_not_supported"), dateString)).queue();
            return;
        }
        Reminder reminder;
        if (textOption != null) {
            String message = textOption.getAsString();
            reminder = new Reminder(ev.getUser(), date, new Date(), getChannel(ev).getName(), message);
        } else {
            reminder = new Reminder(ev.getUser(), date, new Date(), getChannel(ev).getName());
        }
        storage.addReminder(reminder);
        ev.reply(String.format(DisiBot.getString("reminder_you_will_be_reminded_on"), date)).queue();
    }

    public static Date parseTimeString(String dateString) throws ParseException, UnknownFormatConversionException {
        DateStringParser parser = new DateStringParser();
        Calendar calendarNow = Calendar.getInstance();
        int day = calendarNow.get(Calendar.DAY_OF_MONTH);
        int month = calendarNow.get(Calendar.MONTH);
        int year = calendarNow.get(Calendar.YEAR);
        Calendar tomorrow = Calendar.getInstance();
        tomorrow.add(Calendar.DAY_OF_MONTH, 1);
        parser.addFormat("now", data -> calendarNow.getTime());
        parser.addFormat("tomorrow", data -> tomorrow.getTime());
        parser.addFormat("^([01][0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9]", data -> {
            Date date = new SimpleDateFormat("HH:mm:ss").parse(data);
            return addOneDayIfOld(date, calendarNow, year, month, day);
        });
        parser.addFormat("^[0-9]:[0-5][0-9]:[0-5][0-9]", data -> {
            Date date = new SimpleDateFormat("H:mm:ss").parse(data);
            return addOneDayIfOld(date, calendarNow, year, month, day);
        });
        parser.addFormat("^([01][0-9]|2[0-3]):[0-5][0-9]", data -> {
            Date date = new SimpleDateFormat("HH:mm").parse(data);
            return addOneDayIfOld(date, calendarNow, year, month, day);
        });
        parser.addFormat("^[0-9]:[0-5][0-9]", data -> {
            Date date = new SimpleDateFormat("H:mm").parse(data);
            return addOneDayIfOld(date, calendarNow, year, month, day);
        });
        parser.addFormat("^(0?[1-9]|[12][0-9]|3[01]).(0?[1-9]|1[012]).\\d{4}", data -> new SimpleDateFormat("d.M.yyyy").parse(data));
        parser.addFormat("^(0?[1-9]|[12][0-9]|3[01]).(0?[1-9]|1[012]).", data -> {
            Date date = new SimpleDateFormat("d.M.").parse(data);
            calendarNow.setTime(date);
            calendarNow.set(Calendar.YEAR, year);
            if (calendarNow.before(Calendar.getInstance())) {
                calendarNow.add(Calendar.YEAR, 1);
            }
            return calendarNow.getTime();
        });
        parser.addFormat("^[0-9]+\\s?[dD]ays?", data -> {
            calendarNow.add(Calendar.DAY_OF_MONTH, Integer.parseInt(data.toLowerCase().split("d")[0].replaceAll("\\s", "")));
            return calendarNow.getTime();
        });
        parser.addFormat("^[0-9]+\\s?[mM]in(ute)?s?", data -> {
            calendarNow.add(Calendar.MINUTE, Integer.parseInt(data.toLowerCase().split("m")[0].replaceAll("\\s", "")));
            return calendarNow.getTime();
        });
        parser.addFormat("^[0-9]+\\s?[hH](our)?s?", data -> {
            calendarNow.add(Calendar.HOUR, Integer.parseInt(data.toLowerCase().split("h")[0].replaceAll("\\s", "")));
            return calendarNow.getTime();
        });
        parser.addFormat("^[0-9]+\\s?[wW]eeks?", data -> {
            calendarNow.add(Calendar.WEEK_OF_YEAR, Integer.parseInt(data.toLowerCase().split("week")[0].replaceAll("\\s", "")));
            return calendarNow.getTime();
        });
        parser.addFormat("^[0-9]+\\s?[yY]ears?", data -> {
            calendarNow.add(Calendar.YEAR, Integer.parseInt(data.toLowerCase().split("year")[0].replaceAll("\\s", "")));
            return calendarNow.getTime();
        });
        parser.addFormat("^[0-9]+\\s?[mM]onths?", data -> {
            calendarNow.add(Calendar.MONTH, Integer.parseInt(data.toLowerCase().split("month")[0].replaceAll("\\s", "")));
            return calendarNow.getTime();
        });
        return parser.parseDateString(dateString);
    }

    private static @Nonnull Date addOneDayIfOld(Date date, Calendar now, int year, int month, int day) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(year, month, day);
        if (calendar.before(now)) {
            calendar.add(Calendar.DAY_OF_MONTH, 1);
        }
        return calendar.getTime();
    }

}

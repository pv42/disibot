package disibot.reminder;

import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UnknownFormatConversionException;

public class DateStringParser {
    private final Map<String, DateStringFormat> formats;

    public DateStringParser() {
        formats = new HashMap<>();
    }

    public Date parseDateString(String data) throws ParseException, UnknownFormatConversionException {
        for (String pattern : formats.keySet()) {
            if (data.matches(pattern + "(\\s.*)?")) { //maybe more after space
                return formats.get(pattern).getDate(data);
            }
        }
        throw new UnknownFormatConversionException("No valid date format matches");
    }

    public void addFormat(String pattern, DateStringFormat format) {
        formats.put(pattern, format);
    }
}

package disibot.reminder;

import disibot.context.ContextStorage;
import disibot.modules.DisiBotModuleInterface;
import disibot.modules.DisiBotModule;
import disibot.tasks.Task;
import net.dv8tion.jda.api.JDA;
import org.jetbrains.annotations.NotNull;

import javax.annotation.Nonnull;
import java.time.Duration;

public class ReminderModule extends DisiBotModule {
    private final RemindersCommandManager remindersCommandManager;
    private final RemindMeCommandHandler remindMeCommandHandler;
    private final Task senderTask;
    public ReminderModule(ContextStorage contextStorage, JDA jda) {
        ReminderStorage storage = new ReminderStorage(jda);
        ReminderSender sender = new ReminderSender(storage);
        remindersCommandManager = new RemindersCommandManager(storage);
        remindMeCommandHandler = new RemindMeCommandHandler(storage);
        senderTask = new Task("ReminderSenderTask", Duration.ofMinutes(1), sender);
    }

    @Override
    public void load(@Nonnull @NotNull DisiBotModuleInterface moduleInterface) {
        moduleInterface.addSlashCommandHandler(this, remindersCommandManager);
        moduleInterface.addSlashCommandHandler(this, remindMeCommandHandler);
        moduleInterface.addTask(this, senderTask);
    }
}

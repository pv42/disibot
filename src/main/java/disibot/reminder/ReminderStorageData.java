package disibot.reminder;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.stream.JsonWriter;
import disibot.util.TimestampAdapter;
import disibot.util.UserAdapter;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

class ReminderStorageData {
    private transient static final Logger LOG = LoggerFactory.getLogger(ReminderStorageData.class);
    private final List<Reminder> reminders;

    ReminderStorageData() {
        reminders = new ArrayList<>();
    }

    ReminderStorageData(@Nonnull ReminderStorageData data) {
        reminders = new ArrayList<>(data.reminders);
    }

    public static void save(@Nonnull String filename, @Nonnull ReminderStorageData data, @Nonnull JDA jda) {
        data.checkValidity();
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeHierarchyAdapter(User.class, new UserAdapter(jda));
        builder.registerTypeAdapter(Date.class, new TimestampAdapter());
        Gson gson = builder.create();
        File f = new File(filename);
        FileWriter fw;
        try {
            fw = new FileWriter(f);
            gson.toJson(data, data.getClass(), new JsonWriter(fw));
            fw.close();
        } catch (IOException e) {
            LOG.error("Could not write reminders save file");
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            LOG.error("Error while writing reminder storage:" + e.getMessage());
        }
    }

    public static ReminderStorageData load(String filename, JDA jda) {
        ReminderStorageData data;
        File f = new File(filename);
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeHierarchyAdapter(User.class, new UserAdapter(jda));
        builder.registerTypeAdapter(Date.class, new TimestampAdapter());
        Gson gson = builder.create();
        if (f.exists()) {
            if (!f.isDirectory()) {
                try {
                    FileReader fr = new FileReader(f);
                    data = gson.fromJson(fr, ReminderStorageData.class);
                    fr.close();
                    if (data == null || data.getReminders() == null) data = new ReminderStorageData();
                } catch (IOException e) {
                    LOG.error("Reminders save file could not be read");
                    e.printStackTrace();
                    data = new ReminderStorageData();
                }
            } else {
                throw new IllegalStateException("reminders save file may not be a directory");
            }
        } else {
            data = new ReminderStorageData();
            ReminderStorageData.save(filename, data, jda);
            LOG.info("reminders save file created");
        }
        return data;
    }

    private void checkValidity() throws IllegalStateException {
        if (reminders == null) throw new IllegalStateException("Reminder storage date corrupt: reminders are null");
        for (Reminder reminder : reminders) {
            if (reminder.getUser() == null || reminder.getCreationTimestamp() == null ||
                    reminder.getReminderTimestamp() == null || reminder.getCreationPlace() == null)

                throw new IllegalStateException("Reminder storage date corrupt: reminder invalid");
        }
    }

    List<Reminder> getReminders() {
        return reminders;
    }
}

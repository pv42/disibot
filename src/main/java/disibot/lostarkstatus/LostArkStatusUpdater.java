package disibot.lostarkstatus;

import disibot.tasks.Task;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.Activity;

import java.time.Duration;

public class LostArkStatusUpdater {
    private final static String SERVERNAME = "Beatrice";
    private final JDA jda;

    public LostArkStatusUpdater(JDA jda) {
        this.jda = jda;
    }

    public Task createTask() {
        return new Task("LostArkStatus", Duration.ofMinutes(5), this::updateStatusTask);
    }

    private void updateStatusTask() {
        String status = LostArkApi.getServerStatus(SERVERNAME);
        jda.getPresence().setActivity(Activity.playing(SERVERNAME + " is " + status));
    }
}

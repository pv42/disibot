package disibot.lostarkstatus;

import disibot.modules.DisiBotModule;
import disibot.modules.DisiBotModuleInterface;
import net.dv8tion.jda.api.JDA;
import org.jetbrains.annotations.NotNull;

public class LostArkStatusModule extends DisiBotModule {
    private final LostArkStatusUpdater lostArkStatusUpdater;
    private final LostArkCommandHandler commandHandler;

    public LostArkStatusModule(JDA jda) {
        this.lostArkStatusUpdater = new LostArkStatusUpdater(jda);
        commandHandler = new LostArkCommandHandler();
    }

    @Override
    public void load(@NotNull DisiBotModuleInterface moduleInterface) {
        moduleInterface.addSlashCommandHandler(this, commandHandler);
        moduleInterface.addTask(this, lostArkStatusUpdater.createTask());
    }
}

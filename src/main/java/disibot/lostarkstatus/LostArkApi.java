package disibot.lostarkstatus;

import com.google.gson.Gson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

public class LostArkApi {
    private static final Logger LOG = LoggerFactory.getLogger(LostArkApi.class);
    private static final String API_SERVER = "https://lost-ark-api.vercel.app/";

    private static ApiResponseDto getFromApi(String path) throws IOException {
        URL url = new URL(API_SERVER + path);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        Gson gson = new Gson();
        conn.connect();
        StringBuilder result = new StringBuilder();
        try (BufferedReader reader = new BufferedReader(
                new InputStreamReader(conn.getInputStream()))) {
            for (String line; (line = reader.readLine()) != null; ) {
                result.append(line);
            }
        }
        String responseMsg = result.toString();
        return gson.fromJson(responseMsg, ApiResponseDto.class);
    }

    public static String getServerStatus(String servername) {
        try {
            ApiResponseDto dto = getFromApi("server/" + servername);
            if (dto.status == 200) {
                return dto.data.get(servername);
            }
            LOG.warn("Could not get status: return code " + dto.status);
        } catch (IOException e) {
            LOG.error("Failed to read status", e);
        }
        return "unknown";
    }

    public static String getSteamPlayerCount() {
        try {
            ApiResponseDto dto = getFromApi("steam/playercount");
            if (dto.status == 200) {
                return dto.data.get("player_count");
            }
            LOG.warn("Could not get player count: return code " + dto.status);
        } catch (IOException e) {
            LOG.error("Failed to read status", e);
        }
        return "unknown";
    }

    private static class ApiResponseDto {
        int status;
        Map<String, String> data;
    }
}



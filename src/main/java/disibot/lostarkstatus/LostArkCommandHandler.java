package disibot.lostarkstatus;

import disibot.newcommand.SlashCommandHandler;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.interactions.commands.build.CommandData;
import org.jetbrains.annotations.NotNull;

public class LostArkCommandHandler implements SlashCommandHandler {
    @Override
    public void handle(@NotNull SlashCommandEvent ev) {
        String countStr = LostArkApi.getSteamPlayerCount();
        ev.reply("Currently " + countStr + " are playing LostArk").queue();
    }

    @Override
    public CommandData getCommand() {
        return new CommandData("lostarkcount", "shows LostArk's current Steam player count");
    }
}

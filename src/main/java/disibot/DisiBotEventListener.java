package disibot;

import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.Emote;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageHistory;
import net.dv8tion.jda.api.entities.MessageReaction;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.events.message.react.MessageReactionAddEvent;
import net.dv8tion.jda.api.events.message.react.MessageReactionRemoveEvent;
import net.dv8tion.jda.api.exceptions.InsufficientPermissionException;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import static disibot.util.DiscordUtil.sendException;


public class DisiBotEventListener extends ListenerAdapter {
    private static final Logger LOG = LoggerFactory.getLogger(DisiBotEventListener.class);
    private static final boolean dumpExceptions = true;
    private static final boolean dontScan = true; // true for debug
    private final List<Guild> scannedServers;
    private final UsageStorage usage;
    private JDA jda;

    public DisiBotEventListener(List<Guild> scannedServers, UsageStorage usage) {
        this.scannedServers = scannedServers;
        this.usage = usage;
    }

    public void setJda(JDA jda) {
        this.jda = jda;
    }

    @Override
    public void onMessageReceived(@Nonnull MessageReceivedEvent event) {
        if (jda == null) return;
        try {
            if (event.getChannelType().isGuild()) {
                if (!scannedServers.contains(event.getGuild()) && !dontScan) {
                    CompletableFuture<Message> waitMessage = event.getChannel().sendMessage("Scanning server please wait ...").submit();
                    scanServer(event.getGuild());
                    waitMessage.get().delete().queue();
                }
            }
            Message msg = event.getMessage();
            for (Emote emote : msg.getEmotes()) {
                LOG.info("Emote " + emote.getName() + " used in message.");
                if (event.getChannelType().isGuild())
                    usage.pushEvent(new UsageEvent(event.getGuild(), emote, event.getMessage().getChannel().getName(), OffsetDateTime.now(), event.getMessageIdLong()));
            }
        } catch (Exception e) {
            if (dumpExceptions) sendException(e, event.getChannel());
            e.printStackTrace();
        }
    }

    @Override
    public void onMessageReactionAdd(@Nonnull MessageReactionAddEvent event) {
        if (jda == null) return;
        try {
            if (!event.getReactionEmote().isEmote()) return;
            Emote emote = event.getReactionEmote().getEmote();
            LOG.debug("emote" + emote.getName() + " used");
            usage.pushEvent(new UsageEvent(event.getGuild(), emote, event.getChannel().getName(), OffsetDateTime.now(), event.getMessageIdLong()));
        } catch (Exception e) {
            if (dumpExceptions) sendException(e, event.getChannel());
            e.printStackTrace();
        }
    }

    @Override
    public void onMessageReactionRemove(@Nonnull MessageReactionRemoveEvent event) {
        if (jda == null) return;
        try {
            if (!event.getReactionEmote().isEmote()) return;
            Emote emote = event.getReactionEmote().getEmote();
            usage.removeEvent(new UsageEvent(event.getGuild(), emote, event.getChannel().getName(), OffsetDateTime.now(), event.getMessageIdLong()));
        } catch (Exception e) {
            if (dumpExceptions) sendException(e, event.getChannel());
            e.printStackTrace();
        }
    }

    private void scanServer(Guild guild) {
        scannedServers.add(guild);
        LOG.info("Scanning " + guild.getName());
        for (TextChannel channel : guild.getTextChannels()) {
            scanChannel(channel);
        }
    }

    private void scanChannel(@Nonnull TextChannel channel) {
        MessageHistory history;
        try {
            history = channel.getHistory();
        } catch (InsufficientPermissionException ex) {
            LOG.error("can't scan channel " + channel.getName() + " insufficient permission");
            return;
        }
        List<Message> messages = null;
        do {
            if (messages == null) {
                messages = history.retrievePast(100).complete();
            } else {
                messages = channel.getHistoryBefore(messages.get(99), 100).complete().getRetrievedHistory();
            }
            for (Message message : messages) {
                scanMessage(message);
            }
        } while (messages.size() == 100);

    }

    private void scanMessage(@Nonnull Message msg) {
        for (Emote emote : msg.getEmotes()) {
            usage.pushEvent(new UsageEvent(msg.getGuild(), emote, msg.getChannel().getName(), msg.getTimeCreated(), msg.getIdLong()));
        }
        for (MessageReaction reaction : msg.getReactions()) {
            Emote emote;
            try {
                emote = reaction.getReactionEmote().getEmote();
            } catch (IllegalStateException e) { // none custom emoji
                return;
            }
            usage.pushEvent(new UsageEvent(msg.getGuild(), emote, msg.getChannel().getName(), msg.getTimeCreated(), msg.getIdLong()));
        }
    }
}

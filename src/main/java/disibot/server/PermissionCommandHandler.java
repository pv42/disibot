package disibot.server;

import disibot.DisiBot;
import disibot.command.InsufficientPowerException;
import disibot.command.NotAServerException;
import disibot.context.ContextStorage;
import disibot.newcommand.SlashCommandHandler;
import disibot.newcommand.SlashCommandUtil;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.interactions.commands.OptionMapping;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.build.CommandData;
import net.dv8tion.jda.api.interactions.commands.build.OptionData;
import net.dv8tion.jda.api.interactions.commands.build.SubcommandData;
import org.jetbrains.annotations.NotNull;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.List;

public class PermissionCommandHandler implements SlashCommandHandler {

    private final ContextStorage contexts;

    public PermissionCommandHandler(ContextStorage contexts) {
        this.contexts = contexts;
    }

    private void handleVolatile(@Nonnull SlashCommandEvent ev) throws InsufficientPowerException, NotAServerException {
        String subcommand = ev.getSubcommandName();
        if (subcommand == null) {
            ev.reply(DisiBot.getString("internal_error")).queue();
            return;
        }
        switch (subcommand) {
            case "own":
                int power = SlashCommandUtil.getServer(ev, contexts).getOwnPower(SlashCommandUtil.getSenderAsMember(ev, contexts));
                ev.reply(String.format(DisiBot.getString("permission_own_power"), power)).queue();
                break;
            case "setrole":
                handleSetRole(ev);
                break;
            case "setpermission":
                handleSetPermission(ev);
                break;
            case "list":
                StringBuilder msg = new StringBuilder("Available permissions:\n");
                boolean first = true;
                Server server = SlashCommandUtil.getServer(ev, contexts);
                for (String s : server.getPermissionList()) {
                    if (!first) msg.append(", ");
                    msg.append(s);
                    power = server.getPermissionPower(SlashCommandUtil.getSenderAsMember(ev, contexts), s);
                    msg.append("(").append(power).append(")");
                    first = false;
                }
                ev.reply(msg.toString()).queue();
                break;
            case "see":
                handleSee(ev);
                break;
            default:
                ev.reply(DisiBot.getString("internal_error")).queue();
                break;
        }
    }

    private void handleSee(@Nonnull SlashCommandEvent ev) throws NotAServerException, InsufficientPowerException {
        OptionMapping nameOM = ev.getOption("name");
        if (nameOM == null) {
            ev.reply(DisiBot.getString("internal_error")).queue();
            return;
        }
        String name = nameOM.getAsString();
        Member member = null;
        for (Member m : SlashCommandUtil.getGuild(ev, contexts).getMembers()) {
            if (name.equals(m.getUser().getName())) {
                member = m;
            }
        }
        if (member == null) {
            ev.reply(String.format(DisiBot.getString("permission_member_not_exists"), name)).queue();
            return;
        }
        Server server = SlashCommandUtil.getServer(ev, contexts);
        int power = server.getMemberPower(SlashCommandUtil.getSenderAsMember(ev, contexts), member);
        ev.reply(String.format(DisiBot.getString("permission_others_power_level"), name, power)).queue();

    }

    private void handleSetPermission(@Nonnull SlashCommandEvent ev) throws NotAServerException, InsufficientPowerException {
        OptionMapping permissionNameOM = ev.getOption("permission");
        OptionMapping powerOM = ev.getOption("power");
        if (permissionNameOM == null || powerOM == null) {
            ev.reply(DisiBot.getString("internal_error")).queue();
            return;
        }
        String permissionName = permissionNameOM.getAsString();

        int power;
        try {
            power = (int)powerOM.getAsLong();
            SlashCommandUtil.getServer(ev, contexts).setPermissionsPowerRequirement(SlashCommandUtil.getSenderAsMember(ev, contexts), permissionName, power);
        } catch (NumberFormatException e) {
            ev.reply(DisiBot.getString("not_a_number")).queue();
            return;
        } catch (IllegalArgumentException e) {
            ev.reply(e.getMessage()).queue();
            return;
        }
        ev.reply(String.format(DisiBot.getString("permission_permission_power_changed"), permissionName, power)).queue();
    }

    private void handleSetRole(@Nonnull SlashCommandEvent ev) throws NotAServerException, InsufficientPowerException {
        OptionMapping roleOM = ev.getOption("role");
        OptionMapping powerOM = ev.getOption("power");
        if (roleOM == null ||powerOM == null) {
            ev.reply(DisiBot.getString("internal_error")).queue();
            return;
        }
        Role role = roleOM.getAsRole();
        int power;
        try {
            power = (int)powerOM.getAsLong();
            if (!role.getGuild().equals(SlashCommandUtil.getGuild(ev, contexts))) {
                ev.reply(DisiBot.getString("permission_role_not_found")).queue();
                return;
            }
            SlashCommandUtil.getServer(ev, contexts).setRolePowerLevel(SlashCommandUtil.getSenderAsMember(ev, contexts), role, power);
        } catch (IllegalArgumentException e) {
            ev.reply(DisiBot.getString("permission_setrole_help")).queue();
            return;
        }
        ev.reply(String.format(DisiBot.getString("permission_role_power_changed"), role.getName(), power)).queue();
    }

    @Override
    public void handle(@NotNull SlashCommandEvent ev) {
        try {
            handleVolatile(ev);
        } catch (InsufficientPowerException e) {
            ev.reply(DisiBot.getString("permission_denied")).queue();
        } catch (NotAServerException e) {
            ev.reply(DisiBot.getString("server_only_command")).queue();
        }
    }

    @Override
    public CommandData getCommand() {
        List<OptionData> setPermissionOptions = new ArrayList<>();
        setPermissionOptions.add(new OptionData(OptionType.STRING, "permission", "Permission you want to modify", true));
        setPermissionOptions.add(new OptionData(OptionType.INTEGER, "power", "Permissions power required to have this permission", true));
        SubcommandData setPermissionSubcommandData = new SubcommandData("setpermission", "Change the permission power required for a permission");
        setPermissionSubcommandData.addOptions(setPermissionOptions);
        List<OptionData> setRoleOptions = new ArrayList<>();
        setRoleOptions.add(new OptionData(OptionType.ROLE, "role", "Role to grant permission power", true));
        setRoleOptions.add(new OptionData(OptionType.INTEGER, "power", "Permission power granted by a role", true));
        SubcommandData setRoleSubcommandData = new SubcommandData("setrole", "Change the permission power granted by a role");
        SubcommandData seeCommandData = new SubcommandData("see", "Sees permission power of a person");
        seeCommandData.addOptions(new OptionData(OptionType.STRING, "name", "person to the the permission power of", true));
        setRoleSubcommandData.addOptions(setRoleOptions);
        List<SubcommandData> subcommands = new ArrayList<>(2);
        subcommands.add(setPermissionSubcommandData);
        subcommands.add(setRoleSubcommandData);
        subcommands.add(seeCommandData);
        subcommands.add(new SubcommandData("own", "shows own permission power"));
        subcommands.add(new SubcommandData("list", "shows all available permissions"));
        CommandData commandData = new CommandData("permission", "Manages bot specific permissions");
        commandData.addSubcommands(subcommands);
        return commandData;
    }
}

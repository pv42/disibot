package disibot.server;


import java.util.HashMap;
import java.util.Map;

class ServerSaveData {
    public Map<String,String> soundBoardData;
    HashMap<String, String> keyWordReactions;
    Map<Long, Integer> permissions; // <RoleId,PowerLevel>
    Map<String, String> customCommands;
    int powerEmoteAutoremove;
    int powerSeeOwnPower;
    int powerSeeOtherPower;
    int powerSeePermissionPower;
    int powerEditCustomCommand;
    int powerListCustomCommands;
    int powerUseAsContext;
    int powerPlayAudio;
    int powerEditSoundBoard;
    int powerUseSoundBoard;
    static final int powerChangePermissions = 100; // always max, fixed to owner only
    ServerSaveData() {
        powerEmoteAutoremove = 50;
        powerSeeOwnPower = 0;
        powerSeeOtherPower = 20;
        powerSeePermissionPower = 10;
        powerEditCustomCommand = 30;
        powerListCustomCommands = 40;
        powerUseAsContext = 5;
        powerPlayAudio = 30;
        powerEditSoundBoard = 30;
        powerUseSoundBoard = 25;
        permissions = new HashMap<>();
        customCommands = new HashMap<>();
        keyWordReactions = new HashMap<>();
        soundBoardData = new HashMap<>();
    }
}
package disibot.server;

import disibot.UsageStorage;
import disibot.config.ConfigStorage;
import disibot.context.ContextStorage;
import disibot.modules.DisiBotModule;
import disibot.modules.DisiBotModuleInterface;
import disibot.modules.ModuleFactory;
import disibot.tasks.TaskRunner;
import net.dv8tion.jda.api.JDA;

import java.util.List;

public class PermissionModuleFactory implements ModuleFactory {
    @Override
    public DisiBotModule initiateModule(List<DisiBotModule> activeModules, List<DisiBotModule> loadedModules, ContextStorage contextStorage, DisiBotModuleInterface moduleInterface, JDA jda, ConfigStorage configStorage, UsageStorage usageStorage, TaskRunner taskRunner) {
        return new PermissionModule(contextStorage);
    }
}

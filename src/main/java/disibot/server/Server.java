package disibot.server;

import com.google.gson.Gson;
import com.google.gson.stream.JsonWriter;
import disibot.command.InsufficientPowerException;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.internal.JDAImpl;
import net.dv8tion.jda.internal.entities.GuildImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.BiConsumer;

public class Server {

    static final String PERMISSION_LIST_CUSTOM_COMMANDS = "listcustomcommands";
    static final String PERMISSION_AUTOREMOVE_EMOTE = "autoremove";
    static final String PERMISSION_SEE_OWN_POWER = "seeownpower";
    static final String PERMISSION_SEE_OTHER_POWER = "seeotherpower";
    static final String PERMISSION_SEE_PERMISSION_POWER = "seepermissionpower";
    static final String PERMISSION_EDIT_CUSTOM_COMMANDS = "editcustomcommands";
    static final String PERMISSION_USE_AS_CONTEXT = "useascontext";
    static final String PERMISSION_PLAY_AUDIO = "playaudio";
    private static final Logger LOG = LoggerFactory.getLogger(Server.class);
    private final Guild dv8Handle;
    private final List<Member> elevatedMembers; //sudos
    private ServerSaveData data;

    Server(Guild dv8Handle) {
        this.dv8Handle = dv8Handle;
        loadFromDisk();
        elevatedMembers = new ArrayList<>();
    }

    Server(JDAImpl api, long id) {
        this(new GuildImpl(api, id));
    }

    private int getPower(@Nonnull Member member) {
        if (elevatedMembers.contains(member)) return Integer.MAX_VALUE;
        if (dv8Handle.getOwnerIdLong() == member.getIdLong()) return 100; //owner
        int power = 0;
        for (Role role : member.getRoles()) {
            if (data.permissions.containsKey(role.getIdLong()) && data.permissions.get(role.getIdLong()) > power) {
                power = data.permissions.get(role.getIdLong());
            }
        }
        return power;
    }

    public boolean canChangePermissions(Member member) {
        return getPower(member) >= ServerSaveData.powerChangePermissions; // always 100
    }

    public boolean canAutoremoveEmote(Member member) {
        return getPower(member) >= data.powerEmoteAutoremove;
    }

    public boolean canSeeOwnPower(Member member) {
        return getPower(member) >= data.powerSeeOwnPower;
    }

    public boolean canSeeOtherPower(Member member) {
        return getPower(member) >= data.powerSeeOtherPower;
    }

    public boolean canSeePermissionPower(Member member) {
        return getPower(member) >= data.powerSeePermissionPower;
    }

    public boolean canEditCustomCommands(Member member) {
        return getPower(member) >= data.powerEditCustomCommand;
    }

    private boolean canListCustomCommands(Member source) {
        return getPower(source) >= data.powerListCustomCommands;
    }

    public boolean canUseAsContext(Member member) {
        return getPower(member) >= data.powerUseAsContext;
    }

    public boolean canPlayAudio(Member member) {
        return getPower(member) >= data.powerPlayAudio;
    }

    public boolean canUseSoundboard(Member member) {
        return getPower(member) >= data.powerUseSoundBoard;
    }

    public boolean canEditSoundboard(Member member) {
        return getPower(member) >= data.powerEditSoundBoard;
    }

    /**
     * sets the power a role provides
     *
     * @param source member taking the action
     * @param role   affected role
     * @param power  permission power granted to the role, must be between 0 and 100
     * @throws InsufficientPowerException if the member does not have the permission to change role powers
     * @throws IllegalArgumentException   if the power is not within the range of 0 to 100
     */
    public void setRolePowerLevel(Member source, Role role, Integer power) throws InsufficientPowerException {
        if (!canChangePermissions(source)) {
            throw new InsufficientPowerException("Can't change permission due too low permission power");
        }
        if (power < 0 || power > 100) {
            throw new IllegalArgumentException("Power must be between 0 and 100");
        }
        data.permissions.put(role.getIdLong(), power);
        saveToDisk();
    }

    /**
     * sets the permission power required for a specific permission
     *
     * @param source     member initiating the change, must have the required power to change permissions
     * @param permission permission string e.g. "autoremove"
     * @param power      power required for the permission in the future
     * @throws InsufficientPowerException if the member does not have the permission to change role powers
     * @throws IllegalArgumentException   if the power is not within the range of 0 to 100 or the permission does not exist
     */
    public void setPermissionsPowerRequirement(Member source, String permission, Integer power) throws InsufficientPowerException {
        if (!canChangePermissions(source)) {
            throw new InsufficientPowerException("Can't change permission due too low permission power");
        }
        if (power < 0 || power > 100) {
            throw new IllegalArgumentException("Power must be between 0 and 100");
        }
        switch (permission) {
            case PERMISSION_AUTOREMOVE_EMOTE:
                data.powerEmoteAutoremove = power;
                break;
            case PERMISSION_SEE_OWN_POWER:
                data.powerSeeOwnPower = power;
                break;
            case PERMISSION_SEE_OTHER_POWER:
                data.powerSeeOtherPower = power;
                break;
            case PERMISSION_SEE_PERMISSION_POWER:
                data.powerSeePermissionPower = power;
                break;
            case PERMISSION_EDIT_CUSTOM_COMMANDS:
                data.powerEditCustomCommand = power;
                break;
            case PERMISSION_LIST_CUSTOM_COMMANDS:
                data.powerListCustomCommands = power;
                break;
            case PERMISSION_USE_AS_CONTEXT:
                data.powerUseAsContext = power;
                break;
            case PERMISSION_PLAY_AUDIO:
                data.powerPlayAudio = power;
                break;
            default:
                throw new IllegalArgumentException(permission + " is not a valid permission");
        }
        saveToDisk();
    }

    public int getPermissionPower(Member source, String permission) throws InsufficientPowerException {
        if (!canSeePermissionPower(source))
            throw new InsufficientPowerException("Can't see permission power due too low permission power");
        if (permission.equals(PERMISSION_AUTOREMOVE_EMOTE)) return data.powerEmoteAutoremove;
        if (permission.equals(PERMISSION_SEE_OWN_POWER)) return data.powerSeeOwnPower;
        if (permission.equals(PERMISSION_SEE_OTHER_POWER)) return data.powerSeeOtherPower;
        if (permission.equals(PERMISSION_SEE_PERMISSION_POWER)) return data.powerSeePermissionPower;
        if (permission.equals(PERMISSION_EDIT_CUSTOM_COMMANDS)) return data.powerEditCustomCommand;
        if (permission.equals(PERMISSION_LIST_CUSTOM_COMMANDS)) return data.powerListCustomCommands;
        if (permission.equals(PERMISSION_USE_AS_CONTEXT)) return data.powerUseAsContext;
        if (permission.equals(PERMISSION_PLAY_AUDIO)) return data.powerPlayAudio;
        throw new IllegalArgumentException("Unknown permission");
    }

    /**
     * returns an array of all available permissions
     *
     * @return array of permissions
     */
    public String[] getPermissionList() {
        return new String[]{PERMISSION_AUTOREMOVE_EMOTE, PERMISSION_SEE_OWN_POWER, PERMISSION_SEE_OTHER_POWER,
                PERMISSION_SEE_PERMISSION_POWER, PERMISSION_EDIT_CUSTOM_COMMANDS, PERMISSION_LIST_CUSTOM_COMMANDS, PERMISSION_USE_AS_CONTEXT, PERMISSION_PLAY_AUDIO};
    }


    public int getOwnPower(Member member) throws InsufficientPowerException {
        if (!canSeeOwnPower(member))
            throw new InsufficientPowerException("Can't see own power due too low permission power");
        return getPower(member);
    }

    private void saveToDisk() {
        Gson gson = new Gson();
        File f = new File("Server." + dv8Handle.getIdLong() + ".json");
        try {
            FileWriter fw = new FileWriter(f);
            gson.toJson(data, data.getClass(), new JsonWriter(fw));
            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void loadFromDisk() {
        File f = new File("Server." + dv8Handle.getIdLong() + ".json");
        Gson gson = new Gson();
        if (f.exists()) {
            if (!f.isDirectory()) {
                try {
                    FileReader fr = new FileReader(f);
                    data = gson.fromJson(fr, ServerSaveData.class);
                    fr.close();
                    LOG.info("Server config for " + dv8Handle.getName() + " loaded");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                throw new IllegalStateException("Server configfile may not be a directory");
            }
        } else {
            data = new ServerSaveData();
            try {
                FileWriter fw = new FileWriter(f);
                gson.toJson(data, data.getClass(), new JsonWriter(fw));
                fw.close();
                LOG.info("Server config for " + dv8Handle.getName() + " created");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public int getMemberPower(Member source, Member target) throws InsufficientPowerException {
        if (!canSeeOtherPower(source))
            throw new InsufficientPowerException("Can't see power due too low permission power");
        return getPower(target);
    }

    public void setCustomCommand(Member source, String command, String returnString) throws InsufficientPowerException {
        if (!canEditCustomCommands(source))
            throw new InsufficientPowerException("Can't edit custom command due too low permission power");
        if (returnString.replaceAll("\\s+", "").length() == 0)
            throw new IllegalArgumentException("Command message must be effectively none-empty");
        if (!command.matches("[\\w-]+")) {
            throw new IllegalArgumentException("Illegal command name: Allowed characters are A-Z, a-z, 0-9, _");
        }
        if (command.length() == 0) throw new IllegalArgumentException("Command can't be empty");
        data.customCommands.put(command, returnString);
        saveToDisk();
    }

    public void removeCustomCommand(Member source, String command) throws InsufficientPowerException, IllegalArgumentException {
        if (!canEditCustomCommands(source))
            throw new InsufficientPowerException("Can't remove custom command due too to low permission power");
        if (!data.customCommands.containsKey(command)) throw new IllegalArgumentException("Command does not exist");
        data.customCommands.remove(command);
        saveToDisk();
    }

    public boolean isCustomCommand(String command) {
        return data.customCommands.containsKey(command);
    }

    public String getCustomCommand(String command) {
        return data.customCommands.get(command);
    }

    public void foreachCustomCommand(BiConsumer<? super String, ? super String> consumer) {
        data.customCommands.forEach(consumer);
    }

    public Map<String, String> getKeywordReactionData() {
        return new HashMap<>(data.keyWordReactions);
    }

    public Map<String, String> getSoundboardData() {
        return new HashMap<>(data.soundBoardData);
    }

    public void foreachSoundboardCommand(BiConsumer<? super String, ? super String> consumer) {
        data.soundBoardData.forEach(consumer);
    }

    public void addKeywordReaction(String keyword, String emote) {
        data.keyWordReactions.put(keyword, emote);
        saveToDisk();
    }

    public void removeKeywordReaction(String keyword) {
        data.keyWordReactions.remove(keyword);
        saveToDisk();
    }

    public Set<String> getCustomCommands(Member source) throws InsufficientPowerException {
        if (!canListCustomCommands(source))
            throw new InsufficientPowerException("Can't list commands due to too low permission power");
        return data.customCommands.keySet();
    }

    public void elevate(Member member) {
        elevatedMembers.add(member);
    }

    public void ground(Member member) {
        elevatedMembers.remove(member);
    }

    public boolean isElevated(Member member) {
        return elevatedMembers.contains(member);
    }

    public boolean canExecuteAdminCommands(User user) {
        return user.getIdLong() == 332225171601620992L;
    }

    public void setSoundBoard(Member source, String command, String soundFileName) throws InsufficientPowerException,IllegalArgumentException {
        if (!canEditSoundboard(source))
            throw new InsufficientPowerException("Can't edit custom command due too low permission power");
        if (!command.matches("[\\w-]+")) {
            throw new IllegalArgumentException("Illegal command name: Allowed characters are A-Z, a-z, 0-9, _");
        }
        if (command.length() == 0) throw new IllegalArgumentException("Command can't be empty");
        data.soundBoardData.put(command, soundFileName);
        saveToDisk();
    }

    public void removeSoundBoardCommand(String commandName) throws IllegalArgumentException {
        if(data.keyWordReactions.remove(commandName) == null) {
            throw new IllegalArgumentException("Soundboard command does not exist");
        }
        saveToDisk();
    }
}

package disibot.server;

import disibot.context.ContextStorage;
import disibot.modules.DisiBotModuleInterface;
import disibot.modules.DisiBotModule;
import org.jetbrains.annotations.NotNull;

public class PermissionModule extends DisiBotModule {
    private final ContextStorage contextStorage;

    public PermissionModule(ContextStorage contextStorage) {
        this.contextStorage = contextStorage;
    }

    @Override
    public void load(@NotNull DisiBotModuleInterface moduleInterface) {
        moduleInterface.addSlashCommandHandler(this, new PermissionCommandHandler(contextStorage));
    }
}

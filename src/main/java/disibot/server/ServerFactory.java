package disibot.server;

import net.dv8tion.jda.api.entities.Guild;
import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Map;

public class ServerFactory {
    private static Map<Long, Server> servers = new HashMap<>();
    @NotNull
    public static Server get(@NotNull Guild dv8handle) {
        if(servers.containsKey(dv8handle.getIdLong())) {
            return servers.get(dv8handle.getIdLong());
        } else {
            Server server = new Server(dv8handle);
            servers.put(dv8handle.getIdLong(), server);
            return server;
        }
    }

    // for test only
    public static void reset() {
        servers.clear();
    }
}

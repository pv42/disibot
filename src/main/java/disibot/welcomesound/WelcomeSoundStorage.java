package disibot.welcomesound;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import disibot.util.MemberAdapter;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.Member;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class WelcomeSoundStorage {
    private final static Logger LOG = LoggerFactory.getLogger(WelcomeSoundStorage.class);
    private final static String STORAGE_FILE_NAME = "welcomesoundstorage.json";
    private final Map<Member, String> messages;
    private final Gson gson;

    public WelcomeSoundStorage(JDA jda) {
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeHierarchyAdapter(Member.class, new MemberAdapter(jda));
        builder.enableComplexMapKeySerialization();
        gson = builder.create();
        messages = load();
    }

    public String getWelcomeSoundPath(Member member) {
        return messages.get(member);
    }

    public boolean hasMessage(Member member) {
        return messages.containsKey(member);
    }

    public void putMessage(Member member, String path) {
        messages.put(member, path);
        save();
    }

    public void deleteMessage(Member member) throws IOException {
        String filename = messages.remove(member);
        save();
        Files.delete(Path.of(filename));
    }

    private void save() {
        try {
            FileWriter fw = new FileWriter(STORAGE_FILE_NAME);
            gson.toJson(messages, messages.getClass(), new JsonWriter(fw));
            fw.close();
        } catch (IOException e) {
            LOG.error("Could not write welcome sound storage file", e);
        }
    }

    private Map<Member, String> load() {
         try {
            FileReader fr = new FileReader(STORAGE_FILE_NAME);
            TypeToken<Map<Member, String>> typeToken = new TypeToken<>() {};
            Map<Member, String> map = gson.fromJson(new JsonReader(fr), typeToken.getType());
            if(map == null) {
                LOG.error("Could not read welcome storage");
                return new HashMap<>();
            }
            fr.close();
            return map;
        } catch (FileNotFoundException e) {
            LOG.info("welcome storage does not exist, creating new");
            return new HashMap<>();
        } catch (IOException e) {
             LOG.info("Could not load welcome storage", e);
             return new HashMap<>();
         }
    }

    public String getRandomWelcomeSoundPath() {
        int index = new Random().nextInt(messages.keySet().size());
        return messages.get(new ArrayList<>(messages.keySet()).get(index));
    }
}

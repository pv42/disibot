package disibot.welcomesound;

import disibot.context.ContextStorage;
import disibot.modules.DisiBotModule;
import disibot.modules.DisiBotModuleInterface;
import disibot.util.SoundFilePlayer;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.VoiceChannel;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.IOException;

public class WelcomeSoundModule extends DisiBotModule {
    private static final Logger LOG = LoggerFactory.getLogger(WelcomeSoundModule.class);
    private final ContextStorage contextStorage;
    private final WelcomeSoundStorage welcomeStorage;
    private final JDA jda;

    public WelcomeSoundModule(ContextStorage contextStorage, JDA jda) {
        this.jda = jda;
        this.contextStorage = contextStorage;
        this.welcomeStorage = new WelcomeSoundStorage(jda);

    }

    @Override
    public void load(@NotNull DisiBotModuleInterface moduleInterface) {
        moduleInterface.addCommandHandler(this, new WelcomeCommandHandler(contextStorage, welcomeStorage));
        moduleInterface.addDiscordListenerAdapter(this, new WelcomeSoundListener(welcomeStorage));
        //createUserCommands(moduleInterface); disabled since it does not seem to work
    }

    private void createUserCommands(@NotNull DisiBotModuleInterface moduleInterface) {
        try {
            for (Guild g : jda.getGuilds()) {
                UserCommandCreateActionImpl ucc = new UserCommandCreateActionImpl(g, new UserCommandData("Play welcome sound"));
                moduleInterface.addUserCommandHandler(this, ucc, ev -> {
                    if (!ev.isFromGuild()) {
                        ev.reply("Must be used in a guild").queue();
                        return;
                    }
                    VoiceChannel channel = null;
                    Guild guild = ev.getGuild();
                    assert guild != null;
                    for (VoiceChannel vc : guild.getVoiceChannels()) {
                        if (vc.getMembers().size() > 0) {
                            channel = vc;
                            break;
                        }
                    }

                    if(!welcomeStorage.hasMessage(ev.getMember())) {
                        ev.reply(ev.getMember().getEffectiveName() + " does not have a welcome message.").setEphemeral(true).queue();
                        return;
                    }
                    if (channel == null) {
                        ev.reply("Count not find valid voice channel.").setEphemeral(true).queue();
                        return;
                    }
                    ev.reply("Playing welcome message of " + ev.getMember().getEffectiveName()).setEphemeral(true).queue();
                    try {
                        SoundFilePlayer.playSoundFile(welcomeStorage.getWelcomeSoundPath(ev.getMember()), channel, ev.getGuild());
                    } catch (IOException | UnsupportedAudioFileException e) {
                        LOG.error("Failed playing sound.", e);
                    }
                });
            }
        } catch (Exception e) {
            LOG.warn("User interaction test failed:", e);
        }
    }
}

package disibot.welcomesound;

import net.dv8tion.jda.api.utils.data.DataObject;
import net.dv8tion.jda.api.utils.data.SerializableData;
import net.dv8tion.jda.internal.utils.Checks;

import javax.annotation.Nonnull;

public class UserCommandData implements SerializableData {
    protected String name;

    public UserCommandData(@Nonnull String name) {
        Checks.notEmpty(name, "Name");
        Checks.notLonger(name, 32, "Name");
        //Checks.matches(name, Checks.ALPHANUMERIC_WITH_DASH, "Name");
        //Checks.isLowercase(name, "Name");
        this.name = name;
    }

    /**
     * Configure the name
     *
     * @param name The lowercase alphanumeric (with dash) name, 1-32 characters
     * @return The builder, for chaining
     * @throws IllegalArgumentException If the name is null, not alphanumeric, or not between 1-32 characters
     */
    @Nonnull
    public UserCommandData setName(@Nonnull String name) {
        Checks.notEmpty(name, "Name");
        Checks.notLonger(name, 32, "Name");
        Checks.isLowercase(name, "Name");
        Checks.matches(name, Checks.ALPHANUMERIC_WITH_DASH, "Name");
        this.name = name;
        return this;
    }


    /**
     * The configured name
     *
     * @return The name
     */
    @Nonnull
    public String getName() {
        return name;
    }


    @Nonnull
    @Override
    public DataObject toData() {
        return DataObject.empty()
                .put("name", name)
                .put("type", "2");
    }
}

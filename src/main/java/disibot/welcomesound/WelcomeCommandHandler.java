package disibot.welcomesound;

import disibot.DisiBot;
import disibot.command.CommandHandler;
import disibot.command.NoAttachmentException;
import disibot.command.NotAServerException;
import disibot.context.ContextStorage;
import disibot.util.Command;
import disibot.util.FilePlayerAudioSendHandler;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.Random;

class WelcomeCommandHandler extends CommandHandler {
    private static final Logger LOG = LoggerFactory.getLogger(WelcomeCommandHandler.class);
    private final WelcomeSoundStorage welcomeSoundStorage;
    public WelcomeCommandHandler(ContextStorage contexts, WelcomeSoundStorage welcomeSoundStorage) {
        super(contexts);
        this.welcomeSoundStorage = welcomeSoundStorage;
    }

    @Override
    public boolean canHandleCommand(@NotNull Command command, @NotNull MessageReceivedEvent event) {
        return command.peekString().equals("welcome");
    }

    @Override
    protected void handle(@NotNull Command command) {
        try {
            handleVolatile(command);
        } catch (NotAServerException e) {
            reply(DisiBot.getString("server_only_command"));
        } catch (NoAttachmentException e) {
            reply(DisiBot.getString("sound_no_attachment"));
        }
    }

    private void handleVolatile(@NotNull Command command) throws NotAServerException, NoAttachmentException {
        command.nextString();
        String sub = command.nextString();
        Member member = null;
        if (sub.equals("help") ||sub.equals("")) {
            reply(DisiBot.getString("welcome_usage_help"));
            return;
        } else if(sub.matches("[^#]+#\\d{4}")) {
            member = getGuild().getMemberByTag(sub);
        } else {
            List<Member> matches = getGuild().getMembersByName(sub, true);
            if(matches.size() == 1) {
                member = matches.get(0);
            }
        }
        if(member == null) {
            reply("Could not identify server member '" + sub + "'");
        } else if(command.nextString().equals("remove")){
            try {
                if(!welcomeSoundStorage.hasMessage(member)) {
                    reply(String.format(DisiBot.getString("welcome_remove_no_msg_set"), member.getEffectiveName()));
                    return;
                }
                welcomeSoundStorage.deleteMessage(member);
                reply(String.format(DisiBot.getString("welcome_message_removed"), member.getEffectiveName()));
            } catch (IOException e) {
                reply(DisiBot.getString("internal_error"));
                LOG.error("Could not delete welcome message file." , e);
            }
        } else {
            String filename;
            do {
                filename = "welcomeaudio." + new Random().nextInt();
            } while (Files.exists(Path.of(filename)));
            downloadAttachment(filename);
            if(FilePlayerAudioSendHandler.canPlayFile(filename)) {
                welcomeSoundStorage.putMessage(member, filename);
                reply(String.format(DisiBot.getString("welcome_message_set"), member.getEffectiveName()));
            } else {
                reply(DisiBot.getString("sound_audio_format_not_supported"));
                try {
                    Files.delete(Path.of(filename));
                } catch (IOException e) {
                    LOG.error("Could not delete invalid welcome audio " + filename);
                }
            }
        }
    }
}

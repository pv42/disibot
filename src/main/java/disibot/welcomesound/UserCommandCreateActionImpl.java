package disibot.welcomesound;

import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.interactions.commands.Command;
import net.dv8tion.jda.api.interactions.commands.build.CommandData;
import net.dv8tion.jda.api.interactions.commands.build.OptionData;
import net.dv8tion.jda.api.interactions.commands.build.SubcommandData;
import net.dv8tion.jda.api.interactions.commands.build.SubcommandGroupData;
import net.dv8tion.jda.api.requests.Request;
import net.dv8tion.jda.api.requests.Response;
import net.dv8tion.jda.api.requests.restaction.CommandCreateAction;
import net.dv8tion.jda.api.utils.data.DataObject;
import net.dv8tion.jda.internal.JDAImpl;
import net.dv8tion.jda.internal.requests.RestActionImpl;
import net.dv8tion.jda.internal.requests.Route;
import net.dv8tion.jda.internal.utils.Checks;
import okhttp3.RequestBody;

import javax.annotation.Nonnull;
import java.util.concurrent.TimeUnit;
import java.util.function.BooleanSupplier;

public class UserCommandCreateActionImpl extends RestActionImpl<Command> implements CommandCreateAction {
    private final Guild guild;
    private UserCommandData data;

    public UserCommandCreateActionImpl(JDAImpl api, UserCommandData command) {
        super(api, Route.Interactions.CREATE_COMMAND.compile(api.getSelfUser().getApplicationId()));
        this.guild = null;
        this.data = command;
    }

    public UserCommandCreateActionImpl(Guild guild, UserCommandData command) {
        super(guild.getJDA(), Route.Interactions.CREATE_GUILD_COMMAND.compile(guild.getJDA().getSelfUser().getApplicationId(), guild.getId()));
        this.guild = guild;
        this.data = command;
    }

    @Nonnull
    @Override
    public CommandCreateAction addCheck(@Nonnull BooleanSupplier checks) {
        return (CommandCreateAction) super.addCheck(checks);
    }

    @Nonnull
    @Override
    public CommandCreateAction setCheck(BooleanSupplier checks) {
        return (CommandCreateAction) super.setCheck(checks);
    }

    @Nonnull
    @Override
    public CommandCreateAction deadline(long timestamp) {
        return (CommandCreateAction) super.deadline(timestamp);
    }

    @Nonnull
    @Override
    public CommandCreateAction setDefaultEnabled(boolean enabled) {
        throw new UnsupportedOperationException("Not implemented");
        //return this;
    }

    @Nonnull
    @Override
    public CommandCreateAction timeout(long timeout, @Nonnull TimeUnit unit) {
        return (CommandCreateAction) super.timeout(timeout, unit);
    }

    @Nonnull
    @Override
    public CommandCreateAction setName(@Nonnull String name) {
        Checks.notEmpty(name, "Name");
        Checks.notLonger(name, 32, "Name");
        Checks.matches(name, Checks.ALPHANUMERIC_WITH_DASH, "Name");
        data.setName(name);
        return this;
    }

    @Nonnull
    @Override
    public CommandCreateAction setDescription(@Nonnull String description) {
        throw new UnsupportedOperationException("Descriptions are not allowed on user commands");
    }

    @Nonnull
    @Override
    public CommandCreateAction addOptions(@Nonnull OptionData... options) {
        throw new UnsupportedOperationException("Options are not allowed on user commands");
    }

    @Nonnull
    @Override
    public CommandCreateAction addSubcommands(@Nonnull SubcommandData subcommand) {
        throw new UnsupportedOperationException("Subcommands are not allowed on user commands");
    }

    @Nonnull
    @Override
    public CommandCreateAction addSubcommandGroups(@Nonnull SubcommandGroupData group) {
        throw new UnsupportedOperationException("SubcommandGroups are not allowed on user commands");
    }

    @Override
    public RequestBody finalizeData() {
        DataObject dto = data.toData();
        return getRequestBody(dto);
    }

    @Override
    protected void handleSuccess(Response response, Request<Command> request) {
        DataObject json = response.getObject();
        request.onSuccess(new Command(api, guild, json));
    }

    public String getName() {
        return data.getName();
    }
}


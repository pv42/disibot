package disibot.welcomesound;

import disibot.util.SoundFilePlayer;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.VoiceChannel;
import net.dv8tion.jda.api.events.guild.voice.GuildVoiceJoinEvent;
import net.dv8tion.jda.api.exceptions.InsufficientPermissionException;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.IOException;
import java.time.LocalDate;
import java.time.Month;
import java.util.Date;

public class WelcomeSoundListener extends ListenerAdapter {
    private static final Logger LOG = LoggerFactory.getLogger(WelcomeSoundListener.class);
    private final WelcomeSoundStorage storage;

    public WelcomeSoundListener(WelcomeSoundStorage storage) {
        this.storage = storage;
    }

    @Override
    public void onGuildVoiceJoin(@NotNull GuildVoiceJoinEvent event) {
        VoiceChannel channel = event.getChannelJoined();
        Member member = event.getMember();
        if(storage.hasMessage(member)) {
            try {
                LocalDate date = LocalDate.now();
                String path;
                if(date.getDayOfMonth() == 1 && date.getMonth() == Month.APRIL) {
                    path = storage.getRandomWelcomeSoundPath();
                    LOG.info("selected aprils fools welcome message");
                } else {
                    path = storage.getWelcomeSoundPath(member);
                }
                SoundFilePlayer.playSoundFile(path, channel, channel.getGuild());
            } catch (IOException e) {
                LOG.warn("IOException during the playing of a join sound.", e);
            } catch (UnsupportedAudioFileException e) {
                LOG.warn("UnsupportedAudioFileException during the playing of a join sound.", e);
            } catch (InsufficientPermissionException e) {
                LOG.warn("Voice channel can't be joined to play welcome sound (InsufficientPermission):" + channel.getName());
            }
        }
    }
}

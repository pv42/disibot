package disibot.update;

import disibot.DisiBot;
import disibot.command.NotAServerException;
import disibot.context.ContextStorage;
import disibot.newcommand.SlashCommandHandler;
import disibot.newcommand.SlashCommandUtil;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.interactions.commands.build.CommandData;
import org.jetbrains.annotations.NotNull;

import javax.annotation.Nonnull;

public class RestartCommandHandler implements SlashCommandHandler {
    private ContextStorage contexts;

    public RestartCommandHandler(ContextStorage contexts) {
        this.contexts = contexts;
    }

    @Override
    public void handle(@NotNull SlashCommandEvent ev) {
        try {
            handleVolatile(ev);
        } catch (NotAServerException e) {
            ev.reply(DisiBot.getString("server_only_command")).queue();
        }
    }

    private void handleVolatile(@Nonnull SlashCommandEvent ev) throws NotAServerException {
        if (!SlashCommandUtil.getServer(ev, contexts).canExecuteAdminCommands(ev.getUser())) {
            ev.reply(DisiBot.getString("permission_denied_must_be_root")).queue();
            return;
        }
        ev.deferReply().complete();
        DisiBot.restart(ev.getChannel());
    }

    @Override
    public CommandData getCommand() {
        return new CommandData("restart", "Restarts Disibot, privileged command");

    }
}

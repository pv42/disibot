package disibot.update;

import disibot.DisiBot;
import disibot.command.NotAServerException;
import disibot.context.ContextStorage;
import disibot.newcommand.SlashCommandHandler;
import disibot.newcommand.SlashCommandUtil;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.interactions.commands.OptionMapping;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.build.CommandData;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import static java.net.HttpURLConnection.HTTP_OK;

public class UpdateCommandHandler implements SlashCommandHandler {

    private static final Logger LOG = LoggerFactory.getLogger(UpdateCommandHandler.class);
    private final ContextStorage contexts;

    public UpdateCommandHandler(ContextStorage contexts) {
        this.contexts = contexts;
    }

    private static void unzip(String zipFilePath, String destDir) throws IOException {
        File dir = new File(destDir);
        // create output directory if it doesn't exist
        if (!dir.exists()) {
            if (!dir.mkdirs()) {
                throw new IOException("Could not create target directory");
            }
        }
        FileInputStream fis;
        //buffer for read and write data to file
        byte[] buffer = new byte[1024];
        try {
            fis = new FileInputStream(zipFilePath);
            ZipInputStream zis = new ZipInputStream(fis);
            ZipEntry ze = zis.getNextEntry();
            while (ze != null) {
                String fileName = ze.getName();
                File newFile = new File(destDir + File.separator + fileName);
                LOG.debug("Unzipping " + fileName + " to " + newFile.getAbsolutePath());
                //create directories for sub directories in zip
                if (new File(newFile.getParent()).mkdirs()) {
                    LOG.info("created " + newFile.getParent());
                }
                FileOutputStream fos = new FileOutputStream(newFile);
                int len;
                while ((len = zis.read(buffer)) > 0) {
                    fos.write(buffer, 0, len);
                }
                fos.close();
                //close this ZipEntry
                zis.closeEntry();
                ze = zis.getNextEntry();
            }
            //close last ZipEntry
            zis.closeEntry();
            zis.close();
            fis.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void moveOrOverwrite(Path source, Path target) {
        try {
            if (Files.exists(target) && !Files.isDirectory(target)) {
                Files.delete(target);
            }
            if (Files.isDirectory(target) && Files.isDirectory(source)) {
                Files.list(source).forEach(subSource -> {
                    Path subTarget = target.resolve(subSource.getFileName());
                    moveOrOverwrite(subSource, subTarget);
                });
            } else {
                Files.move(source, target);
            }
        } catch (IOException e) {
            LOG.error("could not move or overwrite " + target + " with " + source);
            e.printStackTrace();
        }
    }

    private static void deleteDir(Path path) throws IOException {
        if (!Files.exists(path)) throw new NoSuchFileException(path.toString());
        if (Files.isDirectory(path)) {
            for (Path p : Files.list(path).collect(Collectors.toSet())) {
                deleteDir(p);
            }
        }
        Files.delete(path);
    }


    private void handleVolatile(@Nonnull SlashCommandEvent ev) throws NotAServerException {
        if (!SlashCommandUtil.getServer(ev, contexts).canExecuteAdminCommands(ev.getUser())) {
            ev.reply(DisiBot.getString("permission_denied_must_be_root")).queue();
            return;
        }
        String updateBranch = "master";
        OptionMapping branchOptionMapping = ev.getOption("branch");
        if(branchOptionMapping != null) {
            updateBranch = branchOptionMapping.getAsString();
        }
        ev.reply(DisiBot.getString("update_updating")).queue();
        update(ev.getChannel(), updateBranch);
        ev.getChannel().sendMessage(DisiBot.getString("update_update_complete")).complete();
        DisiBot.restart(ev.getChannel());
    }

    private void update(MessageChannel ch, String updateBranch) {
        if (downloadUpdate(ch, updateBranch)) {
            unpackUpdate(ch);
            DisiBot.restart(ch);
        }
    }

    private void unpackUpdate(MessageChannel ch) {
        ch.sendMessage(DisiBot.getString("update_installing_update")).queue();
        LOG.info("Installing update");
        try {
            unzip("update.zip", "update");
            moveOrOverwrite(Paths.get("update", "res"), Paths.get("res"));
            moveOrOverwrite(Paths.get("update", "startbot.sh"), Paths.get("startbot.sh"));
            moveOrOverwrite(Paths.get("update", "startbot.bat"), Paths.get("startbot.bat"));
            moveOrOverwrite(Paths.get("update", "build", "libs", "disibot.jar", ""), Paths.get("disibot.jar"));
            Files.delete(Paths.get("update.zip"));
            deleteDir(Paths.get("update"));
        } catch (IOException e) {
            ch.sendMessage(String.format(DisiBot.getString("update_install_ioexception"), e.getMessage())).queue();
            e.printStackTrace();
        }
    }

    private boolean downloadUpdate(MessageChannel ch, String updateBranch) {
        ch.sendMessage(DisiBot.getString("update_downloading_update")).queue();
        LOG.info("Downloading update");
        try {
            URL url = new URL("https://gitlab.com/api/v4/projects/15099881/jobs/artifacts/" + updateBranch + "/download?job=build");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.connect();
            if (connection.getResponseCode() != HTTP_OK) {
                ch.sendMessage(String.format(DisiBot.getString("update_download_failed"), connection.getResponseCode())).queue();
                LOG.error("Could not download update (" + url + "): http returned" + connection.getResponseCode());
                return false;
            }
            int c;
            InputStream in = new BufferedInputStream(connection.getInputStream());
            OutputStream out = new BufferedOutputStream(new FileOutputStream("update.zip"));
            c = in.read();
            while (c != -1) {
                out.write(c);
                c = in.read();
            }
            in.close();
            out.close();
            connection.disconnect();
        } catch (MalformedURLException e) {
            ch.sendMessage(String.format(DisiBot.getString("update_url_parse_error"), e.getMessage())).queue();
            return false;
        } catch (IOException e) {
            ch.sendMessage(DisiBot.getString("update_download_ioexception")).queue();
            return false;
        }
        return true;
    }

    @Override
    public void handle(@NotNull SlashCommandEvent ev) {
        try {
            handleVolatile(ev);
        } catch (NotAServerException e) {
            ev.reply(DisiBot.getString("server_only_command")).queue();
        }
    }

    @Override
    public CommandData getCommand() {
        CommandData cd = new CommandData("update", "Updates Disibot, privileged command");
        cd.addOption(OptionType.STRING, "branch","git branch to update to", false);
        return cd;
    }
}
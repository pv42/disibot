package disibot.update;

import disibot.context.ContextStorage;
import disibot.modules.DisiBotModule;
import disibot.modules.DisiBotModuleInterface;
import org.jetbrains.annotations.NotNull;

import javax.annotation.Nonnull;

public class UpdateModule extends DisiBotModule {
    private final UpdateCommandHandler updateCommandHandler;
    private final RestartCommandHandler restartCommandHandler;

    public UpdateModule(ContextStorage contextStorage) {
        this.updateCommandHandler = new UpdateCommandHandler(contextStorage);
        this.restartCommandHandler = new RestartCommandHandler(contextStorage);
    }

    @Override
    public void load(@Nonnull @NotNull DisiBotModuleInterface moduleInterface) {
        moduleInterface.addSlashCommandHandler(this, updateCommandHandler);
        moduleInterface.addSlashCommandHandler(this, restartCommandHandler);
    }
}

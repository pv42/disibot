package disibot.coinflip;

import disibot.modules.DisiBotModule;
import disibot.modules.DisiBotModuleInterface;
import org.jetbrains.annotations.NotNull;

import javax.annotation.Nonnull;

public class CoinFlipModule extends DisiBotModule {
    private final CoinflipCommandHandler commandHandler;

    public CoinFlipModule() {
        commandHandler = new CoinflipCommandHandler();
    }

    @Override
    public void load(@Nonnull @NotNull DisiBotModuleInterface moduleInterface) {
        moduleInterface.addSlashCommandHandler(this, commandHandler);
    }
}

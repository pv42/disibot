package disibot.coinflip;

import disibot.DisiBot;
import disibot.newcommand.SlashCommandHandler;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.interactions.commands.build.CommandData;

import javax.annotation.Nonnull;
import java.util.Random;

public class CoinflipCommandHandler implements SlashCommandHandler {
    @Override
    public void handle(@Nonnull SlashCommandEvent ev) {
        ev.reply(DisiBot.getString(new Random().nextBoolean() ? "coin_head" : "coin_tail")).queue();
    }

    @Override
    public CommandData getCommand() {
        return new CommandData("coinflip", "Flips a coin");
    }
}

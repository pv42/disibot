package disibot.modules;

import disibot.context.ContextStorage;
import org.jetbrains.annotations.NotNull;

import javax.annotation.Nonnull;
import java.util.List;

public class ModuleManagementModule extends DisiBotModule {
    private final ModuleCommandHandler commandHandler;

    public ModuleManagementModule(List<DisiBotModule> active, List<DisiBotModule> all, ContextStorage contextStorage, DisiBotModuleInterface moduleInterface) {
        this.commandHandler = new ModuleCommandHandler(active, all, contextStorage,  moduleInterface);
    }

    @Override
    public void load(@Nonnull @NotNull DisiBotModuleInterface moduleInterface) {
        moduleInterface.addSlashCommandHandler(this, commandHandler);
    }
}

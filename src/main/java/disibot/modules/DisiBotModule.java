package disibot.modules;

import javax.annotation.Nonnull;

public abstract class DisiBotModule {
    public abstract void load(@Nonnull DisiBotModuleInterface moduleInterface);

    public void unload(@Nonnull DisiBotModuleInterface moduleInterface) {
        moduleInterface.unloadModule(this);
    }

    public String getName() {
        return this.getClass().getSimpleName();
    }
}

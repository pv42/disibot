package disibot.modules;

import disibot.UsageStorage;
import disibot.config.ConfigStorage;
import disibot.context.ContextStorage;
import disibot.tasks.TaskRunner;
import net.dv8tion.jda.api.JDA;

import java.util.List;

public interface ModuleFactory {
    DisiBotModule initiateModule(List<DisiBotModule> activeModules, List<DisiBotModule> loadedModules, ContextStorage contextStorage, DisiBotModuleInterface moduleInterface, JDA jda, ConfigStorage configStorage, UsageStorage usageStorage, TaskRunner taskRunner);
}

package disibot.modules;

import disibot.command.CommandHandler;
import disibot.command.MainCommandHandler;
import disibot.config.ConfigSection;
import disibot.config.ConfigStorage;
import disibot.newcommand.MainSlashCommandManager;
import disibot.newcommand.SlashCommandEventHandler;
import disibot.newcommand.SlashCommandHandler;
import disibot.tasks.Task;
import disibot.tasks.TaskRunner;
import disibot.welcomesound.UserCommandCreateActionImpl;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.dv8tion.jda.api.interactions.commands.Command;
import net.dv8tion.jda.api.interactions.commands.build.CommandData;
import net.dv8tion.jda.api.interactions.commands.build.OptionData;
import net.dv8tion.jda.api.interactions.commands.build.SubcommandData;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DisiBotModuleInterface {
    private static final Logger LOG = LoggerFactory.getLogger(DisiBotModuleInterface.class);
    private final Map<DisiBotModule, List<Task>> tasks = new HashMap<>();
    private final TaskRunner taskRunner;
    private final ConfigStorage storage;
    private final Map<DisiBotModule, List<CommandHandler>> commandHandlers = new HashMap<>();
    private final Map<DisiBotModule, List<ListenerAdapter>> listenerAdapters = new HashMap<>();
    private final MainSlashCommandManager mainSlashCommandManager;
    private final JDA jda;
    private @Nullable
    MainCommandHandler mainCommandHandler;

    public DisiBotModuleInterface(TaskRunner taskRunner, ConfigStorage storage, JDA jda) {
        this.taskRunner = taskRunner;
        this.storage = storage;
        this.jda = jda;
        mainSlashCommandManager = new MainSlashCommandManager(jda);
        addDiscordListenerAdapter(null, mainSlashCommandManager); // todo clean up
    }

    public void addCommandHandler(DisiBotModule module, CommandHandler commandHandler) {
        commandHandlers.putIfAbsent(module, new ArrayList<>());
        commandHandlers.get(module).add(commandHandler);
        if (mainCommandHandler != null) {
            mainCommandHandler.addCommandHandler(commandHandler);
        } else {
            LOG.warn("MCH is null while loading command handlers");
        }
    }

    public void addTask(DisiBotModule module, Task task) {
        tasks.putIfAbsent(module, new ArrayList<>());
        tasks.get(module).add(task);
        taskRunner.addTask(task);
    }

    public void unloadModule(DisiBotModule module) {
        removeTasks(module);
        removeCommandHandlers(module);
        removeSlashCommandHandlers(module);
        removeDiscordListenerAdapters(module);
    }

    private void removeTasks(DisiBotModule module) {
        if (tasks.containsKey(module)) {
            for (Task task : tasks.get(module)) {
                taskRunner.removeTask(task);
            }
            tasks.remove(module);
        }
    }

    private void removeCommandHandlers(DisiBotModule module) {
        if (commandHandlers.containsKey(module)) {
            if (mainCommandHandler != null) {
                for (CommandHandler commandHandler : commandHandlers.get(module)) {
                    mainCommandHandler.removeCommandHandler(commandHandler);
                }
            } else {
                LOG.warn("MCH is null while unloading command handlers");
            }
            commandHandlers.remove(module);
        }
    }

    private void removeSlashCommandHandlers(@Nonnull DisiBotModule module) {
        mainSlashCommandManager.unloadModule(module);
    }

    public void addSlashCommandHandler(@Nonnull DisiBotModule module, @Nonnull SlashCommandHandler commandHandler) {
        mainSlashCommandManager.registerCommand(module, commandHandler.getCommand(), commandHandler::handle);
    }

    public void addUserCommandHandler(@Nonnull DisiBotModule module, @Nonnull UserCommandCreateActionImpl userCreateCmd, SlashCommandEventHandler handler) {
        mainSlashCommandManager.registerUserCommand(module, userCreateCmd, handler);
    }

    public void addGuildSlashCommandHandler(@Nonnull DisiBotModule module, @Nonnull SlashCommandHandler commandHandler, @Nonnull Guild guild) {
        mainSlashCommandManager.registerGuildCommand(module, commandHandler.getCommand(), commandHandler::handle, guild);
    }

    public void removeGuildCommandHandler(@Nonnull DisiBotModule module, @Nonnull Guild guild, String commandName) {
        long id = 0;
        long ownId = guild.getJDA().retrieveApplicationInfo().complete().getIdLong();
        for (Command command : guild.retrieveCommands().complete()) {
            if(command.getApplicationIdLong() == ownId && command.getName().equals(commandName)) {
                id = command.getIdLong();
                break;
            }
        }
        if(id == 0) {
            LOG.error("Failed to remove guild command " + commandName + ": No match found. (AppId:" + ownId + ")");
            return;
        }
        mainSlashCommandManager.unregisterGuildCommand(module, guild, id);
    }

    public <T extends ConfigSection> T getConfig(DisiBotModule module, ConfigSectionCreator<T> creator) {
        if (storage.hasSection(module.getName())) {
            //noinspection unchecked
            return (T) storage.getSection(module.getName());
        } else {
            T section = creator.create();
            storage.createSection(module.getName(), section);
            return section;
        }
    }

    public void addDiscordListenerAdapter(DisiBotModule module, ListenerAdapter adapter) {
        listenerAdapters.putIfAbsent(module, new ArrayList<>());
        listenerAdapters.get(module).add(adapter);
        jda.addEventListener(adapter);
    }

    private void removeDiscordListenerAdapters(DisiBotModule module) {
        if (listenerAdapters.containsKey(module)) {
            for (ListenerAdapter adapter : listenerAdapters.get(module)) {
                jda.addEventListener(adapter);
            }
        }
    }

    public void setMainCommandHandler(@Nullable MainCommandHandler mainCommandHandler) {
        this.mainCommandHandler = mainCommandHandler;
    }

    public interface ConfigSectionCreator<T extends ConfigSection> {
        T create();
    }

}


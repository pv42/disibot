package disibot.modules;

import disibot.DisiBot;
import disibot.command.NotAServerException;
import disibot.context.ContextStorage;
import disibot.newcommand.SlashCommandHandler;
import disibot.newcommand.SlashCommandUtil;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.interactions.commands.OptionMapping;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.build.CommandData;
import net.dv8tion.jda.api.interactions.commands.build.OptionData;
import net.dv8tion.jda.api.interactions.commands.build.SubcommandData;
import org.jetbrains.annotations.NotNull;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.Objects;

import static disibot.newcommand.SlashCommandUtil.getSenderAsMember;
import static disibot.newcommand.SlashCommandUtil.getServer;

public class ModuleCommandHandler implements SlashCommandHandler {
    private final List<DisiBotModule> loaded;
    private final List<DisiBotModule> all;
    private final DisiBotModuleInterface moduleInterface;
    private ContextStorage contexts;

    public ModuleCommandHandler(List<DisiBotModule> loaded, List<DisiBotModule> all, ContextStorage contexts, DisiBotModuleInterface moduleInterface) {
        this.contexts = contexts;
        this.loaded = loaded;
        this.all = all;
        this.moduleInterface = moduleInterface;
    }

    @Override
    public void handle(@NotNull SlashCommandEvent ev) {
        try {
            handleVolatile(ev);
        } catch (NotAServerException e) {
            ev.reply(DisiBot.getString("server_only_command")).queue();
        }
    }

    private void handleVolatile(@Nonnull SlashCommandEvent ev) throws NotAServerException {
        String sub = ev.getSubcommandName();
        OptionMapping nameOM = ev.getOption("name");
        switch (Objects.requireNonNull(sub)) {
            case "list":
                list(ev);
                break;
            case "load":
                if(nameOM == null) {
                    ev.reply("Missing argument").queue();
                    return;
                }
                load(nameOM.getAsString(), ev);
                break;
            case "unload":
                if(nameOM == null) {
                    ev.reply("Missing argument").queue();
                    return;
                }
                unload(nameOM.getAsString(), ev);
                break;
            default:
                ev.reply(DisiBot.getString("internal_error")).queue();
                break;
        }
    }

    private void list(@Nonnull SlashCommandEvent ev) {
        StringBuilder reply = new StringBuilder();
        if (loaded.size() > 0) {
            reply.append(loaded.size()).append(" ").append(DisiBot.getString("modules_loaded")).append("\n");
            for (DisiBotModule module : loaded) {
                reply.append(module.getName()).append('\n');
            }
        }
        if (all.size() > loaded.size()) {
            reply.append(all.size() - loaded.size()).append(" ").append(DisiBot.getString("modules_unloaded")).append("\n");
            for (DisiBotModule module : all) {
                if (!loaded.contains(module)) {
                    reply.append(module.getName()).append('\n');
                }
            }
        }
        if (all.size() == 0) {
            reply.append("No modules found");
        }
        ev.reply(reply.toString()).queue();

    }

    private void load(String moduleName, SlashCommandEvent ev) throws NotAServerException {
        if (ev.getUser().getIdLong() == 332225171601620992L) {
            for (DisiBotModule module : loaded) {
                if (module.getName().equals(moduleName)) {
                    ev.reply(DisiBot.getString("module_already_loaded")).queue();
                    return;
                }
            }
            for (DisiBotModule module : all) {
                if (module.getName().equals(moduleName)) {
                    module.load(moduleInterface);
                    loaded.add(module);
                    ev.reply(String.format(DisiBot.getString("module_loaded"), module.getName())).queue();
                    return;
                }
            }
            ev.reply(String.format(DisiBot.getString("module_name_not_found"), moduleName)).queue();
        } else {
            ev.reply(DisiBot.getString("permission_denied")).queue();
        }
    }

    private void unload(String moduleName, SlashCommandEvent ev) throws NotAServerException {
        if (ev.getUser().getIdLong() == 332225171601620992L) {
            for (DisiBotModule module : loaded) {
                if (module.getName().equals(moduleName)) {
                    module.unload(moduleInterface);
                    loaded.remove(module);
                    ev.reply(String.format(DisiBot.getString("module_unloaded"), module.getName())).queue();
                    return;
                }
            }
            ev.reply(String.format(DisiBot.getString("module_name_not_found"), moduleName)).queue();
        } else {
            ev.reply(DisiBot.getString("permission_denied")).queue();
        }
    }



    @Override
    public CommandData getCommand() {
        SubcommandData unloadSCD = new SubcommandData("unload", "unloads a module");
        unloadSCD.addOption(OptionType.STRING, "name", "name of module to unload", true);
        SubcommandData loadSCD = new SubcommandData("load", "loads a module");
        loadSCD.addOption(OptionType.STRING, "name", "name of module toload", true);
        SubcommandData listSCD = new SubcommandData("list", "lists all modules");
        CommandData cData = new CommandData("module", "Manages DisiBot SubModule, privileged");
        cData.addSubcommands(unloadSCD, loadSCD, listSCD);
        return cData;
    }
}

package disibot;

import disibot.util.Pair;
import net.dv8tion.jda.api.entities.Emote;
import net.dv8tion.jda.api.entities.Guild;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UsageStorage {
    private static final Logger LOG = LoggerFactory.getLogger(UsageStorage.class);
    private Map<Guild, Map<Emote, List<UsageEvent>>> events;

    public UsageStorage() {
        events = new HashMap<>();
    }

    public void pushEvent(UsageEvent ev) {
        if (!events.containsKey(ev.getServer())) {
            events.put(ev.getServer(), new HashMap<>());
            LOG.info("new Server " + ev.getServer() + " added");
        }
        if (!events.get(ev.getServer()).containsKey(ev.getEmote())) {
            events.get(ev.getServer()).put(ev.getEmote(), new ArrayList<>());
        }
        events.get(ev.getServer()).get(ev.getEmote()).add(ev);
    }

    public List<Pair<Integer, Emote>> getTopList(Guild server) {
        List<Pair<Integer, Emote>> topList = new ArrayList<>();
        if (events.get(server) == null) return topList;
        for (Emote emote : events.get(server).keySet()) {
            topList.add(new Pair<>(events.get(server).get(emote).size(), emote));
        }
        topList.sort((emotePair1, emotePair2) -> emotePair2.getKey().compareTo(emotePair1.getKey()));
        return topList;
    }

    public void removeEvent(UsageEvent usageEvent) {
        int i = 0;
        for (UsageEvent event : events.get(usageEvent.getServer()).get(usageEvent.getEmote())) {
            if (event.getChannelName().equals(usageEvent.getChannelName()) && event.getMsgId() == usageEvent.getMsgId()) {
                events.get(usageEvent.getServer()).get(usageEvent.getEmote()).remove(i);
                return;
            }
            i++;
        }
    }

    // gets time since last Usage
    public long getLastUsage(Emote emote, Guild server) {
        OffsetDateTime last = OffsetDateTime.MIN;
        if (events.get(server).get(emote) == null) return Long.MAX_VALUE;
        for (UsageEvent event : events.get(server).get(emote)) {
            if (event.getTime().isAfter(last)) last = event.getTime();
        }
        if (last.equals(OffsetDateTime.MIN)) return Long.MAX_VALUE;
        Duration diff = Duration.between(last, OffsetDateTime.now());
        return diff.getSeconds();
    }

    public String getLastUsageString(Emote emote, Guild server) {
        OffsetDateTime last = OffsetDateTime.MIN;
        if (events.get(server).get(emote) == null) return "never";
        for (UsageEvent event : events.get(server).get(emote)) {
            if (event.getTime().isAfter(last)) last = event.getTime();
        }
        if (last.equals(OffsetDateTime.MIN)) return "never";
        Duration diff = Duration.between(last, OffsetDateTime.now());
        long diffSeconds = diff.getSeconds() % 60;
        long diffMinutes = diff.getSeconds() / 60 % 60;
        long diffHours = diff.getSeconds() / (60 * 60) % 24;
        long diffDays = diff.getSeconds() / (24 * 60 * 60);
        if (diffDays > 0) return diffDays + "days ago";
        if (diffHours > 0) return diffHours + "hours ago";
        if (diffMinutes > 0) return diffMinutes + "minutes ago";
        return diffSeconds + "seconds ago";
    }

    public int getUsageNumber(Emote emote, Guild server) {
        if (events.get(server) == null) return 0;
        if (events.get(server).get(emote) == null) return 0;
        return events.get(server).get(emote).size();
    }

    public int getUsageNumber(Emote emote, Guild server, int lastDays) {
        if (events.get(server) == null) return 0;
        if (events.get(server).get(emote) == null) return 0;
        int c = 0;
        OffsetDateTime now = OffsetDateTime.now();
        for (UsageEvent event : events.get(server).get(emote)) {
            if (Duration.between(event.getTime(), now).compareTo(Duration.ofDays(lastDays)) < 0) c++;
        }
        return c;
    }

    public Map<String, Integer> getUsageByChannel(Emote emote, Guild server) {
        Map<String, Integer> usage = new HashMap<>();
        for (UsageEvent event : events.get(server).get(emote)) {
            if (!usage.containsKey(event.getChannelName())) {
                usage.put(event.getChannelName(), 1);
            } else {
                usage.put(event.getChannelName(), usage.get(event.getChannelName()) + 1);
            }
        }
        return usage;
    }
}

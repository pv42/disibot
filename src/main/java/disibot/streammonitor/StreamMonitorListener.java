package disibot.streammonitor;

import net.dv8tion.jda.api.entities.Activity;
import net.dv8tion.jda.api.events.guild.voice.GuildVoiceStreamEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Random;

public class StreamMonitorListener extends ListenerAdapter {
    private static final Logger LOG = LoggerFactory.getLogger(StreamMonitorListener.class);
    private final Random rng;
    public StreamMonitorListener() {
        super();
        rng = new Random();
    }

    @Override
    public void onGuildVoiceStream(@NotNull GuildVoiceStreamEvent event) {
        double rn = rng.nextDouble();
        if (event.isStream()) {
            LOG.info("Stream from " + event.getMember().getEffectiveName() + " started");
            if (rn < 0.001) {
                event.getMember().getUser().openPrivateChannel().complete().sendMessage("★★★★★ Stream!").queue();
            } else if (rn < 0.01) {
                event.getMember().getUser().openPrivateChannel().complete().sendMessage("★★★★ stream").queue();
            } else if (rn < 0.02) {
                event.getMember().getUser().openPrivateChannel().complete().sendMessage("Poggers stream").queue();
            } else if (rng.nextDouble() < 0.1) {
                event.getMember().getUser().openPrivateChannel().complete().sendMessage("Noice stream").queue();
            }
        } else {
            LOG.info("Stream from " + event.getMember().getEffectiveName() + " ended");
            if (rn < 0.001) {
                event.getMember().getUser().openPrivateChannel().complete().sendMessage("https://imgur.com/a/5HQJqtU").queue();
            } else if (rn < 0.01) {
                event.getMember().getUser().openPrivateChannel().complete().sendMessage("F\nhttps://memegenerator.net/img/instances/58287492.jpg\n(rip this link)").queue();
            } else if (rn < 0.02) {
                event.getMember().getUser().openPrivateChannel().complete().sendMessage("Rip stream\nhttps://preview.redd.it/gaexlc3xvk461.png?width=446&format=png&auto=webp&s=b4bfc1ec1d9bb8930732cc2dd0584c8fc150e939").queue();
            } else if (rng.nextDouble() < 0.1) {
                event.getMember().getUser().openPrivateChannel().complete().sendMessage("RIP stream").queue();
            }
        }
        if (event.getMember().getUser().getName().equals("pv42")) {
            for (Activity activity : event.getMember().getActivities()) {
                if (activity.getTimestamps() != null)
                    // ac:IntelliJ IDEA Ultimate t:DEFAULT ts:RichPresenceTimestamp(1637700253326-0) -> play ... with start ts
                    event.getMember().getUser().openPrivateChannel().complete().sendMessage("ac:" + activity.getName() + " t:" + activity.getType().name()).queue();
                else
                    event.getMember().getUser().openPrivateChannel().complete().sendMessage("ac:" + activity.getName() + " t:" + activity.getType().name()).queue();
            }
        }
    }
}

package disibot.streammonitor;

import disibot.modules.DisiBotModule;
import disibot.modules.DisiBotModuleInterface;
import org.jetbrains.annotations.NotNull;

public class StreamMonitorModule extends DisiBotModule {
    private StreamMonitorListener streamMonitorListener = new StreamMonitorListener();
    @Override
    public void load(@NotNull DisiBotModuleInterface moduleInterface) {
        moduleInterface.addDiscordListenerAdapter(this, streamMonitorListener);
    }
}

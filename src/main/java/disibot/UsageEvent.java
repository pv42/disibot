package disibot;

import net.dv8tion.jda.api.entities.Emote;
import net.dv8tion.jda.api.entities.Guild;

import java.time.OffsetDateTime;

public class UsageEvent {
    private Guild server;
    private String channelName;
    private OffsetDateTime time;
    private long msgId;
    private Emote emote;

    public UsageEvent(Guild server, Emote emote, String channelName, OffsetDateTime time, long msgId) {
        this.server = server;
        this.emote = emote;
        this.channelName = channelName;
        this.time = time;
        this.msgId = msgId;
    }

    public Emote getEmote() {
        return emote;
    }

    public String getChannelName() {
        return channelName;
    }

    public OffsetDateTime getTime() {
        return time;
    }

    public long getMsgId() {
        return msgId;
    }

    public Guild getServer() {
        return server;
    }
}

package disibot.soundboard;

import disibot.context.ContextStorage;
import disibot.modules.DisiBotModule;
import disibot.modules.DisiBotModuleInterface;
import disibot.server.ServerFactory;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.Guild;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SoundBoardModule extends DisiBotModule {
    private static final Logger LOG = LoggerFactory.getLogger(SoundBoardModule.class);
    private SoundBoardMainCommandHandler mainCommandHandler;
    private DisiBotModuleInterface moduleInterface;
    private JDA jda;

    public SoundBoardModule(JDA jda, ContextStorage contextStorage) {
        mainCommandHandler = new SoundBoardMainCommandHandler(contextStorage, this);
        this.jda = jda;
    }

    @Override
    public void load(@NotNull DisiBotModuleInterface moduleInterface) {
        moduleInterface.addCommandHandler(this, mainCommandHandler);
        this.moduleInterface = moduleInterface;
        // custom slash commands
        LOG.info("Loading soundboard commands for " + jda.getGuilds().size() + " guilds.");
        for (Guild guild : jda.getGuilds()) {
            ServerFactory.get(guild).foreachSoundboardCommand((name, response) ->
                    addCommand(name, response, guild));
        }
    }

    public void addCommand(String command, String soundfilename, Guild guild) {
        moduleInterface.addGuildSlashCommandHandler(this, new SoundBoardCommandHandler(command, soundfilename), guild);
    }

    public void removeCommand(Guild guild, String commandName) {
        if(moduleInterface != null)
            moduleInterface.removeGuildCommandHandler(this, guild, commandName);
        else
            LOG.warn("Can't remove command while module is not loaded");
    }
}

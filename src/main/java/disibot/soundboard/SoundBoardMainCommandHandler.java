package disibot.soundboard;

import disibot.DisiBot;
import disibot.command.CommandHandler;
import disibot.command.InsufficientPowerException;
import disibot.command.NoAttachmentException;
import disibot.command.NotAServerException;
import disibot.context.ContextStorage;
import disibot.util.Command;
import disibot.util.FilePlayerAudioSendHandler;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Random;

public class SoundBoardMainCommandHandler extends CommandHandler {
    private static final Logger LOG = LoggerFactory.getLogger(SoundBoardMainCommandHandler.class);
    private SoundBoardModule module;
    public SoundBoardMainCommandHandler(ContextStorage contexts, SoundBoardModule module) {
        super(contexts);
        this.module = module;
    }

    @Override
    public boolean canHandleCommand(@NotNull Command command, @NotNull MessageReceivedEvent event) {
        return command.peekString().equals("soundboard");
    }

    @Override
    protected void handle(@NotNull Command command) {
        try {
            handleVolatile(command);
        } catch (NotAServerException e) {
            reply(DisiBot.getString("server_only_command"));
        } catch (InsufficientPowerException e) {
            reply(DisiBot.getString("permission_denied"));
        }
    }

    protected void handleVolatile(@NotNull Command command) throws NotAServerException, InsufficientPowerException {
        command.nextString(); // consume soundboard
        switch (command.nextString()) {
            case "set":
                handleSet(command);
                break;
            case "remove":
                handleRemove(command);
                break;
            default:
                reply("Usage:\n" +
                        "!soundboard set <command>     sets a soundboard command to the attached soundfile (mp3)\n" +
                        "!soundboard remove <command>  removes a soundboard command");
        }
    }

    private void handleSet(@NotNull Command command) throws NotAServerException, InsufficientPowerException {
        if (!command.hasNext()) {
            reply("Usage:\n" +
                    "!custom set <command>     sets a soundboard command to the attached soundfile (mp3)");
            return;
        }
        String filename = "soundboard." + new Random().nextInt() + ".mp3";
        String commandName =  command.nextString();
        try {
            downloadAttachment(filename);
            if(FilePlayerAudioSendHandler.canPlayFile(filename)) {
                getServer().setSoundBoard(getSenderAsMember(),commandName, filename);
                module.addCommand(commandName, filename, getGuild());
                reply("Soundboard command  \"/" + commandName+ "\" added");
            } else {
                reply(DisiBot.getString("sound_audio_format_not_supported"));
                try {
                    Files.delete(Path.of(getServer().getSoundboardData().get(commandName)));
                } catch (IOException ioe) {
                    LOG.error("Could not delete sound board commands file after failed set:" + filename, ioe);
                }
            }

        } catch (IllegalArgumentException e) {
            reply(String.format(DisiBot.getString("custom_command_set_error"), e.getMessage()));
            try {
                Files.delete(Path.of(getServer().getSoundboardData().get(commandName)));
            } catch (IOException ioe) {
                LOG.error("Could not delete sound board commands file after failed set:" + filename, ioe);
            }
        } catch (NoAttachmentException e) {
            reply(DisiBot.getString("sound_no_attachment"));
        }
    }

    private void handleRemove(@NotNull Command command) throws NotAServerException {
        if (!command.hasNext()) {
            reply("Usage:\n" +
                    "!cusomt remove <command>  removes a soundboard command");
            return;
        }
        String commandName =  command.nextString();
        String fileName = getServer().getSoundboardData().get(commandName);
        try {
            getServer().removeSoundBoardCommand(command.nextString());
            module.removeCommand(getGuild(), commandName);
            reply("Soundboard command  \"" + commandName+ "\" added");
            Files.delete(Path.of(fileName));
        } catch (IllegalArgumentException e) {
            reply(String.format("Could not remove: %s", e.getMessage()));
        } catch (IOException e) {
            LOG.error("Could not delete removed sound board commands file:" + fileName, e);
        }
    }
}

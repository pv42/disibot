package disibot.soundboard;

import disibot.DisiBot;
import disibot.newcommand.SlashCommandHandler;
import disibot.server.ServerFactory;
import disibot.util.SoundFilePlayer;
import net.dv8tion.jda.api.entities.GuildVoiceState;
import net.dv8tion.jda.api.entities.VoiceChannel;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.interactions.commands.build.CommandData;
import org.jetbrains.annotations.NotNull;

import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.IOException;

public class SoundBoardCommandHandler implements SlashCommandHandler {
    private final String command;
    private final String soundFileName;

    public SoundBoardCommandHandler(String command, String soundFileName) {
        this.command = command;
        this.soundFileName = soundFileName;
    }

    @Override
    public void handle(@NotNull SlashCommandEvent ev) {
        if (!ev.isFromGuild()) {
            ev.reply(DisiBot.getString("server_only_command")).queue();
            return;
        }
        //noinspection ConstantConditions
        if(!ServerFactory.get(ev.getGuild()).canUseSoundboard(ev.getMember())) {
            ev.reply(DisiBot.getString("permission_denied")).setEphemeral(true).queue();
        }
        @SuppressWarnings("ConstantConditions")
        GuildVoiceState voiceState = ev.getMember().getVoiceState();
        if (voiceState == null) {
            ev.reply("Can't read voice information").queue();
            return;
        }
        VoiceChannel ch = voiceState.getChannel();
        if (ch == null) {
            ev.reply(DisiBot.getString("sound_no_voice_channel")).setEphemeral(true).queue();
            return;
        }
        try {
            SoundFilePlayer.playSoundFile(soundFileName, ch, ch.getGuild());
            ev.reply("Playing sound").setEphemeral(true).queue();
        } catch (IOException e) {
            ev.reply(DisiBot.getString("sound_file_io_error")).queue();
        } catch (UnsupportedAudioFileException e) {
            ev.reply(DisiBot.getString("sound_audio_format_not_supported")).queue();
        }
    }

    @Override
    public CommandData getCommand() {
        return new CommandData(command, "A soundboard command.");
    }
}

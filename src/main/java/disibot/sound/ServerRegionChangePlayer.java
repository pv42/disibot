package disibot.sound;


import disibot.util.SoundFilePlayer;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.Region;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.VoiceChannel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class ServerRegionChangePlayer implements Runnable {
    private final static Logger LOG = LoggerFactory.getLogger(ServerRegionChangePlayer.class);
    private final JDA jda;
    private final Map<Guild, Region> serverRegions = new HashMap<>();

    public ServerRegionChangePlayer(JDA jda) {
        this.jda = jda;
    }

    public void run() {
        for (Guild guild : jda.getGuilds()) {
            if (serverRegions.containsKey(guild)) {
                if (guild.getRegion() != serverRegions.get(guild)) {
                    LOG.info("Server region of " + guild.getName() + " changed to " + guild.getRegion().getName());
                }
                if (guild.getRegion().equals(Region.RUSSIA) && !serverRegions.get(guild).equals(Region.RUSSIA)) {
                    VoiceChannel channel = null;
                    for (VoiceChannel c : guild.getVoiceChannels()) {
                        int memberCount = c.getMembers().size();
                        if (memberCount > 0 && (channel == null || channel.getMembers().size() < memberCount)) {
                            channel = c;
                        }
                    }
                    if (channel != null) {
                        VoiceChannel finalChannel = channel;
                        Thread t = new Thread(() -> {
                            try {
                                SoundFilePlayer.playSoundFile("res/sowjet_anthem.mp3", finalChannel, guild);
                            } catch (IOException | UnsupportedAudioFileException e) {
                                e.printStackTrace();
                            }
                        });
                        t.setDaemon(true);
                        t.setName("ServerRegionChangePlayerTask");
                        t.start();
                    }
                }
            }
            serverRegions.put(guild, guild.getRegion());
        }
    }
}

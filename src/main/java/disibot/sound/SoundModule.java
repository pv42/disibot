package disibot.sound;

import disibot.context.ContextStorage;
import disibot.modules.DisiBotModuleInterface;
import disibot.modules.DisiBotModule;
import disibot.tasks.Task;
import net.dv8tion.jda.api.JDA;
import org.jetbrains.annotations.NotNull;

import javax.annotation.Nonnull;
import java.time.Duration;

public class SoundModule extends DisiBotModule {
    private final JDA jda;
    private final ContextStorage context;
    public SoundModule(ContextStorage context, JDA jda) {
        this.jda = jda;
        this.context = context;
    }
    @Override
    public void load(@Nonnull @NotNull DisiBotModuleInterface moduleInterface) {
        //ServerRegionChangePlayer player = new ServerRegionChangePlayer(jda);
        moduleInterface.addCommandHandler(this, new SoundCommandHandler(context));
        //Task task = new Task("ServerRegionChangeCheckTask", Duration.ofSeconds(6), player);
        //moduleInterface.addTask(this, task);
    }
}

package disibot.sound;

import disibot.DisiBot;
import disibot.command.CommandHandler;
import disibot.command.NoAttachmentException;
import disibot.command.NotAServerException;
import disibot.context.ContextStorage;
import disibot.util.Command;
import disibot.util.SoundFilePlayer;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.VoiceChannel;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.managers.AudioManager;

import javax.annotation.Nonnull;
import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.IOException;
import java.util.Random;

public class SoundCommandHandler extends CommandHandler {

    /**
     * Creates a soundboard command handler with a storage that associates private channels with servers
     *
     * @param contexts private message server contexts
     */
    public SoundCommandHandler(ContextStorage contexts) {
        super(contexts);
    }

    @Override
    public boolean canHandleCommand(@Nonnull Command command, @Nonnull MessageReceivedEvent event) {
        return command.peekString().equals("testsound") || command.peekString().equals("play") ||
                command.peekString().equals("stop");
    }

    @Override
    protected void handle(@Nonnull Command command) {
        try {
            handleVolatile(command);
        } catch (NotAServerException e) {
            reply(DisiBot.getString("server_only_command"));
        } catch (IOException e) {
            reply(DisiBot.getString("sound_file_io_error"));
        } catch (UnsupportedAudioFileException e) {
            reply(DisiBot.getString("sound_audio_format_not_supported"));
        } catch (NoAttachmentException e) {
            reply(DisiBot.getString("sound_no_attachment"));
        }
    }

    private VoiceChannel getSendersVoiceChannel() throws NotAServerException {
        Guild guild = getGuild();
        Member sender = getSenderAsMember();
        VoiceChannel channel = null;
        for (VoiceChannel c : guild.getVoiceChannels()) {
            for (Member member : c.getMembers()) {
                if (member.equals(sender)) {
                    channel = c;
                    break;
                }
            }
        }
        return channel;
    }

    private void playSoundFile(String filename, VoiceChannel channel) throws NotAServerException, IOException, UnsupportedAudioFileException {
        SoundFilePlayer.playSoundFile(filename, channel, getGuild());
    }


    private void handleVolatile(@Nonnull Command command) throws NotAServerException, IOException, UnsupportedAudioFileException, NoAttachmentException {
        String sub = command.nextString();
        switch (sub) {
            case "play":
                if (getServer().canPlayAudio(getSenderAsMember())) {
                    String filename = "tmpaudio." + new Random().nextInt();
                    downloadAttachment(filename);
                    VoiceChannel channel = getSendersVoiceChannel();
                    if (channel == null) {
                        reply(DisiBot.getString("sound_no_voice_channel"));
                        return;
                    }
                    playSoundFile(filename, channel);
                } else {
                    reply(DisiBot.getString("permission_denied"));
                }
                break;
            case "stop":
                AudioManager manager = getGuild().getAudioManager();
                manager.closeAudioConnection();
                break;
            case "testsound":
                if (!getServer().isElevated(getSenderAsMember())) {
                    VoiceChannel channel = getSendersVoiceChannel();
                    if (channel == null) {
                        reply(DisiBot.getString("sound_no_voice_channel"));
                        return;
                    }
                    reply(DisiBot.getString("sound_playing_test"));
                    playSoundFile("testres/oh.mp3", channel);
                }
                break;
        }
    }
}

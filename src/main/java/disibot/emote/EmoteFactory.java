package disibot.emote;

import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.Emote;
import net.dv8tion.jda.api.entities.Guild;

import javax.annotation.Nullable;
import java.util.List;
import java.util.Objects;


public class EmoteFactory {
    @Nullable
    public static Emote findEmote(String emoteName, Guild server, JDA jda) {
        List<Emote> emotes = jda.getEmotesByName(emoteName, true);
        for (int i = emotes.size() - 1; i >= 0; i--) {
            if(!Objects.equals(emotes.get(i).getGuild(), server)) {
                emotes.remove(i);
            }
        }
        if(emotes.size() == 0) return null;
        return emotes.get(0);
    }
}

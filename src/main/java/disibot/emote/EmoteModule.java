package disibot.emote;

import disibot.UsageStorage;
import disibot.context.ContextStorage;
import disibot.modules.DisiBotModuleInterface;
import disibot.modules.DisiBotModule;
import net.dv8tion.jda.api.JDA;
import org.jetbrains.annotations.NotNull;

import javax.annotation.Nonnull;

public class EmoteModule extends DisiBotModule {
    private final EmoteCommandHandler commandHandler;
    private final EmotePodiumCreator podiumCreator;

    public EmoteModule(ContextStorage contextStorage, UsageStorage usageStorage, JDA jda) {
        this.podiumCreator = new EmotePodiumCreator(usageStorage, jda);
        this.commandHandler = new EmoteCommandHandler(contextStorage, usageStorage, jda);
    }

    @Override
    public void load(@Nonnull @NotNull DisiBotModuleInterface moduleInterface) {
        moduleInterface.addSlashCommandHandler(this, commandHandler);
        moduleInterface.addTask(this, podiumCreator.createTask());
    }

}

package disibot.emote;

import disibot.DisiBot;
import disibot.UsageStorage;
import disibot.command.NotAServerException;
import disibot.context.ContextStorage;
import disibot.newcommand.SlashCommandHandler;
import disibot.newcommand.SlashCommandUtil;
import disibot.util.Pair;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.Emote;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.interactions.commands.OptionMapping;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.build.CommandData;
import net.dv8tion.jda.api.interactions.commands.build.SubcommandData;
import org.jetbrains.annotations.NotNull;

import java.time.Duration;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class EmoteCommandHandler implements SlashCommandHandler {
    // emote counter does not work anymore therefore this
    private static final boolean disable_remove = true; // todo
    private final UsageStorage usageStorage;
    private final JDA jda;
    private final ContextStorage contextStorage;

    public EmoteCommandHandler(ContextStorage contextStorage, UsageStorage usageStorage, JDA jda) {
        this.contextStorage = contextStorage;
        this.usageStorage = usageStorage;
        this.jda = jda;
    }

    /*@Override
    public boolean canHandleCommand(@NotNull Command command, @NotNull MessageReceivedEvent ev) {
        return command.peekString().equals("emote") || command.peekString().equals("emoji");
    }*/

    @Override
    public void handle(@NotNull SlashCommandEvent ev) {
        try {
            handleVolatile(ev);
        } catch (NotAServerException ex) {
            ev.reply(DisiBot.getString("server_only_command")).queue();
        }
    }

    @Override
    public CommandData getCommand() {
        CommandData cd = new CommandData("emote", "Stats about emotes");
        cd.addSubcommands(new SubcommandData("top", "Shows top used emotes"));
        cd.addSubcommands(new SubcommandData("all", "Shows all emotes"));
        SubcommandData statsSub = new SubcommandData("stats", "Shows stats about a emote");
        statsSub.addOption(OptionType.STRING,"emote", "name of emote");
        cd.addSubcommands(statsSub);
        cd.addSubcommands(new SubcommandData("autoremove", "Remove unused emotes"));
        return cd;
    }

    public void handleVolatile(@NotNull SlashCommandEvent ev) throws NotAServerException {
        String sub = ev.getSubcommandName();
        if(sub == null) {
            ev.reply(DisiBot.getString("internal_error")).queue();
            return;
        }
        switch (sub) {
            case "help":
                ev.reply(DisiBot.getString("emote_help")).queue();
                break;
            case "top": {
                StringBuilder reply = new StringBuilder(DisiBot.getString("emote_usage"));
                reply.append('\n');
                for (Pair<Integer, Emote> hit : usageStorage.getTopList(ev.getGuild())) {
                    reply.append(hit.getValue().getName()).append(": ").append(hit.getKey()).append("\n");
                }
                ev.reply(reply.toString()).queue();
                break;
            }
            case "all": {
                if(!ev.isFromGuild()) {
                    throw new NotAServerException("Command only works on servers");
                }
                @SuppressWarnings("ConstantConditions") // isFromGuild ensures none null
                List<Emote> emotes = ev.getGuild().getEmotes();
                StringBuilder reply = new StringBuilder(String.format("```%-17s|%-12s|%-10s|%s\n",
                        DisiBot.getString("emote_emote"),
                        DisiBot.getString("emote_last_used"),
                        DisiBot.getString("emote_created"),
                        DisiBot.getString("emote_num_uses")));
                for (Emote emote : emotes) {
                    String usageStr = usageStorage.getLastUsageString(emote, ev.getGuild());
                    reply.append(String.format("%-17s|%-12s|%-10s|%6d",
                            emote.getName(),
                            usageStr,
                            emote.getTimeCreated().format(DateTimeFormatter.ISO_DATE).replaceAll("Z", ""),
                            usageStorage.getUsageNumber(emote, ev.getGuild())));
                }
                reply.append("```");
                ev.reply(reply.toString()).queue();
                break;
            }
            case "stats":
                handleStats(ev);
                break;
            case "autoremove":
                handleAutoremove(ev);
                break;
            default:
                ev.reply(DisiBot.getString("internal_error")).queue();
                break;
        }
    }

    private void handleAutoremove(SlashCommandEvent ev) throws NotAServerException {
        if (!SlashCommandUtil.getServer(ev, contextStorage).canAutoremoveEmote(ev.getMember())) {
            ev.reply(DisiBot.getString("permission_denied")).queue();
            return;
        }
        if(!ev.isFromGuild()) {
            throw new NotAServerException("Command only works on servers");
        }
        if (disable_remove) {
            ev.reply("autoremove is disabled").queue();
            return;
        }
        boolean test = true;
        List<String> removed = new ArrayList<>();
        for (Emote emote : ev.getGuild().getEmotes()) {
            long last = usageStorage.getLastUsage(emote, ev.getGuild());
            if (last > Duration.ofDays(90).getSeconds()) {
                if (!test) emote.delete().reason("no/low usage").queue();
                removed.add(emote.getName());
            }
        }
        StringBuilder listStr = new StringBuilder();
        for (String name : removed) {
            listStr.append(", ").append(name);
        }
        String testStr = "";
        if (test) testStr = "[SIMULATION]\n";
        if (removed.size() > 30)
            ev.reply(String.format(DisiBot.getString("emotes_removed"), testStr, removed.size(), listStr.substring(2))).queue();
        else ev.reply(testStr + DisiBot.getString("emotes_none_removed")).queue();
    }

    private void handleStats(@NotNull SlashCommandEvent ev) throws NotAServerException {
        OptionMapping emoteOption = ev.getOption("emote");
        if (emoteOption == null) {
            ev.reply(DisiBot.getString("internal_error")).queue();
            return;
        }
        String emoteName = emoteOption.getAsString();
        if (emoteName.equals("")) {
            ev.reply(DisiBot.getString("emote_stats_usage")).queue();
            return;
        }
        Emote emote = EmoteFactory.findEmote(emoteName, ev.getGuild(), jda);
        if (emote == null) {
            ev.reply(String.format(DisiBot.getString("emote_doesnt_exist"), emoteName)).queue();
            return;
        }
        Guild server = ev.getGuild();
        StringBuilder channelTable = new StringBuilder();
        Map<String, Integer> channelUsage = usageStorage.getUsageByChannel(emote, server);
        for (String channelName : channelUsage.keySet()) {
            channelTable.append(String.format("%-20s|%6d\n", channelName, channelUsage.get(channelName)));
        }
        ev.reply(emoteName + "\n" +
                DisiBot.getString("emote_created") + ": " + emote.getTimeCreated().format(DateTimeFormatter.ISO_DATE).replaceAll("Z", "") + "\n" +
                DisiBot.getString("emote_last_used") + ": " + usageStorage.getLastUsageString(emote, server) + "\n" +
                DisiBot.getString("emote_num_uses") + ": " + usageStorage.getUsageNumber(emote, server) + "\n" +
                DisiBot.getString("emote_uses_30d") + ": " + usageStorage.getUsageNumber(emote, server, 30) + "\n" +
                DisiBot.getString("emote_uses_90d") + ": " + usageStorage.getUsageNumber(emote, server, 90) + "\n" +
                String.format("```%-20s|%s\n%s```", DisiBot.getString("channel"), DisiBot.getString("emote_num_uses"), channelTable)
        ).queue();
    }

}

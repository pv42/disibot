package disibot.emote;

import disibot.UsageStorage;
import disibot.tasks.Task;
import disibot.util.Pair;
import disibot.util.SVGSender;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.Emote;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.TextChannel;
import org.apache.batik.transcoder.TranscoderException;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.time.Duration;
import java.util.Calendar;
import java.util.List;

import static java.util.Calendar.YEAR;

public class EmotePodiumCreator implements Runnable {
    private static final Logger LOG = LoggerFactory.getLogger(EmotePodiumCreator.class);
    private final UsageStorage usageStorage;
    private final JDA jda;
    private int prevYear;

    /**
     * Creates a emote podium creator
     *
     * @param usageStorage storage of emote usages
     * @param jda          java discord api used by the bot, this is used to get the a list of servers on which a podium
     *                     is send
     */
    public EmotePodiumCreator(@NotNull UsageStorage usageStorage, @NotNull JDA jda) {
        this.usageStorage = usageStorage;
        this.jda = jda;
        prevYear = Calendar.getInstance().get(YEAR);
    }

    public Task createTask() {
        return new Task("EmotePodiumTask", Duration.ofSeconds(20), this);
    }

    @Override
    public void run() {
        if (didYearChange()) {
            sendEmotePodiums();
        }
    }

    public void sendEmotePodiums() {
        String svgRaw;
        try {
            svgRaw = loadSVGFile();
        } catch (IOException e) {
            LOG.error("IOException reading the emote podium svg");
            return;
        }
        for (Guild guild : jda.getGuilds()) {
            sendEmotePodium(guild, svgRaw);
        }
    }

    private void sendEmotePodium(Guild guild, String svgRaw) {
        try {
            sendEmotePodiumVolatile(guild, svgRaw);
        } catch (TranscoderException e) {
            LOG.error("Failed to encode svg for podium for server " + guild.getName() + ": " + e.getMessage());
        } catch (IOException e) {
            LOG.error("IOException while sending a podium svg for server " + guild.getName() + ": " + e.getMessage());
        }
    }

    private void sendEmotePodiumVolatile(Guild guild, String svgRaw) throws TranscoderException, IOException {
        LOG.info("processing emote podium for " + guild.getName());
        String svgFormatted = formatSVG(svgRaw, guild);
        if (svgFormatted == null) return;
        TextChannel channel = guild.getDefaultChannel();
        if (channel != null) {
            SVGSender.send(svgFormatted, channel);
        } else {
            LOG.info("Server " + guild.getName() + "does not seem to have a default text channel, skipping emote podium");
        }
    }

    private String formatSVG(String svg, Guild guild) {
        svg = svg.replaceFirst("\\[year]", String.valueOf(prevYear - 1));
        List<Pair<Integer, Emote>> emotes = usageStorage.getTopList(guild);
        if (emotes.size() < 3) {
            LOG.warn("This server does not have at least 3 used custom emotes");
            return null;
        }
        for (int i = 1; i <= 3; i++) {
            Pair<Integer, Emote> emotePair = emotes.get(i - 1);
            Emote emote = emotePair.getValue();
            int uses = emotePair.getKey();
            String usageString = uses == 1 ? "1 use" : uses + " uses";
            svg = svg.replaceFirst("\\[url" + i + "]", emote.getImageUrl());
            svg = svg.replaceFirst("\\[name" + i + "]", emote.getName());
            svg = svg.replaceFirst("\\[uses" + i + "]", usageString);
        }
        return svg;
    }

    private String loadSVGFile() throws IOException {
        File svgFile = new File("res/top.svg");
        FileInputStream in = new FileInputStream(svgFile);
        byte[] data = new byte[(int) svgFile.length()];
        int bytesRead = in.read(data);
        if (bytesRead != data.length) throw new IOException("Could not read whole file.");
        return new String(data);
    }

    private boolean didYearChange() {
        int yearNow = Calendar.getInstance().get(YEAR);
        boolean change = yearNow != prevYear;
        prevYear = yearNow;
        return change;
    }
}

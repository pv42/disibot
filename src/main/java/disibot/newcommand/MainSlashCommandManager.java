package disibot.newcommand;

import disibot.modules.DisiBotModule;
import disibot.welcomesound.UserCommandCreateActionImpl;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.dv8tion.jda.api.interactions.commands.Command;
import net.dv8tion.jda.api.interactions.commands.build.CommandData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.util.HashMap;
import java.util.Map;

public final class MainSlashCommandManager extends ListenerAdapter {
    private static final Logger LOG = LoggerFactory.getLogger(MainSlashCommandManager.class);
    private final JDA jda;
    private final Map<DisiBotModule, Map<Long, SlashCommandEventHandler>> activeCommands = new HashMap<>();
    private final Map<DisiBotModule, Map<Guild, Map<Long, SlashCommandEventHandler>>> activeGuildCommands = new HashMap<>();
    private final Map<DisiBotModule, Map<String, SlashCommandEventHandler>> activeUserCommands = new HashMap<>();

    public MainSlashCommandManager(JDA jda) {
        this.jda = jda;
    }

    public void registerCommand(DisiBotModule module, CommandData commandData, SlashCommandEventHandler handler) {
        jda.upsertCommand(commandData).queue(command -> {
            if (!activeCommands.containsKey(module)) {
                activeCommands.put(module, new HashMap<>());
            }
            activeCommands.get(module).put(command.getIdLong(), handler);
        });
    }

    public void registerGuildCommand(DisiBotModule module, @Nonnull CommandData commandData, SlashCommandEventHandler handler, @Nonnull Guild guild) {
        LOG.info("registering slash command " + commandData.getName());
        guild.upsertCommand(commandData).queue(command -> {
            if (!activeGuildCommands.containsKey(module)) {
                activeGuildCommands.put(module, new HashMap<>());
            }
            if (!activeGuildCommands.get(module).containsKey(guild)) {
                activeGuildCommands.get(module).put(guild, new HashMap<>());
            }
            activeGuildCommands.get(module).get(guild).put(command.getIdLong(), handler);
        });
    }

    public void unregisterGuildCommand(DisiBotModule module, @Nonnull Guild guild, long id) {
        guild.deleteCommandById(id).queue();
        activeGuildCommands.get(module).get(guild).remove(id);
    }

    @Override
    public void onSlashCommand(@Nonnull SlashCommandEvent event) {
        for (Map<Long, SlashCommandEventHandler> commands : activeCommands.values()) {
            if (commands.containsKey(event.getCommandIdLong())) {
                commands.get(event.getCommandIdLong()).handle(event);
                return;
            }
        }
        for (Map<Guild, Map<Long, SlashCommandEventHandler>> guildCommands : activeGuildCommands.values()) {
            if (guildCommands.containsKey(event.getGuild())) {
                Map<Long, SlashCommandEventHandler> commands = guildCommands.get(event.getGuild());
                if (commands.containsKey(event.getCommandIdLong())) {
                    commands.get(event.getCommandIdLong()).handle(event);
                    return;
                }
            }
        }
        for (Map<String, SlashCommandEventHandler> commands : activeUserCommands.values()) {
            if (commands.containsKey(event.getName())) {
                commands.get(event.getName()).handle(event);
                return;
            }
        }
        event.reply("unknown command event").queue();
        LOG.warn("Received event for unregistered event:" + event.getName());
    }

    public void unloadModule(DisiBotModule module) {
        for (Long id : activeCommands.get(module).keySet()) {
            jda.deleteCommandById(id).queue();
        }
        for (String name : activeUserCommands.get(module).keySet()) {
            for (Command command : jda.retrieveCommands().complete()) {
                if (command.getName().equals(name)) {
                    jda.deleteCommandById(command.getIdLong()).queue();
                    break;
                }
            }
        }
        for (Guild guild : activeGuildCommands.get(module).keySet()) {
            for (Long id : activeGuildCommands.get(module).get(guild).keySet()) {
                guild.deleteCommandById(id).queue();
            }
        }
        activeCommands.remove(module);
        activeGuildCommands.remove(module);
    }

    public void registerUserCommand(DisiBotModule module, @Nonnull UserCommandCreateActionImpl uccai, SlashCommandEventHandler handler) {
        uccai.queue(command -> {
            if (!activeUserCommands.containsKey(module)) {
                activeUserCommands.put(module, new HashMap<>());
            }
            activeUserCommands.get(module).put(uccai.getName(), handler);
        });
    }
}

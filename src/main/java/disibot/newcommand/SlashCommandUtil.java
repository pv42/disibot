package disibot.newcommand;

import disibot.command.NotAServerException;
import disibot.context.ContextStorage;
import disibot.server.Server;
import disibot.server.ServerFactory;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;

public class SlashCommandUtil {
    public static void reply(SlashCommandEvent ev, String msg) {
        ev.getChannel().sendMessage(msg).queue();
    }

    public static Member getSenderAsMember(SlashCommandEvent ev, ContextStorage contexts) throws NotAServerException {
        return getGuild(ev, contexts).getMember(ev.getUser());
    }

    public static MessageChannel getChannel(SlashCommandEvent ev) {
        return ev.getChannel();
    }

    public static Server getServer(SlashCommandEvent ev, ContextStorage contexts) throws NotAServerException {
        return ServerFactory.get(getGuild(ev, contexts));
    }

    public static Guild getGuild(SlashCommandEvent ev, ContextStorage contexts) throws NotAServerException {
        if (ev.isFromGuild()) {
            return ev.getGuild();
        } else {
            if (contexts.getData().containsKey(ev.getPrivateChannel())) {
                return contexts.getData().get(ev.getPrivateChannel());
            } else {
                throw new NotAServerException("This message did not happen on a Server");
            }
        }
    }
}

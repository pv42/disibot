package disibot.newcommand;

import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;

public interface SlashCommandEventHandler {
    void handle(SlashCommandEvent ev);
}

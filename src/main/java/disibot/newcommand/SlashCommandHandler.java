package disibot.newcommand;

import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.interactions.commands.build.CommandData;

import javax.annotation.Nonnull;

public interface SlashCommandHandler {
    void handle(@Nonnull SlashCommandEvent ev);
    CommandData getCommand();
}

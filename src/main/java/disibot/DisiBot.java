package disibot;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import disibot.antidel.AntiDelModuleFactory;
import disibot.coinflip.CoinflipModuleFactory;
import disibot.command.MainCommandModuleFactory;
import disibot.config.ConfigModuleFactory;
import disibot.config.ConfigSection;
import disibot.config.ConfigSectionAdapter;
import disibot.config.ConfigStorage;
import disibot.context.ContextModuleFactory;
import disibot.context.ContextStorage;
import disibot.custom.CustomCommandModuleFactory;
import disibot.emote.EmoteModuleFactory;
import disibot.keywordreactions.KeywordReactionModuleFactory;
import disibot.lostarkstatus.LostArkStatusModuleFactory;
import disibot.modules.DisiBotModule;
import disibot.modules.DisiBotModuleInterface;
import disibot.modules.ModuleFactory;
import disibot.modules.ModuleManagementModuleFactory;
import disibot.poll.PollModuleFactory;
import disibot.reminder.ReminderModuleFactory;
import disibot.server.PermissionModuleFactory;
import disibot.sound.SoundModuleFactory;
import disibot.soundboard.SoundBoardModuleFactory;
import disibot.streammonitor.StreamMonitorModuleFactory;
import disibot.tasks.TaskModuleFactory;
import disibot.tasks.TaskRunner;
import disibot.update.UpdateModuleFactory;
import disibot.util.ModuleLoader;
import disibot.welcomesound.WelcomeSoundModuleFactory;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.entities.Activity;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.interactions.commands.Command;
import net.dv8tion.jda.api.requests.GatewayIntent;
import org.jetbrains.annotations.PropertyKey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.impl.SimpleLogger;

import javax.annotation.Nullable;
import javax.security.auth.login.LoginException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

import static disibot.util.Util.readTokenFile;
import static net.dv8tion.jda.api.requests.GatewayIntent.DEFAULT;
import static net.dv8tion.jda.api.requests.GatewayIntent.GUILD_MEMBERS;
import static net.dv8tion.jda.api.requests.GatewayIntent.GUILD_PRESENCES;

public class DisiBot {

    public static final String VERSION = "v0.5-pre-t5a";
    private static final String ERR_TOKEN_FILE_NOT_FOUND = "Could not find token file.";
    private static final String ERR_TOKEN_FILE_READ_ERROR = "Could not read token file.";
    private static final String ERR_TOKEN_INVALID_ERROR = "Could not connect to bot: token is invalid";
    private static final String INF_STARTING = "Starting Bot";
    private static final int INTENTS = DEFAULT | GUILD_MEMBERS.getRawValue() | GUILD_PRESENCES.getRawValue();
    private static final String BUNDLE_NAME = "disibot_core";
    private static final Logger LOG;
    private static final String CONFIG_PATH = "config.json";
    private static DisiBot instance;

    private static ResourceBundle resourceStringBundle = ResourceBundle.getBundle(BUNDLE_NAME, Locale.US);

    static { // configure log
        System.setProperty(SimpleLogger.LOG_FILE_KEY, "disibot.log");
        System.setProperty(SimpleLogger.SHOW_SHORT_LOG_NAME_KEY, "true");
        System.setProperty(SimpleLogger.SHOW_DATE_TIME_KEY, "true");
        System.setProperty(SimpleLogger.DATE_TIME_FORMAT_KEY, "dd.MM HH:mm:ss:SSS");
        LOG = LoggerFactory.getLogger(DisiBot.class);
    }

    private final ContextStorage contextStorage;
    private final List<DisiBotModule> allModules = new ArrayList<>();
    private final List<DisiBotModule> activeModules = new ArrayList<>();
    private final UsageStorage usage;
    private DisiBotModuleInterface moduleInterface;
    private TaskRunner taskRunner;
    private ConfigStorage configStorage;
    private JDA jda;

    public DisiBot() {
        if (instance != null)
            throw new UnsupportedOperationException("Can't create multiple instances of " + this.getClass().getName());
        instance = this;
        contextStorage = new ContextStorage();
        usage = new UsageStorage();
        taskRunner = new TaskRunner();
        JDABuilder builder = createJDABuilder();
        if (builder == null) return;
        List<Guild> scannedServers = new ArrayList<>();
        DisiBotEventListener eventListener = new DisiBotEventListener(scannedServers, usage);
        builder.addEventListeners(eventListener);
        builder.setActivity(Activity.playing(resourceStringBundle.getString("activity")));
        try {
            this.jda = builder.build();
            eventListener.setJda(jda);
            this.init();
        } catch (LoginException e) {
            LOG.error(ERR_TOKEN_INVALID_ERROR);
        }
        // remove global user cmd
        for (Command cmd: jda.retrieveCommands().complete()) {
            if(cmd.getName().equals("Play welcome sound")) {
                jda.deleteCommandById(cmd.getIdLong()).queue();
            }
        }
        configStorage = loadConfigStorage();
        moduleInterface = new DisiBotModuleInterface(taskRunner, configStorage, jda);
        LOG.info(INF_STARTING);
        loadModules();
    }

    public static void main(String[] args) {
        if (args.length >= 2) {
            if (args[1].equals("-version")) {
                System.out.println(VERSION);
            } else if (args[1].equals("-help")) {
                System.out.println("-version  shows current version\n" +
                        "-help    shows this help");
            } else {
                System.out.println("Unknown command line option:" + args[1]);
            }
        } else {
            new DisiBot();
        }
    }

    public static String getString(@PropertyKey(resourceBundle = BUNDLE_NAME) String key) {
        return resourceStringBundle.getString(key);
    }

    private void saveConfigStorage() {
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeHierarchyAdapter(ConfigSection.class, new ConfigSectionAdapter());
        Gson gson = builder.create();
        try {
            FileWriter fw = new FileWriter(CONFIG_PATH);
            gson.toJson(configStorage, ConfigStorage.class, new JsonWriter(fw));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private ConfigStorage loadConfigStorage() {
        try {
            GsonBuilder builder = new GsonBuilder();
            builder.registerTypeHierarchyAdapter(ConfigSection.class, new ConfigSectionAdapter());
            Gson gson = builder.create();
            FileReader fr = new FileReader(CONFIG_PATH);
            return gson.fromJson(new JsonReader(fr), ConfigStorage.class);
        } catch (FileNotFoundException ex) {
            LOG.info("create new config storage");
        } catch (JsonIOException | JsonSyntaxException e) {
            LOG.error("could not load config", e);
            e.printStackTrace();
        }
        return new ConfigStorage(this::saveConfigStorage);
    }

    private void loadModules() {
        List<ModuleFactory> factories = new ArrayList<>();
        factories.add(new MainCommandModuleFactory());
        factories.add(new UpdateModuleFactory());
        factories.add(new ConfigModuleFactory());
        factories.add(new ModuleManagementModuleFactory());
        factories.add(new TaskModuleFactory());
        factories.add(new ContextModuleFactory());
        factories.add(new PermissionModuleFactory());
        factories.add(new ReminderModuleFactory());
        ModuleFactory leagueModuleFactory = ModuleLoader.loadModule(new File("modules/LeagueModule.jar"));
        if(leagueModuleFactory != null) factories.add(leagueModuleFactory);
        //factories.add(new LeagueModuleFactory());
        factories.add(new CoinflipModuleFactory());
        factories.add(new SoundModuleFactory());
        factories.add(new EmoteModuleFactory());
        factories.add(new CustomCommandModuleFactory());
        factories.add(new KeywordReactionModuleFactory());
        factories.add(new AntiDelModuleFactory());
        ModuleFactory apexModuleFactory = ModuleLoader.loadModule(new File("modules/ApexModule.jar"));
        if(apexModuleFactory != null) factories.add(apexModuleFactory);
        factories.add(new WelcomeSoundModuleFactory());
        factories.add(new SoundBoardModuleFactory());
        factories.add(new PollModuleFactory());
        factories.add(new StreamMonitorModuleFactory());
        factories.add(new LostArkStatusModuleFactory());
        for (ModuleFactory factory: factories) {
            allModules.add(factory.initiateModule(activeModules, allModules, contextStorage, moduleInterface, jda, configStorage, usage, taskRunner));
        }
        for (DisiBotModule module : allModules) {
            try {
                module.load(moduleInterface);
                activeModules.add(module);
            } catch (Exception e) {
                LOG.error("Could not load module " + module.getName() + ": " + e.getMessage(), e);
            }
        }
    }

    @Nullable
    private JDABuilder createJDABuilder() {
        String TOKEN_FILE = "token";
        String token;
        try {
            token = readTokenFile(TOKEN_FILE);
        } catch (FileNotFoundException e) {
            LOG.error(ERR_TOKEN_FILE_NOT_FOUND);
            return null;
        } catch (IOException e) {
            LOG.error(ERR_TOKEN_FILE_READ_ERROR);
            return null;
        }
        return JDABuilder.create(token, GatewayIntent.getIntents(INTENTS));
    }

    private void init() {
        long start = System.currentTimeMillis();
        JDA.Status status = jda.getStatus();
        LOG.info("JDA status is " + status);
        while (status != JDA.Status.CONNECTED) {
            if (status != jda.getStatus()) LOG.info("JDA status is changed to " + jda.getStatus());
            status = jda.getStatus();
            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        LOG.info(String.format("JDA connected in %d ms", System.currentTimeMillis() - start));
        taskRunner = new TaskRunner();
    }


    public static void shutdown() {
        instance.shutdownInternal();
    }

    private void shutdownInternal() {
        LOG.info("Shutting down DisiBot ...");
        taskRunner.shutdown();
        jda.shutdown();
        jda.shutdownNow();
        LOG.info("Shutdown complete");
    }

    public static void restart(MessageChannel ch) {
        ch.sendMessage(DisiBot.getString("update_restarting_bot")).queue();
        Thread shutdownThread = new Thread(DisiBot::shutdown);
        shutdownThread.setName("ShutdownThread");
        shutdownThread.start();
        System.exit(1);
    }
}
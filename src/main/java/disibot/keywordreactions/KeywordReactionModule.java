package disibot.keywordreactions;

import disibot.context.ContextStorage;
import disibot.modules.DisiBotModule;
import disibot.modules.DisiBotModuleInterface;
import org.jetbrains.annotations.NotNull;

public class KeywordReactionModule extends DisiBotModule {
    private final ContextStorage contexts;

    public KeywordReactionModule(ContextStorage contexts) {
        this.contexts = contexts;
    }

    @Override
    public void load(@NotNull DisiBotModuleInterface moduleInterface) {
        KeywordReactionCommandHandler commandHandler = new KeywordReactionCommandHandler(contexts);
        KeywordReactionEventListener listener = new KeywordReactionEventListener(contexts);
        moduleInterface.addDiscordListenerAdapter(this, listener);
        moduleInterface.addSlashCommandHandler(this, commandHandler);
    }
}

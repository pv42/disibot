package disibot.keywordreactions;

import disibot.DisiBot;
import disibot.command.NotAServerException;
import disibot.context.ContextStorage;
import disibot.newcommand.SlashCommandHandler;
import disibot.util.DiscordUtil;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.interactions.commands.OptionMapping;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.build.CommandData;
import net.dv8tion.jda.api.interactions.commands.build.OptionData;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static disibot.newcommand.SlashCommandUtil.getServer;

public class KeywordReactionCommandHandler implements SlashCommandHandler {
    private ContextStorage contextStorage;

    public KeywordReactionCommandHandler(ContextStorage contextStorage) {
        this.contextStorage = contextStorage;
    }

    public void handle(@NotNull SlashCommandEvent ev) {
        try {
            handleVolatile(ev);
        } catch (NotAServerException e) {
            ev.reply(DisiBot.getString("server_only_command")).queue();
        }
    }

    @Override
    public CommandData getCommand() {
        List<OptionData> options = new ArrayList<>();
        OptionData phraseOption = new OptionData(OptionType.STRING, "phrase", "Phrase to trigger the reaction", true);
        OptionData emoteOption = new OptionData(OptionType.STRING, "emote", "Emote to react with or remove to remove a existing reaction, emote can be a unicode or discord emote", true);
        options.add(phraseOption);
        options.add(emoteOption);
        CommandData commandData = new CommandData("reaction", "Sets a automatic reaction based of the usage of a phrase");
        commandData.addOptions(options);
        return commandData;
    }


    private void handleVolatile(@NotNull SlashCommandEvent ev) throws NotAServerException {
        OptionMapping phraseOption = ev.getOption("phrase");
        OptionMapping emoteOption = ev.getOption("emote");
        if (phraseOption == null || emoteOption == null) {
            ev.reply(DisiBot.getString("internal_error")).queue();
            return;
        }
        String keyword = phraseOption.getAsString();
        Map<String, String> keywords = getServer(ev, contextStorage).getKeywordReactionData();
        if (emoteOption.getAsString().equals("remove")) {// remove
            if (!keywords.containsKey(keyword) && !keywords.containsKey(keyword.toLowerCase())) { // in current version all are lowercase but in prev versions this allowed for upper case
                ev.reply(DisiBot.getString("reaction_remove_failed")).queue();
                return;
            }
            if (!keywords.containsKey(keyword)) { // check if was lower case or matching case
                keyword = keyword.toLowerCase();
            }
            getServer(ev, contextStorage).removeKeywordReaction(keyword);
            ev.reply(String.format(DisiBot.getString("reaction_reaction_removed"), keyword)).queue();
            return;
        }
        keyword = keyword.toLowerCase();
        String emote = emoteOption.getAsString();
        if (DiscordUtil.isStringCustomEmote(emoteOption.getAsString())) {
            getServer(ev, contextStorage).addKeywordReaction(keyword, emote);
            ev.reply(String.format(DisiBot.getString("reaction_reaction_set"), emote, keyword)).queue();
        } else if (emote.length() == 2) {
            getServer(ev, contextStorage).addKeywordReaction(keyword, emote);
            ev.reply(String.format(DisiBot.getString("reaction_reaction_set"), emote, keyword)).queue();
        } else {
            ev.reply(String.format(DisiBot.getString("reaction_not_a_emote"), emote)).queue();
        }
    }
}

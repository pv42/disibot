package disibot.keywordreactions;

import disibot.command.NotAServerException;
import disibot.context.ContextStorage;
import disibot.server.Server;
import disibot.server.ServerFactory;
import disibot.util.DiscordUtil;
import net.dv8tion.jda.api.entities.Emote;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

public class KeywordReactionEventListener extends ListenerAdapter {

    private static final Logger LOG = LoggerFactory.getLogger(KeywordReactionEventListener.class);
    private final ContextStorage contexts;

    public KeywordReactionEventListener(ContextStorage contexts) {
        this.contexts = contexts;
    }

    @Override
    public void onMessageReceived(@NotNull MessageReceivedEvent event) {
        Message message = event.getMessage();
        Map<String,String> keywords;
        try {
            keywords = getServer(event).getKeywordReactionData();
        } catch (NotAServerException e) {
            return; // private channel w/o context -> ignore
        }
        for (String keyword : keywords.keySet()) {
            if (message.getContentRaw().toLowerCase().contains(keyword.toLowerCase())) {
                String reaction = keywords.get(keyword);
                if (DiscordUtil.isStringCustomEmote(reaction)) {
                    Emote emote = event.getJDA().getEmoteById(reaction.split(":")[2].split(">")[0]);
                    if(emote == null) {
                        LOG.error("Could not resolve emote " + reaction);
                        continue;
                    }
                    message.addReaction(emote).queue();
                } else {
                    message.addReaction(reaction).queue();
                }
            }
        }
    }

    private Server getServer(MessageReceivedEvent event) throws NotAServerException {
        return ServerFactory.get(getGuild(event));
    }

    private Guild getGuild(MessageReceivedEvent event) throws NotAServerException {
        if (event.isFromGuild()) {
            return event.getGuild();
        } else {
            if (contexts.getData().containsKey(event.getPrivateChannel())) {
                return contexts.getData().get(event.getPrivateChannel());
            } else {
                throw new NotAServerException("This message did not happen on a Server");
            }
        }
    }
}

package disibot.custom;

import disibot.DisiBot;
import disibot.command.InsufficientPowerException;
import disibot.command.NotAServerException;
import disibot.context.ContextStorage;
import disibot.newcommand.SlashCommandHandler;
import disibot.newcommand.SlashCommandUtil;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.interactions.commands.OptionMapping;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.build.CommandData;
import net.dv8tion.jda.api.interactions.commands.build.OptionData;
import net.dv8tion.jda.api.interactions.commands.build.SubcommandData;
import org.jetbrains.annotations.NotNull;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.List;

public class CustomCommandHandler implements SlashCommandHandler {

    private final ContextStorage contexts;
    private final CustomCommandModule module;

    public CustomCommandHandler(ContextStorage contexts, CustomCommandModule module) {
        this.contexts = contexts;
        this.module = module;

    }

    @Override
    public void handle(@NotNull SlashCommandEvent ev) {
        try {
            handleCustomSetupCommandVolatile(ev);
        } catch (NotAServerException e) {
            ev.reply(DisiBot.getString("internal_error")).queue();
        } catch (InsufficientPowerException e) {
            ev.reply(DisiBot.getString("permission_denied")).queue();
        }
    }

    @Override
    public CommandData getCommand() {
        List<OptionData> setOptions = new ArrayList<>();
        setOptions.add(new OptionData(OptionType.STRING, "name", "Name/command of the custom command", true));
        setOptions.add(new OptionData(OptionType.STRING, "value", "Text that should be send when the command gets invoked", true));
        SubcommandData setSubcommandData = new SubcommandData("set", "Creates or changes a custom command");
        setSubcommandData.addOptions(setOptions);
        SubcommandData delSubcommandData = new SubcommandData("remove", "Removes a custom command");
        delSubcommandData.addOption(OptionType.STRING, "name", "Name/command of the custom command", true);
        List<SubcommandData> subcommands = new ArrayList<>(2);
        SubcommandData listSubcommand = new SubcommandData("list", "List all custom commands on this server");
        subcommands.add(setSubcommandData);
        subcommands.add(delSubcommandData);
        subcommands.add(listSubcommand);
        CommandData commandData = new CommandData("custom", "Creates a custom command");
        commandData.addSubcommands(subcommands);
        return commandData;
    }

    private void handleCustomSetupCommandVolatile(@Nonnull SlashCommandEvent ev) throws NotAServerException, InsufficientPowerException {
        String subcommand = ev.getSubcommandName();
        if (subcommand == null) {
            ev.reply(DisiBot.getString("internal_error")).queue();
            return;
        }
        OptionMapping nameOp;
        String name;
        switch (subcommand) {
            case "set":
                nameOp = ev.getOption("name");
                OptionMapping valueOp = ev.getOption("value");
                if (nameOp == null || valueOp == null) {
                    ev.reply(DisiBot.getString("internal_error")).queue();
                    return;
                }
                name = nameOp.getAsString();
                String value = valueOp.getAsString();
                try {
                    SlashCommandUtil.getServer(ev, contexts).setCustomCommand(SlashCommandUtil.getSenderAsMember(ev, contexts), name, value);
                } catch (IllegalArgumentException e) {
                    ev.reply(String.format(DisiBot.getString("custom_command_set_error"), e.getMessage())).queue();
                    return;
                }
                module.addCommand(name, value, ev.getGuild());
                ev.reply(String.format(DisiBot.getString("custom_command_set"), name)).queue();
                break;
            case "remove":
                nameOp = ev.getOption("name");
                if (nameOp == null) {
                    ev.reply(DisiBot.getString("internal_error")).queue();
                    return;
                }
                name = nameOp.getAsString();
                try {
                    module.removeCommand(ev.getGuild(), name);
                    SlashCommandUtil.getServer(ev, contexts).removeCustomCommand(SlashCommandUtil.getSenderAsMember(ev, contexts), name);
                    ev.reply(String.format(DisiBot.getString("custom_command_removed"), name)).queue();
                } catch (IllegalArgumentException e) {
                    ev.reply(String.format(DisiBot.getString("custom_command_remove_error"), e.getMessage())).queue();
                }
                break;
            case "list":
                StringBuilder reply = new StringBuilder(DisiBot.getString("custom_commands_are"));
                reply.append('\n');
                boolean first = true;
                for (String cc : SlashCommandUtil.getServer(ev, contexts).getCustomCommands(SlashCommandUtil.getSenderAsMember(ev, contexts))) {
                    if (!first) reply.append(", ");
                    reply.append(cc);
                    first = false;
                }
                ev.reply(reply.toString()).queue();
                break;
            default:
                ev.reply(DisiBot.getString("custom_syntax_error")).queue();
                break;
        }
    }
}

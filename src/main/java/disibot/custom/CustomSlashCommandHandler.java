package disibot.custom;

import disibot.newcommand.SlashCommandHandler;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.interactions.commands.build.CommandData;
import org.jetbrains.annotations.NotNull;

public class CustomSlashCommandHandler implements SlashCommandHandler {
    private final String command;
    private final String response;

    public CustomSlashCommandHandler(String command, String response) {
        this.command = command;
        this.response = response;
    }

    @Override
    public void handle(@NotNull SlashCommandEvent ev) {
        ev.reply(response).queue();
    }

    @Override
    public CommandData getCommand() {
        return new CommandData(command, "A custom command.");
    }
}

package disibot.custom;

import disibot.context.ContextStorage;
import disibot.modules.DisiBotModule;
import disibot.modules.DisiBotModuleInterface;
import disibot.server.ServerFactory;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.Guild;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;

public class CustomCommandModule extends DisiBotModule {
    private static final Logger LOG = LoggerFactory.getLogger(CustomCommandModule.class);
    private final CustomCommandHandler commandHandler;
    private final JDA jda;
    private DisiBotModuleInterface moduleInterface;

    public CustomCommandModule(ContextStorage contextStorage, JDA jda) {
        this.commandHandler = new CustomCommandHandler(contextStorage, this);
        this.jda = jda;
    }

    @Override
    public void load(@Nonnull DisiBotModuleInterface moduleInterface) {
        moduleInterface.addSlashCommandHandler(this, commandHandler);
        this.moduleInterface = moduleInterface;
        // custom slash commands
        LOG.info("Loading custom commands for " + jda.getGuilds().size() + " guilds.");
        for (Guild guild : jda.getGuilds()) {
            ServerFactory.get(guild).foreachCustomCommand((name, response) ->
                    addCommand(name, response, guild));
        }
    }

    @Override
    public void unload(@NotNull DisiBotModuleInterface moduleInterface) {
        this.moduleInterface = null;
        super.unload(moduleInterface);
    }

    protected void addCommand(String name, String response, Guild guild) {
        if(moduleInterface != null)
            moduleInterface.addGuildSlashCommandHandler(this, new CustomSlashCommandHandler(name, response), guild);
        else
            LOG.warn("Can't add command while module is not loaded");
    }

    public void removeCommand(Guild guild, String commandName) {
        if(moduleInterface != null)
            moduleInterface.removeGuildCommandHandler(this, guild, commandName);
        else
            LOG.warn("Can't remove command while module is not loaded");
    }
}

package disibot.command;

public class NotAServerException extends Exception {
    public NotAServerException(String s) {
        super(s);
    }
}

package disibot.command;

/**
 * An <code>InsufficientPowerException</code> is thrown by certain methods of the
 * <code>disibot.server.Server</code> class to indicate that the user who tried to execute a command does not have permission to perform the
 * action requested by the command.
 *
 * @author pv42
 */
public class InsufficientPowerException extends Exception {
    /**
     * Constructs an <code>InsufficientPowerException</code> with the specified
     * detail message.
     *
     * @param msg the detail message
     */
    public InsufficientPowerException(String msg) {
        super(msg);
    }
}

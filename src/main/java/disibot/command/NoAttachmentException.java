package disibot.command;

public class NoAttachmentException extends Exception {
    public NoAttachmentException(String message) {
        super(message);
    }

    public NoAttachmentException(String message, Exception cause) {
        super(message, cause);
    }
}

package disibot.command;

import disibot.context.ContextStorage;
import disibot.modules.DisiBotModule;
import disibot.modules.DisiBotModuleInterface;
import org.jetbrains.annotations.NotNull;

public class MainCommandModule extends DisiBotModule {
    private final ContextStorage contexts;

    public MainCommandModule(ContextStorage contexts) {
        this.contexts = contexts;
    }

    @Override
    public void load(@NotNull DisiBotModuleInterface moduleInterface) {
        MainCommandHandler commandHandler = new MainCommandHandler(contexts);
        MainCommandHandlerEventListener eventListener = new MainCommandHandlerEventListener(commandHandler);
        moduleInterface.addDiscordListenerAdapter(this, eventListener);
        moduleInterface.setMainCommandHandler(commandHandler);
    }

    @Override
    public void unload(@NotNull DisiBotModuleInterface moduleInterface) {
        super.unload(moduleInterface);
        moduleInterface.setMainCommandHandler(null);
    }
}

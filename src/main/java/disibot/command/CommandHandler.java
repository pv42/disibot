package disibot.command;

import disibot.context.ContextStorage;
import disibot.server.Server;
import disibot.server.ServerFactory;
import disibot.util.Command;
import disibot.util.DiscordUtil;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.entities.PrivateChannel;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.io.File;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionException;

/**
 * @author pv42
 */
public abstract class CommandHandler {
    private MessageReceivedEvent event;
    private final ContextStorage contexts;
    private static final Logger logger = LoggerFactory.getLogger(CommandHandler.class);

    /**
     * Creates a command handler with a storage that associates private channels with servers
     *
     * @param contexts private message server contexts
     */
    public CommandHandler(ContextStorage contexts) {
        this.contexts = contexts;
    }

    /**
     * handles a command received by within a message event
     *
     * @param command command to be handled, must not be equal to the context of the message event
     * @param event   message event, in which the message was received
     */
    public final void handleCommand(@Nonnull Command command, @Nonnull MessageReceivedEvent event) {
        setEvent(event);
        handle(command);
    }

    /**
     * sets the event that contained the to be handled command, since handleCommand automaticly does that it should only
     * be used if the event context is needed for canHandleCommand
     * @param event message event, in which the message was received
     */
    protected void setEvent(@Nonnull MessageReceivedEvent event) {
        this.event = event;
    }

    /**
     * indicates whether a command handler can handle a given command
     *
     * @param command command to be evaluated
     * @return true if the command can be handled by the command handler, false otherwise
     */
    public abstract boolean canHandleCommand(@Nonnull Command command, @Nonnull MessageReceivedEvent event);

    /**
     * handles a command received by within a message event internally
     *
     * @param command command to be handled, must not be equal to the context of the message event
     */
    protected abstract void handle(@Nonnull Command command);

    /**
     * Sends a message in the channel the message was received
     *
     * @param message message to be send, must be at most then 2000 characters
     */
    protected final void reply(@Nonnull String message) {
        if(message.length()<=2000) {
            getChannel().sendMessage(message).queue();
        } else {
            DiscordUtil.sendLongMessage(message,getChannel());
        }
    }

    /**
     * Sends a message in the channel the message was received
     *
     * @param embed message to be send
     */
    protected final void reply(MessageEmbed embed) {
        getChannel().sendMessageEmbeds(embed).queue();
    }

    /**
     * gets the server context of the current or last handled command
     *
     * @return commands server context
     * @throws NotAServerException if the message was not send on a server and no context was specified
     */
    @Nonnull
    protected final Server getServer() throws NotAServerException {
        return ServerFactory.get(getGuild());
    }

    /**
     * Returns the server the message is associated to, this can be either the server it was send on or the server
     * specified by a !context command in a direct message
     *
     * @return the message events server
     * @throws NotAServerException if the message was not send on a server and no context was specified
     */
    @Nonnull
    protected final Guild getGuild() throws NotAServerException {
        if (event.isFromGuild()) {
            return event.getGuild();
        } else {
            if (contexts.getData().containsKey(getPrivateChannel())) {
                return contexts.getData().get(getPrivateChannel());
            } else {
                throw new NotAServerException("This message did not happen on a Server");
            }
        }
    }

    /**
     * Returns the server member who has send the message, this might be derived from a direct message with a associated
     * server context
     *
     * @return the sender
     * @throws NotAServerException if the message was not send on a server and no context was specified
     */
    protected final Member getSenderAsMember() throws NotAServerException {
        if (event.isFromGuild()) {
            return event.getMember();
        } else {
            return getGuild().getMember(event.getAuthor());
        }
    }

    /**
     * Returns the user who send the message
     *
     * @return the sender
     */
    @Nonnull
    protected final User getSenderAsUser() {
        return event.getAuthor();
    }

    /**
     * Returns the receive event, of the message in which the command was send
     *
     * @return command's message's receive event
     */
    final MessageReceivedEvent getEvent() {
        return event;
    }

    /**
     * Returns the Channel the message was send in.
     *
     * @return the command's message's channel
     */
    @Nonnull
    protected final MessageChannel getChannel() {
        return event.getChannel();
    }

    /**
     * Returns the private channel of the message in the command was, if the command was send in a pm
     *
     * @return the command's channel
     * @throws IllegalStateException if the message was not send in a private channel
     */
    @Nonnull
    protected final PrivateChannel getPrivateChannel() throws IllegalStateException {
        return event.getPrivateChannel();
    }

    /**
     * Downloads a file attached to the message, if there are multiple attachments, select the first
     *
     * @param targetPath path to store the download to
     * @throws NoAttachmentException if the message has no attachment
     * @throws CompletionException   if there was an exception while downloading the file
     */
    protected final void downloadAttachment(String targetPath) throws NoAttachmentException, CompletionException {
        if (event.getMessage().getAttachments().size() == 0)
            throw new NoAttachmentException("This message has no file attachment");
        if (event.getMessage().getAttachments().size() > 1) logger.info("Message has multible attachments, choosing first.");
        CompletableFuture<File> download = event.getMessage().getAttachments().get(0).downloadToFile(targetPath);
        download.join();
    }

    protected JDA getJDA() {
        return event.getJDA();
    }
}

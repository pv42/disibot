package disibot.command;

import disibot.DisiBot;
import disibot.context.ContextStorage;
import disibot.util.Command;
import net.dv8tion.jda.api.entities.GuildChannel;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.List;

public class MainCommandHandler extends CommandHandler {
    static final String GENERAL_HELP = "```" +
            "DisiBot Usage:\n" +
            "!help       prints this help\n" +
            "/emote      handles emotes\n" +
            "/permission handles permissions\n" +
            "/coinflip   flips a coin\n" +
            "!version    shows current version\n" +
            "/custom     configures custom commands\n" +
            "!welcome    configures welcome message\n" +
            "/lol        LoL commands\n" +
            "/apex       Apex commands\n" +
            "/poll       creates polls\n" +
            "!soundboard creates a soundboard command\n" +
            "```";
    static final String SUDO_NOT_ALLOWED = " is not in the sudoers file. This incident will be reported";
    static final String GENERAL_UNKNOWN = "```Unknown command, try !help```";
    private static final Logger LOG = LoggerFactory.getLogger(MainCommandHandler.class);
    private final List<CommandHandler> commandHandlers = new ArrayList<>();

    public MainCommandHandler(@Nonnull ContextStorage contextStorage) {
        super(contextStorage);
    }


    @Override
    public boolean canHandleCommand(@Nonnull Command command, @Nonnull MessageReceivedEvent ev) {
        return command.toString().startsWith("!") && !ev.getAuthor().isBot();
    }

    @Override
    public void handle(@Nonnull Command command) {
        LOG.info("Handling command:" + command);
        for (CommandHandler handler : commandHandlers) {
            if (handler.canHandleCommand(command, getEvent())) {
                handler.handleCommand(command, getEvent());
                return;
            }
        }
        String part = command.nextString();
        switch (part) {
            case "help":
                reply(GENERAL_HELP);
                break;
            case "version":
            case "about":
                reply("DisiBot " + DisiBot.VERSION + " by pv42#6061");
                break;
            case "sudo":
                if (getSenderAsUser().getIdLong() == 332225171601620992L) {
                    try {
                        getServer().elevate(getSenderAsMember());
                        if (command.hasNext()) {
                            this.handle(command);
                        } else {
                            reply(GENERAL_UNKNOWN);
                        }
                        getServer().ground(getSenderAsMember());
                    } catch (NotAServerException e) {
                        reply("Invalid context");
                    }
                } else {
                    reply(getSenderAsUser().getName() + SUDO_NOT_ALLOWED);
                }
                break;
            default:
                try {
                    switch (part) {
                        case "dumproles":
                            dumpRoles();
                            break;
                        case "dumpmembers":
                            dumpMembers();
                            break;
                        case "dumpusers":
                            dumpUsers();
                            break;
                        case "dumpchannels":
                            dumpChannels();
                            break;
                        default:
                            reply(GENERAL_UNKNOWN);
                            break;
                    }
                } catch (NotAServerException ignored) {
                    reply("Must be executed on a server");
                }
                break;
        }
    }

    private void dumpRoles() throws NotAServerException {
        if (getServer().isElevated(getSenderAsMember())) {

            for (Role role : getGuild().getRoles()) {
                reply(role.getName().replaceAll("@", "") + ":" + role.getId());
            }

        } else {
            reply("Permission denied");
        }
    }

    private void dumpMembers() throws NotAServerException {
        if (getServer().isElevated(getSenderAsMember())) {

            for (Member member : getGuild().getMembers()) {
                reply(member.getEffectiveName().replaceAll("@", "") + ":" + member.getId());
            }

        } else {
            reply("Permission denied");
        }
    }

    private void dumpUsers() throws NotAServerException {
        if (getServer().isElevated(getSenderAsMember())) {

            for (Member member : getGuild().getMembers()) {
                reply(member.getUser().getName().replaceAll("@", "") + ":" + member.getUser().getId());
            }

        } else {
            reply("Permission denied");
        }
    }

    private void dumpChannels() throws NotAServerException {
        if (getServer().isElevated(getSenderAsMember())) {
            for (GuildChannel channel : getGuild().getChannels()) {
                reply(channel.getName() + ":" + channel.getId());
            }
        } else {
            reply("Permission denied");
        }
    }

    public void addCommandHandler(CommandHandler handler) {
        commandHandlers.add(handler);
    }

    public void removeCommandHandler(CommandHandler handler) {
        commandHandlers.remove(handler);
    }
}

package disibot.command;

import disibot.util.Command;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.jetbrains.annotations.NotNull;

import javax.annotation.Nonnull;

import static disibot.util.DiscordUtil.sendException;

public class MainCommandHandlerEventListener extends ListenerAdapter {
    private final MainCommandHandler commandHandler;
    private static final boolean dumpExceptions = true;

    public MainCommandHandlerEventListener(MainCommandHandler commandHandler) {
        this.commandHandler = commandHandler;
    }

    @Override
    public void onMessageReceived(@Nonnull MessageReceivedEvent event) {
        try {
            Message msg = event.getMessage();
            if (msg.getContentRaw().startsWith("!") && !msg.getAuthor().equals(event.getJDA().getSelfUser())) {
                String cmd = msg.getContentRaw().substring(1);
                if (commandHandler == null) return; // bot is in startup
                commandHandler.handleCommand(new Command(cmd), event);
            }
        } catch (Exception e) {
            if (dumpExceptions) sendException(e, event.getChannel());
            e.printStackTrace();
        }
    }
}

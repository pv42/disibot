package disibot.config;

import disibot.context.ContextStorage;
import disibot.modules.DisiBotModuleInterface;
import disibot.modules.DisiBotModule;
import org.jetbrains.annotations.NotNull;

import javax.annotation.Nonnull;

public class ConfigModule extends DisiBotModule {
    private final ConfigCommandHandler commandHandler;
    public ConfigModule(ContextStorage contextStorage, @Nonnull ConfigStorage configStorage) {
        commandHandler = new ConfigCommandHandler(contextStorage, configStorage);
    }
    @Override
    public void load(@Nonnull @NotNull DisiBotModuleInterface moduleInterface) {
        moduleInterface.addCommandHandler(this, commandHandler);
    }
}

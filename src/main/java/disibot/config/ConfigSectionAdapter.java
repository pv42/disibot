package disibot.config;

import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;
import java.util.Map;

public class ConfigSectionAdapter extends TypeAdapter<ConfigSection> {
    private static final Gson gson = new Gson();

    @Override
    public void write(JsonWriter out, ConfigSection section) throws IOException {
        gson.toJson(section.configValues, Map.class, out);
    }

    @Override
    public ConfigSection read(JsonReader in) throws IOException {
        TypeToken<Map<String,String>> strStrMap = new TypeToken<>(){};
        Map<String, String> configValues = gson.fromJson(in,strStrMap.getType());
        ConfigSection section = new ConfigSection(null) {};
        section.configValues = configValues;
        return section;
    }
}

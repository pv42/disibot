package disibot.config;

import disibot.DisiBot;
import disibot.command.CommandHandler;
import disibot.command.InsufficientPowerException;
import disibot.command.NotAServerException;
import disibot.context.ContextStorage;
import disibot.util.Command;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

import javax.annotation.Nonnull;

public class ConfigCommandHandler extends CommandHandler {
    private final ConfigStorage configStorage;

    public ConfigCommandHandler(ContextStorage contexts, ConfigStorage configStorage) {
        super(contexts);
        this.configStorage = configStorage;
    }

    @Override
    public boolean canHandleCommand(@Nonnull Command command, @Nonnull MessageReceivedEvent event) {
        return command.peekString().equals("config");
    }

    @Override
    protected void handle(@Nonnull Command command) {
        try {
            handleVolatile(command);
        } catch (InsufficientPowerException e) {
            reply(DisiBot.getString("permission_denied"));
        } catch (NotAServerException e) {
            reply(DisiBot.getString("server_only_command"));
        }
    }

    private void handleVolatile(Command command) throws InsufficientPowerException, NotAServerException {
        if (!getServer().isElevated(getSenderAsMember())) {
            throw new InsufficientPowerException("Must be root.");
        }
        command.nextString(); // skip config
        String subcmd = command.nextString();
        String sectionName;
        String valueName;
        switch (subcmd) {
            case "get":
                sectionName = command.nextString();
                if (!configStorage.hasSection(sectionName)) {
                    reply(DisiBot.getString("config_section_not_exists"));
                    return;
                }
                valueName = command.nextString();
                ConfigSection section = configStorage.getSection(sectionName);
                //noinspection ConstantConditions the hasSection ensures it exists
                if (!section.configValues.containsKey(valueName)) {
                    reply(DisiBot.getString("config_value_not_exists"));
                    return;
                }
                reply("`" + sectionName + "::" + valueName + " = " + section.configValues.get(valueName) + "`");
                break;
            case "set":
                sectionName = command.nextString();
                if (!configStorage.hasSection(sectionName)) {
                    reply(DisiBot.getString("config_section_not_exists"));
                    return;
                }
                valueName = command.nextString();
                section = configStorage.getSection(sectionName);
                //noinspection ConstantConditions the hasSection ensures it exists
                if (!section.configValues.containsKey(valueName)) {
                    reply(DisiBot.getString("config_value_not_exists"));
                    return;
                }
                String value = command.nextString();
                section.configValues.put(valueName, value);
                reply(DisiBot.getString("config_value_set"));
                break;
            case "section":
                sectionName = command.nextString();
                if (!configStorage.hasSection(sectionName)) {
                    reply(DisiBot.getString("config_section_not_exists"));
                    return;
                }
                section = configStorage.getSection(sectionName);
                StringBuilder message = new StringBuilder("Sections:\n");
                //noinspection ConstantConditions the hasSection ensures it exists
                for (String name : section.configValues.keySet()) {
                    message.append(name);
                    message.append(", ");
                }
                reply(message.substring(0, message.length() - 2).toString());
                break;
            case "sections":
                message = new StringBuilder(DisiBot.getString("config_sections"));
                message.append('\n');
                for (String name : configStorage.getSectionNames()) {
                    message.append(name);
                    message.append(", ");
                }
                reply(message.substring(0, message.length() - 2));
                break;
            default:
                reply(DisiBot.getString("config_syntax"));
        }
    }
}

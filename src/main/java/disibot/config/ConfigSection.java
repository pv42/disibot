package disibot.config;

import java.util.HashMap;
import java.util.Map;

public abstract class ConfigSection {
    Runnable saveMethod;
    Map<String,String> configValues;
    private final Map<String,Object> convertCache = new HashMap<>();

    ConfigSection(Void v) {} // exists as an do nothing constructor for deserialization

    protected ConfigSection() {
        configValues = new HashMap<>();
    }

    protected int getInt(String name) {
        if(convertCache.containsKey(name)) return (Integer) convertCache.get(name);
        int value = Integer.parseInt(configValues.get(name));
        convertCache.put(name, value);
        return value;
    }

    protected void setInt(String name, int value) {
        convertCache.put(name,value);
        configValues.put(name,String.valueOf(value));
    }

    protected boolean getBoolean(String name) {
        if(convertCache.containsKey(name)) return (Boolean) convertCache.get(name);
        boolean value = Boolean.parseBoolean(configValues.get(name));
        convertCache.put(name, value);
        return value;
    }

    protected void setBoolean(String name, boolean value) {
        convertCache.put(name,value);
        configValues.put(name,String.valueOf(value));
    }

    protected void setSaveMethod(Runnable saveMethod) {
        this.saveMethod = saveMethod;
    }
    protected void save() {
        saveMethod.run();
    }
}

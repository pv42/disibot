package disibot.config;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class ConfigStorage {
    private final Map<String, ConfigSection> sections;
    private transient Runnable saveMethod;

    public ConfigStorage(Runnable saveMethod) {
        this.sections = new HashMap<>();
        this.saveMethod = saveMethod;
    }

    @Nullable
    public ConfigSection getSection(@Nonnull String sectionName) {
        return sections.get(sectionName);
    }

    public void createSection(@Nonnull String sectionName, ConfigSection section) {
        if (!hasSection(sectionName)) {
            sections.put(sectionName, section);
            section.setSaveMethod(saveMethod);
        }
    }

    public boolean hasSection(@Nonnull String sectionName) {
        return sections.containsKey(sectionName);
    }

    public void setSaveMethod(Runnable saveToDisk) {
        this.saveMethod = saveToDisk;
    }

    public Set<String> getSectionNames() {
        return sections.keySet();
    }
}

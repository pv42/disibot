package disibot.poll;

import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.interactions.InteractionHook;

import javax.annotation.Nonnull;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Poll {
    private final String title;
    private final List<String> options;
    private final Map<User, String> votes = new HashMap<>();
    private final boolean allowChangeVote;
    private InteractionHook pollInteractionHook;

    public Poll(String title, List<String> options) {
        this.title = title;
        this.options = options;
        this.allowChangeVote = false;
    }

    public String getTitle() {
        return title;
    }

    public List<String> getOptions() {
        return options;
    }

    public boolean vote(User voter, String vote) {
        if (votes.containsKey(voter) && !allowChangeVote) return false;
        votes.put(voter, vote);
        return true;
    }

    public Map<String, Integer> countVotes() {
        Map<String, Integer> result = new HashMap<>();
        for (String option : options) {
            result.put(option, 0);
        }
        for (String option : votes.values()) {
            result.put(option, result.get(option) + 1);
        }
        return result;
    }

    public InteractionHook getPollInteractionHook() {
        return pollInteractionHook;
    }

    public void setPollInteractionHook(@Nonnull InteractionHook pollInteractionHook) {
        if(this.pollInteractionHook != null) throw new UnsupportedOperationException("Can't change a polls associated InteractionHook");
        this.pollInteractionHook = pollInteractionHook;
    }
}

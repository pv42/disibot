package disibot.poll;

import disibot.modules.DisiBotModule;
import disibot.modules.DisiBotModuleInterface;

import javax.annotation.Nonnull;

public class PollModule extends DisiBotModule {
    private final PollSlashCommandHandler commandHandler;

    public PollModule() {
        commandHandler = new PollSlashCommandHandler();
    }

    @Override
    public void load(@Nonnull DisiBotModuleInterface moduleInterface) {
        moduleInterface.addSlashCommandHandler(this, commandHandler);
        moduleInterface.addDiscordListenerAdapter(this, commandHandler);
    }
}

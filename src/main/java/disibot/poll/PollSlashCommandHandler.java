package disibot.poll;

import disibot.DisiBot;
import disibot.newcommand.SlashCommandHandler;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.Emoji;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.events.interaction.ButtonClickEvent;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.dv8tion.jda.api.interactions.commands.OptionMapping;
import net.dv8tion.jda.api.interactions.commands.OptionType;
import net.dv8tion.jda.api.interactions.commands.build.CommandData;
import net.dv8tion.jda.api.interactions.commands.build.OptionData;
import net.dv8tion.jda.api.interactions.components.ActionRow;
import net.dv8tion.jda.api.interactions.components.Button;
import net.dv8tion.jda.api.interactions.components.Component;
import net.dv8tion.jda.api.requests.restaction.interactions.ReplyAction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PollSlashCommandHandler extends ListenerAdapter implements SlashCommandHandler {
    private static final Logger LOG = LoggerFactory.getLogger(PollSlashCommandHandler.class);
    Map<Long, Poll> activePolls = new HashMap<>();

    @Override
    public void handle(@Nonnull SlashCommandEvent ev) {
        OptionMapping titleOM = ev.getOption("title");
        if (titleOM == null) {
            ev.reply(DisiBot.getString("internal_error")).queue();
            return;
        }
        List<String> options = new ArrayList<>();
        for (int i = 1; i <= 5; i++) {
            OptionMapping optionMapping = ev.getOption("option" + i);
            if (optionMapping == null) continue;
            options.add(optionMapping.getAsString());
        }
        Poll poll = new Poll(titleOM.getAsString(), options);
        ReplyAction replyInteraction = ev.replyEmbeds(getPollEmbed(poll));
        List<Component> buttons = new ArrayList<>();
        for (int i = 0; i < poll.getOptions().size(); i++) {
            String option = poll.getOptions().get(i);
            if (option.matches(Message.MentionType.EMOTE.getPattern().pattern())) {
                buttons.add(Button.secondary(option, Emoji.fromMarkdown(option)));
            } else {
                // else remove emotes
                buttons.add(Button.secondary(option, option.replaceAll(Message.MentionType.EMOTE.getPattern().pattern(), "")));
            }
        }
        replyInteraction.addActionRows(ActionRow.of(buttons)).queue(poll::setPollInteractionHook);
        activePolls.put(ev.getChannel().getIdLong(), poll);
    }

    @Nonnull
    private MessageEmbed getPollEmbed(@Nonnull Poll poll) {
        EmbedBuilder eb = new EmbedBuilder();
        Map<String, Integer> votes = poll.countVotes();
        for (String optionName : poll.getOptions()) {
            eb.addField(optionName, votes.get(optionName) + " votes", false);
        }
        eb.setTitle(poll.getTitle());
        return eb.build();
    }

    @Override
    public CommandData getCommand() {
        CommandData cd = new CommandData("poll", "Creates a poll, valid for 15 minutes");
        cd.addOptions(new OptionData(OptionType.STRING, "title", "Title of the poll", true));
        cd.addOptions(new OptionData(OptionType.STRING, "option1", "First choice", true));
        cd.addOptions(new OptionData(OptionType.STRING, "option2", "Second choice", true));
        cd.addOptions(new OptionData(OptionType.STRING, "option3", "Third optional choice"));
        cd.addOptions(new OptionData(OptionType.STRING, "option4", "Fourth optional choice"));
        cd.addOptions(new OptionData(OptionType.STRING, "option5", "Fifth optional choice"));
        return cd;
    }

    @Override
    public void onButtonClick(@Nonnull ButtonClickEvent event) {
        Poll poll = activePolls.get(event.getChannel().getIdLong());
        if (poll == null || poll.getPollInteractionHook().isExpired()) {
            event.reply("This poll has expired").setEphemeral(true).queue();
            return;
        }
        if (event.getMessageIdLong() != poll.getPollInteractionHook().retrieveOriginal().complete().getIdLong()) {
            event.reply("Active poll mismatch @pv42").queue();
            return;
        }
        Button button = event.getButton();
        if (button == null) {
            event.reply(DisiBot.getString("internal_error")).queue();
            return;
        }
        if (poll.vote(event.getUser(), button.getId())) {
            poll.getPollInteractionHook().editOriginalEmbeds(getPollEmbed(poll)).queue();
            event.reply("You voted!").setEphemeral(true).queue();
        } else {
            event.reply("You can not vote again!").setEphemeral(true).queue();
        }
    }
}

package disibot.antidel;

import disibot.modules.DisiBotModule;
import disibot.modules.DisiBotModuleInterface;
import org.jetbrains.annotations.NotNull;

public class AntiDelModule extends DisiBotModule {
    @Override
    public void load(@NotNull DisiBotModuleInterface moduleInterface) {
        moduleInterface.addDiscordListenerAdapter(this, new AntiDelEventListener());
    }
}

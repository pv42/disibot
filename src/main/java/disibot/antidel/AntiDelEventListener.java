package disibot.antidel;

import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageReaction;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.events.message.guild.react.GuildMessageReactionRemoveEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.jetbrains.annotations.NotNull;

public class AntiDelEventListener extends ListenerAdapter {
    @Override
    public void onGuildMessageReactionRemove(@NotNull GuildMessageReactionRemoveEvent event) {
        TextChannel channel = event.getChannel();
        if(event.getReaction().isSelf()) {
            Message message = channel.getHistory().getMessageById(event.getMessageIdLong());
            if(message == null) message = channel.retrieveMessageById(event.getMessageIdLong()).complete();
            if(message == null) return; // message removed or not accessible
            MessageReaction.ReactionEmote reactionEmote = event.getReactionEmote();
            if(reactionEmote.isEmoji()) {
                message.addReaction(reactionEmote.getEmoji()).complete();
            } else {
                message.addReaction(reactionEmote.getEmote()).complete();
            }
        }
    }
}

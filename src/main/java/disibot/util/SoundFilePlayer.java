package disibot.util;

import net.dv8tion.jda.api.audio.AudioSendHandler;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.VoiceChannel;
import net.dv8tion.jda.api.managers.AudioManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.IOException;

public class SoundFilePlayer {
    private static final Logger LOG = LoggerFactory.getLogger(SoundFilePlayer.class);
    private SoundFilePlayer() {} // no con
    public static void playSoundFile(String filename, VoiceChannel channel, Guild guild) throws IOException, UnsupportedAudioFileException {
        AudioManager manager = guild.getAudioManager();
        AudioSendHandler sendHandler = new FilePlayerAudioSendHandler(filename);
        manager.setSendingHandler(sendHandler);
        manager.openAudioConnection(channel);
        long start = System.currentTimeMillis();
        Thread stopperThread = new Thread(() -> {
            while (sendHandler.canProvide()) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            LOG.info("Sound stopped after " + (System.currentTimeMillis() - start) + "ms");
            manager.closeAudioConnection();
        });
        stopperThread.setDaemon(true);
        stopperThread.start();
    }
}

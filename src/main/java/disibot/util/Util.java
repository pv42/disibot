package disibot.util;


import javax.annotation.Nonnull;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDateTime;

import static java.time.ZoneOffset.UTC;

public class Util {
    private Util(){}

    public static @Nonnull LocalDateTime ofEpochMillis(long epochMillis) {
        return LocalDateTime.ofEpochSecond(epochMillis / 1000, (int) ((epochMillis % 1000) * 1000000), UTC);
    }

    @Nonnull
    public static String readTokenFile(String path) throws IOException {
        String api_key;
        FileReader fr = new FileReader(path);
        char[] buffer = new char[512];
        int r = fr.read(buffer);
        if (r < 0) throw new IOException("Could not read file.");
        api_key = String.valueOf(buffer);
        api_key = api_key.substring(0, r);
        fr.close();
        api_key = api_key.replaceAll("\n", "");
        return api_key;
    }
}

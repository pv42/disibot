package disibot.util;

import net.dv8tion.jda.api.entities.MessageChannel;

import javax.annotation.Nonnull;
import java.io.OutputStream;
import java.io.PrintStream;

public class DiscordUtil {
    private DiscordUtil() {}

    /**
     * Sends any throwables (usually an exceptions) stacktrace and message into a discord text channel, this uses the
     * same format as used in printStacktrace()
     *
     * @param e       Throwable to send
     * @param channel channel, in which to send
     */
    public static void sendException(@Nonnull Throwable e, MessageChannel channel) {
        final StringBuilder msg = new StringBuilder();
        OutputStream s = new OutputStream() {
            @Override
            public void write(int b) {
                msg.append((char) b);
            }
        };
        e.printStackTrace(new PrintStream(s));
        sendLongMessage(msg.toString(), channel);
    }

    /**
     * Allows to send messages with over 2000 characters by dividing it into smaller messages at linebreaks,
     * if the message is a code block (defined by beginning and ending with ```)
     *
     * @param message message to send
     * @param channel channel to send the message in
     */
    public static void sendLongMessage(@Nonnull String message, MessageChannel channel) {
        int maxlen = 2000;
        boolean isCode = message.startsWith("```") && message.endsWith("```");
        if (isCode) {
            message = message.replaceAll("```", "");
            maxlen -= 6;
        }
        while (message.length() > maxlen) {
            int i = message.substring(0, maxlen - 1).lastIndexOf("\n");
            if (i == -1) i = maxlen - 1;
            sendLongPart(message.substring(0, i + 1), channel, isCode);
            message = message.substring(i + 1);
        }
        sendLongPart(message, channel, isCode);
    }

    private static void sendLongPart(String msgPart, MessageChannel channel, boolean isCode) {
        if (isCode) {
            channel.sendMessage("```" + msgPart + "```").queue();
        } else {
            channel.sendMessage(msgPart).queue();
        }
    }

    public static boolean isStringCustomEmote(String str) {
        return str.matches("<a?:\\S+:\\d+>");
    }
}

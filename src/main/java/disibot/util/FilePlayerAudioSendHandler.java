package disibot.util;

import net.dv8tion.jda.api.audio.AudioSendHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nullable;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

public class FilePlayerAudioSendHandler implements AudioSendHandler {
    private static final Logger LOG = LoggerFactory.getLogger(AudioSendHandler.class);
    private AudioInputStream stream;
    private List<ByteBuffer> parts;
    private int partIndex = 0;

    public static boolean canPlayFile(String filename) {
        File file = new File(filename);
        try {
            AudioInputStream s = AudioSystem.getAudioInputStream(file);
        } catch (UnsupportedAudioFileException | IOException e) {
            return false;
        }
        return true;
    }

    public FilePlayerAudioSendHandler(String filename) throws IOException, UnsupportedAudioFileException {
        load(filename);
        prepare();
    }

    private void load(String filename) throws IOException, UnsupportedAudioFileException {
        File file = new File(filename);
        AudioInputStream loadedStream = AudioSystem.getAudioInputStream(file);
        stream = AudioSystem.getAudioInputStream(FilePlayerAudioSendHandler.INPUT_FORMAT, loadedStream);
        LOG.info("loaded file " + filename + " with audio format:" + loadedStream.getFormat());
    }

    private void prepare() throws IOException {
        parts = new ArrayList<>();
        int bytesToRead = (int) (INPUT_FORMAT.getFrameRate() * INPUT_FORMAT.getFrameSize() * 0.02);
        int bytesPerSample = INPUT_FORMAT.getSampleSizeInBits()/8;
        while (true) {
            byte[] buffer = new byte[bytesToRead];
            int bytesRead = stream.read(buffer);
            if(bytesRead <= bytesPerSample) break;
            if (bytesRead != bytesToRead)  {
                for(int i = bytesRead; i < bytesToRead; i += INPUT_FORMAT.getSampleSizeInBits()/8) {
                    System.arraycopy(buffer, bytesRead - bytesPerSample, buffer, i, bytesPerSample);
                }
            }
            parts.add(ByteBuffer.wrap(buffer, 0, bytesToRead));
            if (bytesRead != bytesToRead) break;
        }
        System.out.println(parts.size() + " parts loaded");
    }

    @Override
    public boolean canProvide() {
        return partIndex < parts.size();
    }


    @Nullable
    @Override
    public ByteBuffer provide20MsAudio() {
        ByteBuffer part = parts.get(partIndex);
        partIndex++;
        //partIndex %= parts.size(); // loop
        return part;
    }
}

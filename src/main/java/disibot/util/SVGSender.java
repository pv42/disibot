package disibot.util;

import net.dv8tion.jda.api.entities.MessageChannel;
import org.apache.batik.transcoder.TranscoderException;
import org.apache.batik.transcoder.TranscoderInput;
import org.apache.batik.transcoder.TranscoderOutput;
import org.apache.batik.transcoder.image.PNGTranscoder;

import javax.annotation.Nonnull;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;


/**
 * convers and sends a svg
 *
 * @author pv42
 */
public class SVGSender {
    public static void send(@Nonnull String svg, @Nonnull MessageChannel channel) throws TranscoderException, IOException {
        byte[] pngData = convertSVGtoPNG(svg.getBytes());
        channel.sendFile(pngData, "image.png").queue();
    }

    @Nonnull
    private static byte[] convertSVGtoPNG(@Nonnull byte[] svgData) throws IOException, TranscoderException {
        PNGTranscoder transcoder = new PNGTranscoder();
        TranscoderInput input = new TranscoderInput(new ByteArrayInputStream(svgData));
        ByteArrayOutputStream outStream = new ByteArrayOutputStream();
        TranscoderOutput output = new TranscoderOutput(outStream);
        transcoder.transcode(input, output);
        outStream.flush();
        return outStream.toByteArray();
    }
}

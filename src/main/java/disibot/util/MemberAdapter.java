package disibot.util;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;

import java.io.IOException;

public class MemberAdapter extends TypeAdapter<Member> {
    private final JDA jda;

    public MemberAdapter(JDA jda) {
        this.jda = jda;
    }

    @Override
    public void write(JsonWriter writer, Member member) throws IOException {
        writer.value(member.getGuild().getIdLong() + ":" + member.getIdLong());
    }

    @Override
    public Member read(JsonReader in) throws IOException {
        if (jda == null) throw new IOException("Can't parse user without a jda");
        String[] parts = in.nextString().split(":");
        Guild guild = jda.getGuildById(parts[0]);
        if(guild == null) throw new IOException("Could not get guild with id '" + parts[0]+ "'");
        return guild.getMemberById(parts[1]);
    }
}

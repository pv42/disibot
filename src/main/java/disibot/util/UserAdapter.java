package disibot.util;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.User;

import java.io.IOException;

public class UserAdapter extends TypeAdapter<User> {

    private JDA jda;

    public UserAdapter(JDA jda) {
        this.jda = jda;
    }

    @Override
    public void write(JsonWriter writer, User user) throws IOException {
        writer.value(user.getIdLong());
    }

    @Override
    public User read(JsonReader in) throws IOException {
        if (jda == null) throw new IOException("Can't parse user without a jda");
        long id = in.nextLong();
        return jda.getUserById(id);
    }
}

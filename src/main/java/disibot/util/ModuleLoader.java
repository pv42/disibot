package disibot.util;

import disibot.modules.ModuleFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.impl.SimpleLogger;

import javax.annotation.Nullable;
import java.io.File;
import java.io.IOException;
import java.lang.instrument.Instrumentation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.NoSuchFileException;
import java.util.jar.Attributes;
import java.util.jar.JarFile;

public class ModuleLoader {
    static { // configure log
        System.setProperty(SimpleLogger.LOG_FILE_KEY, "disibot.log");
        System.setProperty(SimpleLogger.SHOW_SHORT_LOG_NAME_KEY, "true");
        System.setProperty(SimpleLogger.SHOW_DATE_TIME_KEY, "true");
        System.setProperty(SimpleLogger.DATE_TIME_FORMAT_KEY, "dd.MM HH:mm:ss:SSS");
    }
    private static final Logger logger = LoggerFactory.getLogger(ModuleLoader.class);
    private static Instrumentation instrumentation;

    public static void premain(String agentArgs, Instrumentation instrumentation) {
        ModuleLoader.instrumentation = instrumentation;
        logger.info("Instrumentation initialized");
    }

    @Nullable
    public static ModuleFactory loadModule(File jarFile) {
        try {
            JarFile jar = new JarFile(jarFile);
            Attributes manifestEntries = jar.getManifest().getMainAttributes();
            if(manifestEntries.containsKey(new Attributes.Name("disibot-module-module-factory-name"))) {
                loadJarUnsafe(jar, jarFile);
                String moduleFactoryClassName = manifestEntries.getValue(new Attributes.Name("disibot-module-module-factory-name"));
                Class<?> factoryClass = Class.forName(moduleFactoryClassName);
                Object factoryObject = factoryClass.getConstructor().newInstance();
                if(!(factoryObject instanceof ModuleFactory)) {
                    logger.error("Could not load module " + jarFile.getName() + ": Invalid factory class");
                } else {
                    logger.info("Module loaded: " + jarFile.getName());
                    return (ModuleFactory)factoryObject;
                }
            } else {
                logger.error("Could not load module " + jarFile.getName() + ": Invalid manifest");
            }
        } catch (ReflectiveOperationException | UnsupportedClassVersionError e) {
            logger.error("Could not load module " + jarFile.getName() + ": Could not access class loader: " + e.getMessage());
        } catch (NoSuchFileException e) {
            logger.error("Could not load module " + jarFile.getName() + ": NoSuchFileException:\n" + e.getMessage());
        } catch (IOException e) {
            logger.error("Could not load module " + jarFile.getName() + ": IOException:\n" + e.getMessage());
            e.printStackTrace();
        }
        return null;
    }

    private static void loadJarUnsafe(JarFile jar, File jarFile) throws NoSuchMethodException, MalformedURLException, InvocationTargetException, IllegalAccessException, UnsupportedClassVersionError {
        if(instrumentation != null) {
            instrumentation.appendToSystemClassLoaderSearch(jar);
        } else {
            logger.warn("Instrumentation not initialized, using reflection fall back.");
            ClassLoader classLoader = ClassLoader.getSystemClassLoader();
            if(!(classLoader instanceof URLClassLoader)) throw new UnsupportedClassVersionError("Can not use reflection with the current SystemClassLoader");
            Method addUrlMethod = URLClassLoader.class.getDeclaredMethod("addURL", URL.class);
            addUrlMethod.setAccessible(true);
            addUrlMethod.invoke(classLoader, jarFile.toURI().toURL());
        }
    }
}

package disibot.util;

import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.Emote;

import javax.annotation.Nonnull;

public class Command {
    private final String complete;
    private int pos;
    private String next;

    public Command(String commandString) {
        this.complete = commandString;
        pos = 0;
    }

    public boolean hasNext() {
        consumeWhiteSpaces();
        return next != null || pos < complete.length();
    }

    @Nonnull
    public String nextString() {
        if (next != null) {
            String part = next;
            next = null;
            return part;
        }
        consumeWhiteSpaces();
        return unitlNextWhiteSpace();
    }

    public int nextInt() throws NumberFormatException {
        return Integer.parseInt(nextString());
    }

    @Nonnull
    public String peekString() {
        if (next != null) return next;
        consumeWhiteSpaces();
        next = unitlNextWhiteSpace();
        return next;
    }

    public boolean consumeChar(char c) {
        boolean isValid = complete.charAt(pos) == c;
        if (isValid) pos++;
        return isValid;
    }

    public String remaining() {
        consumeWhiteSpaces();
        if (pos >= complete.length()) return "";
        int oldPos = pos;
        pos = complete.length();
        return complete.substring(oldPos);
    }

    @Nonnull
    private String unitlNextWhiteSpace() {
        int seekPos = pos;
        boolean escaped = false; // prev char was \
        boolean enclosed = false; // inside ""
        StringBuilder output = new StringBuilder();
        while (seekPos < complete.length()) {
            if (escaped) {
                output.append(complete.charAt(seekPos));
                escaped = false;
            } else if (Character.isWhitespace(complete.charAt(seekPos)) && !enclosed) {
                break;
            } else if (complete.charAt(seekPos) == '\\') {
                escaped = true;
            } else if (complete.charAt(seekPos) == '"') {
                enclosed = !enclosed;
            } else {
                output.append(complete.charAt(seekPos));
            }
            seekPos++;
        }
        pos = seekPos;
        return output.toString();
    }

    private void consumeWhiteSpaces() {
        while (pos < complete.length() && Character.isWhitespace(complete.charAt(pos))) {
            pos++;
        }
    }

    @Override
    public String toString() {
        return complete;
    }

    public boolean isNextCustomEmote() {
        return DiscordUtil.isStringCustomEmote(peekString());
    }

    public Emote nextEmote(JDA jda) {
        if(!isNextCustomEmote()) return null;
        return jda.getEmoteById(nextString().split(":")[2]);
    }
}

package disibot.context;

import disibot.context.ContextCommandHandler;
import disibot.context.ContextStorage;
import disibot.modules.DisiBotModuleInterface;
import disibot.modules.DisiBotModule;
import net.dv8tion.jda.api.JDA;
import org.jetbrains.annotations.NotNull;

import javax.annotation.Nonnull;

public class ContextModule extends DisiBotModule {
    private final ContextCommandHandler commandHandler;

    public ContextModule(ContextStorage contextStorage, JDA jda) {
        this.commandHandler = new ContextCommandHandler(contextStorage, jda);
    }

    @Override
    public void load(@Nonnull @NotNull DisiBotModuleInterface moduleInterface) {
        moduleInterface.addCommandHandler(this, commandHandler);
    }
}

package disibot.context;

import disibot.DisiBot;
import disibot.command.CommandHandler;
import disibot.server.Server;
import disibot.server.ServerFactory;
import disibot.util.Command;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.ChannelType;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.PrivateChannel;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class ContextCommandHandler extends CommandHandler {
    private final ContextStorage storage;
    private final JDA jda;

    public ContextCommandHandler(ContextStorage storage, JDA jda) {
        super(storage);
        this.storage = storage;
        this.jda = jda;
    }

    @Override
    public boolean canHandleCommand(@NotNull Command command, @NotNull MessageReceivedEvent ev) {
        return command.peekString().equals("context");
    }

    @Override
    protected void handle(@NotNull Command command) {
        command.nextString(); // consume "custom"
        if (getChannel().getType() != ChannelType.PRIVATE) {
            reply(DisiBot.getString("context_private_channel_only"));
            return;
        }
        PrivateChannel channel = getPrivateChannel();
        //String[] arguments = command.split("\\s");
        if (!command.hasNext()) {
            reply(DisiBot.getString("context_syntax_error"));
            return;
        }
        switch (command.nextString()) {
            case "help":
                reply(DisiBot.getString("context_help"));
                break;
            case "get":
                if (storage.getData().containsKey(channel)) {
                    reply(String.format(DisiBot.getString("context_context_is") , storage.getData().get(channel).getName()));
                } else {
                    reply(DisiBot.getString("context_no_context_set"));
                }
                break;
            case "set":
                handleSet(command);
                break;
            default:
                reply(DisiBot.getString("context_syntax_error"));
                break;
        }
    }

    private void handleSet(Command command) {
        PrivateChannel channel = getPrivateChannel();
        if (command.hasNext()) {
            String servername = command.remaining();
            setContext(servername, channel);
        } else {
            if (storage.getData().containsKey(channel)) {
                storage.getData().remove(channel);
                reply(DisiBot.getString("context_context_removed"));
            } else {
                reply(DisiBot.getString("context_cant_remove_not_set"));
            }
        }
    }

    private void setContext(String servername, PrivateChannel channel) {
        User user = getSenderAsUser();
        List<Guild> serverMatches = new ArrayList<>();
        for (Guild server : jda.getGuildsByName(servername, true)) {
            if (server.isMember(user)) {
                serverMatches.add(server);
            }
        }
        Guild serverMatch;
        if (serverMatches.size() == 0) {
            reply(String.format(DisiBot.getString("context_not_a_member"),servername));
            return;
        } else if (serverMatches.size() == 1) {
            serverMatch = serverMatches.get(0);
        } else {
            reply(String.format(DisiBot.getString("context_multiple_member"),servername));
            return;
        }
        Server server = ServerFactory.get(serverMatch);
        Member member = serverMatch.getMember(user);
        if (!server.canUseAsContext(member)) {
            reply(DisiBot.getString("context_no_permission"));
        } else {
            storage.getData().put(channel, serverMatch);
            reply(String.format(DisiBot.getString("context_set"),serverMatch.getName()));
        }
    }
}

package disibot.context;

import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.PrivateChannel;

import java.util.HashMap;
import java.util.Map;



public class ContextStorage {
    private final Map<PrivateChannel, Guild> contexts;

    public ContextStorage() {
        this.contexts = new HashMap<>();
    }

    public Map<PrivateChannel, Guild> getData() {
        return contexts;
    }
}

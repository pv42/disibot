package fakejda;

import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.Emote;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.managers.EmoteManager;
import net.dv8tion.jda.api.requests.restaction.AuditableRestAction;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.File;
import java.util.List;

public class FakeEmote implements Emote {
    private final String name;
    private final String imagePath;

    public FakeEmote(String name, String imagePath) {
        this.name = name;
        this.imagePath = imagePath;
    }

    @Nullable
    @Override
    public Guild getGuild() {
        return null;
    }

    @Nonnull
    @Override
    public List<Role> getRoles() {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean canProvideRoles() {
        return false;
    }

    @Nonnull
    @Override
    public String getName() {
        return name;
    }

    @Override
    public boolean isManaged() {
        return false;
    }

    @Override
    public boolean isAvailable() {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public JDA getJDA() {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public AuditableRestAction<Void> delete() {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public EmoteManager getManager() {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean isAnimated() {
        return false;
    }

    @Override
    public long getIdLong() {
        return 0;
    }

    @Nonnull
    @Override
    public String getImageUrl() {
        String sep = File.separator.equals("\\") ? "\\\\" : "/";
        return "file:///" +System.getProperty("user.dir").replaceAll("\\\\","\\\\\\\\") + sep + imagePath;
    }
}

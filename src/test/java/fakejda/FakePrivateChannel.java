package fakejda;

import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.ChannelType;
import net.dv8tion.jda.api.entities.PrivateChannel;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.requests.RestAction;

import javax.annotation.Nonnull;

public class FakePrivateChannel extends FakeMessageChannel implements PrivateChannel {
    @Nonnull
    @Override
    public User getUser() {
        return null;
    }

    @Override
    public boolean hasLatestMessage() {
        return false;
    }

    @Nonnull
    @Override
    public String getName() {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public ChannelType getType() {
        return ChannelType.PRIVATE;
    }

    @Nonnull
    @Override
    public JDA getJDA() {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public RestAction<Void> close() {
        throw new UnsupportedOperationException();
    }

    @Override
    public long getIdLong() {
        return 0;
    }
}

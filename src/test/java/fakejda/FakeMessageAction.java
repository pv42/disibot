package fakejda;

import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.IMentionable;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.exceptions.RateLimitedException;
import net.dv8tion.jda.api.interactions.components.ActionRow;
import net.dv8tion.jda.api.requests.restaction.MessageAction;
import net.dv8tion.jda.api.utils.AttachmentOption;
import org.jetbrains.annotations.NotNull;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.File;
import java.io.InputStream;
import java.util.Collection;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import java.util.function.BiConsumer;
import java.util.function.BooleanSupplier;
import java.util.function.Consumer;

public class FakeMessageAction implements MessageAction {
    private Runnable action;

    public FakeMessageAction(Runnable action) {
        this.action = action;
    }


    @Nonnull
    @Override
    public JDA getJDA() {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public MessageAction setCheck(@Nullable BooleanSupplier checks) {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public MessageAction timeout(long timeout, @Nonnull TimeUnit unit) {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public MessageAction deadline(long timestamp) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void queue(@Nullable Consumer<? super Message> success, @Nullable Consumer<? super Throwable> failure) {
        action.run();
    }

    @Override
    public Message complete(boolean shouldQueue) throws RateLimitedException {
        action.run();
        return null;
    }

    @Nonnull
    @Override
    public CompletableFuture<Message> submit(boolean shouldQueue) {
        action.run();
        return null;
    }

    @Nonnull
    @Override
    public MessageChannel getChannel() {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public boolean isEdit() {
        return false;
    }

    @Nonnull
    @Override
    public MessageAction apply(@Nullable Message message) {
        throw new UnsupportedOperationException();
    }

    @NotNull
    @Override
    public MessageAction allowedMentions(@org.jetbrains.annotations.Nullable Collection<Message.MentionType> allowedMentions) {
        throw new UnsupportedOperationException();
    }

    @NotNull
    @Override
    public MessageAction mention(@NotNull IMentionable... mentions) {
        throw new UnsupportedOperationException();
    }

    @NotNull
    @Override
    public MessageAction mentionUsers(@NotNull String... userIds) {
        throw new UnsupportedOperationException();
    }

    @NotNull
    @Override
    public MessageAction mentionRoles(@NotNull String... roleIds) {
        throw new UnsupportedOperationException();
    }


    @Nonnull
    @Override
    public MessageAction referenceById(long messageId) {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public MessageAction mentionRepliedUser(boolean mention) {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public MessageAction failOnInvalidReply(boolean fail) {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public MessageAction tts(boolean isTTS) {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public MessageAction reset() {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public MessageAction nonce(@Nullable String nonce) {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public MessageAction content(@Nullable String content) {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public MessageAction embed(@Nullable MessageEmbed embed) {
        throw new UnsupportedOperationException();
    }

    @NotNull
    @Override
    public MessageAction setEmbeds(@NotNull Collection<? extends MessageEmbed> embeds) {
        return null;
    }

    @Nonnull
    @Override
    public MessageAction append(@Nullable CharSequence csq, int start, int end) {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public MessageAction append(char c) {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public MessageAction addFile(@Nonnull InputStream data, @Nonnull String name, @Nonnull AttachmentOption... options) {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public MessageAction addFile(@Nonnull File file, @Nonnull String name, @Nonnull AttachmentOption... options) {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public MessageAction clearFiles() {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public MessageAction clearFiles(@Nonnull BiConsumer<String, InputStream> finalizer) {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public MessageAction clearFiles(@Nonnull Consumer<InputStream> finalizer) {
        throw new UnsupportedOperationException();
    }

    @NotNull
    @Override
    public MessageAction retainFilesById(@NotNull Collection<String> ids) {
        throw new UnsupportedOperationException();
    }

    @NotNull
    @Override
    public MessageAction setActionRows(@NotNull ActionRow... rows) {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public MessageAction override(boolean bool) {
        throw new UnsupportedOperationException();
    }
}

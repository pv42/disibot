package fakejda;

import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.IMentionable;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.MessageEmbed;
import net.dv8tion.jda.api.entities.PrivateChannel;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.interaction.SlashCommandEvent;
import net.dv8tion.jda.api.exceptions.RateLimitedException;
import net.dv8tion.jda.api.interactions.InteractionHook;
import net.dv8tion.jda.api.interactions.commands.OptionMapping;
import net.dv8tion.jda.api.interactions.components.ActionRow;
import net.dv8tion.jda.api.requests.restaction.interactions.ReplyAction;
import net.dv8tion.jda.api.utils.AttachmentOption;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

import java.io.InputStream;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import java.util.function.BooleanSupplier;
import java.util.function.Consumer;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class FakeSlashCommandEvent extends SlashCommandEvent {
    private String replyStr;
    private String subcommand;
    private Map<String, String> options;
    private MessageChannel channel;
    private Guild guild;
    private User sender;

    public FakeSlashCommandEvent() {
        super(null, 0, null);
    }

    public void setOptions(Map<String, String> options) {
        this.options = options;
    }

    public void setChannel(MessageChannel channel) {
        this.channel = channel;
        if(channel instanceof TextChannel) {
            guild = ((TextChannel)channel).getGuild();
        }
    }

    public String getReplyStr() {
        return replyStr;
    }

    @Override
    public boolean isFromGuild() {
        return guild != null;
    }

    @Nullable
    @Override
    public Guild getGuild() {
        return guild;
    }

    @NotNull
    @Override
    public PrivateChannel getPrivateChannel() {
        if(channel != null && channel instanceof PrivateChannel) return (PrivateChannel) channel;
        return null;
    }

    @NotNull
    @Override
    public ReplyAction deferReply() {
        return new ReplyAction() {
            @NotNull
            @Override
            public ReplyAction setCheck(@Nullable BooleanSupplier checks) {
                throw new UnsupportedOperationException();
            }

            @NotNull
            @Override
            public ReplyAction timeout(long timeout, @NotNull TimeUnit unit) {
                throw new UnsupportedOperationException();
            }

            @NotNull
            @Override
            public ReplyAction deadline(long timestamp) {
                throw new UnsupportedOperationException();
            }

            @NotNull
            @Override
            public ReplyAction addEmbeds(@NotNull Collection<? extends MessageEmbed> embeds) {
                throw new UnsupportedOperationException();
            }

            @NotNull
            @Override
            public ReplyAction addActionRows(@NotNull ActionRow... rows) {
                throw new UnsupportedOperationException();
            }

            @NotNull
            @Override
            public ReplyAction setContent(@Nullable String content) {
                throw new UnsupportedOperationException();
            }

            @NotNull
            @Override
            public ReplyAction setTTS(boolean isTTS) {
                throw new UnsupportedOperationException();
            }

            @NotNull
            @Override
            public ReplyAction setEphemeral(boolean ephemeral) {
                throw new UnsupportedOperationException();
            }

            @NotNull
            @Override
            public ReplyAction addFile(@NotNull InputStream data, @NotNull String name, @NotNull AttachmentOption... options) {
                throw new UnsupportedOperationException();
            }

            @NotNull
            @Override
            public JDA getJDA() {
                throw new UnsupportedOperationException();
            }

            @Override
            public void queue(@Nullable Consumer<? super InteractionHook> success, @Nullable Consumer<? super Throwable> failure) {
                throw new UnsupportedOperationException();
            }

            @Override
            public InteractionHook complete(boolean shouldQueue) throws RateLimitedException {
                throw new UnsupportedOperationException();
            }

            @NotNull
            @Override
            public CompletableFuture<InteractionHook> submit(boolean shouldQueue) {
                throw new UnsupportedOperationException();
            }

            @NotNull
            @Override
            public ReplyAction mentionRepliedUser(boolean mention) {
                throw new UnsupportedOperationException();
            }

            @NotNull
            @Override
            public ReplyAction allowedMentions(@Nullable Collection<Message.MentionType> allowedMentions) {
                throw new UnsupportedOperationException();
            }

            @NotNull
            @Override
            public ReplyAction mention(@NotNull IMentionable... mentions) {
                throw new UnsupportedOperationException();
            }

            @NotNull
            @Override
            public ReplyAction mentionUsers(@NotNull String... userIds) {
                throw new UnsupportedOperationException();
            }

            @NotNull
            @Override
            public ReplyAction mentionRoles(@NotNull String... roleIds) {
                throw new UnsupportedOperationException();
            }
        };
    }

    @Nullable
    @Override
    public OptionMapping getOption(@NotNull String name) {
        if(options == null) return null;
        if (!options.containsKey(name)) return null;
        OptionMapping o =  mock(OptionMapping.class);
        when(o.getAsString()).thenReturn(options.get(name));
        when(o.getAsRole()).then((Answer<Role>) invocation -> {
            if(!options.get(name).startsWith("Not")) return new FakeRole(22, options.get(name), guild);
            else return new FakeRole(22, options.get(name), new FakeGuild("NotAguild",-1));
        });
        when(o.getAsLong()).then((Answer<Long>) invocation -> Long.valueOf(options.get(name)));
        return o;
    }

    @NotNull
    @Override
    public ReplyAction reply(@NotNull String content) {
        return new ReplyAction() {
            @NotNull
            @Override
            public ReplyAction setCheck(@Nullable BooleanSupplier checks) {
                throw new UnsupportedOperationException();
            }

            @NotNull
            @Override
            public ReplyAction timeout(long timeout, @NotNull TimeUnit unit) {
                throw new UnsupportedOperationException();
            }

            @NotNull
            @Override
            public ReplyAction deadline(long timestamp) {
                throw new UnsupportedOperationException();
            }

            @NotNull
            @Override
            public ReplyAction addEmbeds(@NotNull Collection<? extends MessageEmbed> embeds) {
                throw new UnsupportedOperationException();
            }

            @NotNull
            @Override
            public ReplyAction addActionRows(@NotNull ActionRow... rows) {
                throw new UnsupportedOperationException();
            }

            @NotNull
            @Override
            public ReplyAction setContent(@Nullable String content) {
                throw new UnsupportedOperationException();
            }

            @NotNull
            @Override
            public ReplyAction setTTS(boolean isTTS) {
                throw new UnsupportedOperationException();
            }

            @NotNull
            @Override
            public ReplyAction setEphemeral(boolean ephemeral) {
                throw new UnsupportedOperationException();
            }

            @NotNull
            @Override
            public ReplyAction addFile(@NotNull InputStream data, @NotNull String name, @NotNull AttachmentOption... options) {
                throw new UnsupportedOperationException();
            }

            @NotNull
            @Override
            public JDA getJDA() {
                throw new UnsupportedOperationException();
            }

            @Override
            public void queue(@Nullable Consumer<? super InteractionHook> success, @Nullable Consumer<? super Throwable> failure) {
                replyStr = content;
            }

            @Override
            public InteractionHook complete(boolean shouldQueue) throws RateLimitedException {
                throw new UnsupportedOperationException();
            }

            @NotNull
            @Override
            public CompletableFuture<InteractionHook> submit(boolean shouldQueue) {
                throw new UnsupportedOperationException();
            }

            @NotNull
            @Override
            public ReplyAction mentionRepliedUser(boolean mention) {
                throw new UnsupportedOperationException();
            }

            @NotNull
            @Override
            public ReplyAction allowedMentions(@Nullable Collection<Message.MentionType> allowedMentions) {
                throw new UnsupportedOperationException();
            }

            @NotNull
            @Override
            public ReplyAction mention(@NotNull IMentionable... mentions) {
                throw new UnsupportedOperationException();
            }

            @NotNull
            @Override
            public ReplyAction mentionUsers(@NotNull String... userIds) {
                throw new UnsupportedOperationException();
            }

            @NotNull
            @Override
            public ReplyAction mentionRoles(@NotNull String... roleIds) {
                throw new UnsupportedOperationException();
            }
        };
    }

    public void setSubCommand(String subcommand) {
        this.subcommand = subcommand;
    }

    @Override
    public String getSubcommandName() {
        return subcommand;
    }

    public void setSender(User sender) {
        this.sender = sender;
    }

    @NotNull
    @Override
    public User getUser() {
        return sender;
    }
}

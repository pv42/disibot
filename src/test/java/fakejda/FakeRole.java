package fakejda;

import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.GuildChannel;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.entities.RoleIcon;
import net.dv8tion.jda.api.managers.RoleManager;
import net.dv8tion.jda.api.requests.restaction.AuditableRestAction;
import net.dv8tion.jda.api.requests.restaction.RoleAction;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.annotation.Nonnull;
import java.awt.Color;
import java.util.Collection;
import java.util.EnumSet;

public class FakeRole implements Role {
    private Guild guild;
    private String name;
    private long id;

    public FakeRole(long id) {
        this.id = id;
        name = "FakeRole";
    }

    public FakeRole(long id, String name) {
        this.id = id;
        this.name = name;
    }

    public FakeRole(long id, String name, Guild guild) {
        this.id = id;
        this.name = name;
        this.guild = guild;
    }


    @Override
    public int getPosition() {
        throw new UnsupportedOperationException();
    }

    @Override
    public int getPositionRaw() {
        throw new UnsupportedOperationException();
    }

    @NotNull
    @Override
    public String getName() {
        return name;
    }

    @Override
    public boolean isManaged() {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean isHoisted() {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean isMentionable() {
        throw new UnsupportedOperationException();
    }

    @Override
    public long getPermissionsRaw() {
        throw new UnsupportedOperationException();
    }

    @Nullable
    @Override
    public Color getColor() {
        throw new UnsupportedOperationException();
    }

    @Override
    public int getColorRaw() {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean isPublicRole() {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean canInteract(@NotNull Role role) {
        throw new UnsupportedOperationException();
    }

    @NotNull
    @Override
    public Guild getGuild() {
        return guild;
    }

    @NotNull
    @Override
    public EnumSet<Permission> getPermissions() {
        throw new UnsupportedOperationException();
    }

    @NotNull
    @Override
    public EnumSet<Permission> getPermissions(@NotNull GuildChannel channel) {
        throw new UnsupportedOperationException();
    }

    @NotNull
    @Override
    public EnumSet<Permission> getPermissionsExplicit() {
        throw new UnsupportedOperationException();
    }

    @NotNull
    @Override
    public EnumSet<Permission> getPermissionsExplicit(@NotNull GuildChannel channel) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean hasPermission(@NotNull Permission... permissions) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean hasPermission(@NotNull Collection<Permission> permissions) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean hasPermission(@NotNull GuildChannel channel, @NotNull Permission... permissions) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean hasPermission(@NotNull GuildChannel channel, @NotNull Collection<Permission> permissions) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean canSync(@Nonnull GuildChannel targetChannel, @Nonnull GuildChannel syncSource) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean canSync(@Nonnull GuildChannel channel) {
        throw new UnsupportedOperationException();
    }

    @NotNull
    @Override
    public RoleAction createCopy(@NotNull Guild guild) {
        throw new UnsupportedOperationException();
    }

    @NotNull
    @Override
    public RoleManager getManager() {
        throw new UnsupportedOperationException();
    }

    @NotNull
    @Override
    public AuditableRestAction<Void> delete() {
        throw new UnsupportedOperationException();
    }

    @NotNull
    @Override
    public JDA getJDA() {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public RoleTags getTags() {
        throw new UnsupportedOperationException();
    }

    @Nullable
    @Override
    public RoleIcon getIcon() {
        throw new UnsupportedOperationException();
    }

    @Override
    public int compareTo(@NotNull Role o) {
        throw new UnsupportedOperationException();
    }

    @NotNull
    @Override
    public String getAsMention() {
        throw new UnsupportedOperationException();
    }

    @Override
    public long getIdLong() {
        return id;
    }
}

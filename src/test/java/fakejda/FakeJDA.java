package fakejda;

import net.dv8tion.jda.api.AccountType;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.Permission;
import net.dv8tion.jda.api.entities.ApplicationInfo;
import net.dv8tion.jda.api.entities.Category;
import net.dv8tion.jda.api.entities.Emote;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Icon;
import net.dv8tion.jda.api.entities.PrivateChannel;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.entities.SelfUser;
import net.dv8tion.jda.api.entities.StoreChannel;
import net.dv8tion.jda.api.entities.TextChannel;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.entities.VoiceChannel;
import net.dv8tion.jda.api.entities.Webhook;
import net.dv8tion.jda.api.exceptions.RateLimitedException;
import net.dv8tion.jda.api.hooks.IEventManager;
import net.dv8tion.jda.api.interactions.commands.Command;
import net.dv8tion.jda.api.interactions.commands.build.CommandData;
import net.dv8tion.jda.api.managers.AudioManager;
import net.dv8tion.jda.api.managers.DirectAudioController;
import net.dv8tion.jda.api.managers.Presence;
import net.dv8tion.jda.api.requests.GatewayIntent;
import net.dv8tion.jda.api.requests.RestAction;
import net.dv8tion.jda.api.requests.restaction.CommandCreateAction;
import net.dv8tion.jda.api.requests.restaction.CommandEditAction;
import net.dv8tion.jda.api.requests.restaction.CommandListUpdateAction;
import net.dv8tion.jda.api.requests.restaction.GuildAction;
import net.dv8tion.jda.api.sharding.ShardManager;
import net.dv8tion.jda.api.utils.cache.CacheFlag;
import net.dv8tion.jda.api.utils.cache.CacheView;
import net.dv8tion.jda.api.utils.cache.SnowflakeCacheView;
import okhttp3.OkHttpClient;
import org.jetbrains.annotations.NotNull;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ScheduledExecutorService;
import java.util.function.BooleanSupplier;
import java.util.function.Consumer;

public class FakeJDA implements JDA {
    private Map<Long, User> users = new HashMap<>();
    private List<Guild> guilds = new ArrayList<>();

    public FakeJDA() {
        for (long i = 0; i < 10; i++) {
            users.put(i, new FakeUser(i));
        }
        users.put(332225171601620992L, new FakeUser(992));
        users.put(117378235343306761L, new FakeUser(6761));
    }

    @Nonnull
    @Override
    public Status getStatus() {
        return Status.CONNECTED;
    }

    public Emote getEmoteById(@Nonnull String id) {
        return new FakeEmote("fake_emote","not_a_path");
    }

    @Nullable
    public Guild getGuildById(@Nonnull String id) {
        for(Guild guild: guilds) {
            if(guild.getId().equals(id)) return guild;
        }
        return null;
    }

    @Nonnull
    @Override
    public EnumSet<GatewayIntent> getGatewayIntents() {
        throw new UnsupportedOperationException();
    }

    @NotNull
    @Override
    public EnumSet<CacheFlag> getCacheFlags() {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean unloadUser(long userId) {
        throw new UnsupportedOperationException();
    }

    @Override
    public long getGatewayPing() {

        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public JDA awaitStatus(@Nonnull Status status) throws InterruptedException {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public JDA awaitStatus(@Nonnull Status status, @Nonnull Status... failOn) throws InterruptedException {
        throw new UnsupportedOperationException();
    }

    @Override
    public int cancelRequests() {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public ScheduledExecutorService getRateLimitPool() {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public ScheduledExecutorService getGatewayPool() {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public ExecutorService getCallbackPool() {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public OkHttpClient getHttpClient() {
        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public DirectAudioController getDirectAudioController() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void setEventManager(@Nullable IEventManager manager) {

        throw new UnsupportedOperationException();
    }

    @Override
    public void addEventListener(@Nonnull Object... listeners) {

    }

    @Override
    public void removeEventListener(@Nonnull Object... listeners) {

    }

    @Nonnull
    @Override
    public List<Object> getRegisteredListeners() {

        throw new UnsupportedOperationException();
    }

    @NotNull
    @Override
    public RestAction<List<Command>> retrieveCommands() {

        throw new UnsupportedOperationException();
    }

    @NotNull
    @Override
    public RestAction<Command> retrieveCommandById(@NotNull String id) {

        throw new UnsupportedOperationException();
    }

    @NotNull
    @Override
    public CommandCreateAction upsertCommand(@NotNull CommandData command) {

        throw new UnsupportedOperationException();
    }

    @NotNull
    @Override
    public CommandListUpdateAction updateCommands() {

        throw new UnsupportedOperationException();
    }

    @NotNull
    @Override
    public CommandEditAction editCommandById(@NotNull String id) {

        throw new UnsupportedOperationException();
    }

    @NotNull
    @Override
    public RestAction<Void> deleteCommandById(@NotNull String commandId) {

        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public GuildAction createGuild(@Nonnull String name) {

        throw new UnsupportedOperationException();
    }

    @NotNull
    @Override
    public RestAction<Void> createGuildFromTemplate(@NotNull String code, @NotNull String name, @org.jetbrains.annotations.Nullable Icon icon) {

        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public CacheView<AudioManager> getAudioManagerCache() {

        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public SnowflakeCacheView<User> getUserCache() {

        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public List<Guild> getMutualGuilds(@Nonnull User... users) {

        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public List<Guild> getMutualGuilds(@Nonnull Collection<User> users) {

        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public RestAction<User> retrieveUserById(@Nonnull String id) {
        return new RestAction<User>() {
            @Nonnull
            @Override
            public JDA getJDA() {
                throw new UnsupportedOperationException();
            }

            @Nonnull
            @Override
            public RestAction<User> setCheck(@Nullable BooleanSupplier checks) {
                throw new UnsupportedOperationException();
            }

            @Override
            public void queue(@Nullable Consumer<? super User> success, @Nullable Consumer<? super Throwable> failure) {
                throw new UnsupportedOperationException();
            }

            @Override
            public User complete(boolean shouldQueue) throws RateLimitedException {
                return users.get(Long.valueOf(id));
            }

            @Nonnull
            @Override
            public CompletableFuture<User> submit(boolean shouldQueue) {

                throw new UnsupportedOperationException();
            }
        };
    }

    @Nullable
    @Override
    public User getUserById(@Nonnull long id) {
        return users.get(id);
    }

    @Nonnull
    @Override
    public RestAction<User> retrieveUserById(long id) {
        return new RestAction<User>() {
            @Nonnull
            @Override
            public JDA getJDA() {
                throw new UnsupportedOperationException();
            }

            @Nonnull
            @Override
            public RestAction<User> setCheck(@Nullable BooleanSupplier checks) {
                throw new UnsupportedOperationException();
            }

            @Override
            public void queue(@Nullable Consumer<? super User> success, @Nullable Consumer<? super Throwable> failure) {
                throw new UnsupportedOperationException();
            }

            @Override
            public User complete(boolean shouldQueue) throws RateLimitedException {
                return users.get(id);
            }

            @Nonnull
            @Override
            public CompletableFuture<User> submit(boolean shouldQueue) {

                throw new UnsupportedOperationException();
            }
        };
    }

    @Nonnull
    @Override
    public RestAction<User> retrieveUserById(long id, boolean update) {

        throw new UnsupportedOperationException();
    }

    @Nonnull
    @Override
    public SnowflakeCacheView<Guild> getGuildCache() {
        return null;
    }

    @Nonnull
    @Override
    public SnowflakeCacheView<Role> getRoleCache() {
        return null;
    }

    @Nonnull
    @Override
    public SnowflakeCacheView<Category> getCategoryCache() {
        return null;
    }

    @Nonnull
    @Override
    public SnowflakeCacheView<StoreChannel> getStoreChannelCache() {
        return null;
    }

    @Nonnull
    @Override
    public SnowflakeCacheView<TextChannel> getTextChannelCache() {
        return null;
    }

    @Nonnull
    @Override
    public SnowflakeCacheView<VoiceChannel> getVoiceChannelCache() {
        return null;
    }

    @Nonnull
    @Override
    public SnowflakeCacheView<PrivateChannel> getPrivateChannelCache() {
        return null;
    }

    @Nonnull
    @Override
    public RestAction<PrivateChannel> openPrivateChannelById(long userId) {
        return null;
    }

    @Nonnull
    @Override
    public SnowflakeCacheView<Emote> getEmoteCache() {
        return null;
    }

    @Nonnull
    @Override
    public IEventManager getEventManager() {
        return null;
    }

    @Nonnull
    @Override
    public SelfUser getSelfUser() {
        return null;
    }

    @Nonnull
    @Override
    public Presence getPresence() {
        return null;
    }

    @Nonnull
    @Override
    public ShardInfo getShardInfo() {
        return null;
    }

    @Nonnull
    @Override
    public String getToken() {
        return null;
    }

    @Override
    public long getResponseTotal() {
        return 0;
    }

    @Override
    public int getMaxReconnectDelay() {
        return 0;
    }

    @Override
    public void setAutoReconnect(boolean reconnect) {

    }

    @Override
    public void setRequestTimeoutRetry(boolean retryOnTimeout) {

    }

    @Override
    public boolean isAutoReconnect() {
        return false;
    }

    @Override
    public boolean isBulkDeleteSplittingEnabled() {
        return false;
    }

    @Override
    public void shutdown() {

    }

    @Override
    public void shutdownNow() {

    }

    @Nonnull
    @Override
    public AccountType getAccountType() {
        return null;
    }

    @Nonnull
    @Override
    public RestAction<ApplicationInfo> retrieveApplicationInfo() {
        return null;
    }

    @NotNull
    @Override
    public JDA setRequiredScopes(@NotNull Collection<String> scopes) {
        return null;
    }

    @Nonnull
    @Override
    public String getInviteUrl(@Nullable Permission... permissions) {
        return null;
    }

    @Nonnull
    @Override
    public String getInviteUrl(@Nullable Collection<Permission> permissions) {
        return null;
    }

    @Nullable
    @Override
    public ShardManager getShardManager() {
        return null;
    }

    @Nonnull
    @Override
    public RestAction<Webhook> retrieveWebhookById(@Nonnull String webhookId) {
        return null;
    }

    public void addGuild(Guild guild) {
        guilds.add(guild);
    }

    @Nonnull
    @Override
    public List<Guild> getGuilds() {
        return guilds;
    }

    @Nonnull
    @Override
    public List<Guild> getGuildsByName(@Nonnull String name, boolean ignoreCase) {
        List<Guild> result = new ArrayList<>();
        for(Guild guild: guilds) {
            if(guild.getName().equals(name) || (ignoreCase && guild.getName().toLowerCase().equals(name.toLowerCase()))) {
                result.add(guild);
            }
        }
        return result;
    }

    @Nonnull
    @Override
    public Set<String> getUnavailableGuilds() {
        return null;
    }

    @Override
    public boolean isUnavailable(long guildId) {
        return false;
    }
}

package fakejda;

import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.User;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

@SuppressWarnings("ConstantConditions")
public class FakeMessageEvent extends MessageReceivedEvent {
    private MessageChannel channel;
    private Member member;
    public FakeMessageEvent(FakeMessage message) {
        super(null, 0, message);
        channel = message.getChannel();

    }

    public FakeMessageEvent(FakeMessage message, FakeMember member) {
        super(null, 0, message);
        channel = message.getChannel();
        this.member = member;

    }

    @Nonnull
    @Override
    public MessageChannel getChannel() {
        return channel;
    }


    @Nullable
    @Override
    public Member getMember() {
        return member;
    }

    @Nonnull
    @Override
    public User getAuthor() {
        if(member == null) return null;
        return member.getUser();
    }
}

package disibot.keywordreactions;

import disibot.context.ContextStorage;
import disibot.newcommand.AbstractSlashCommandHandlerTester;
import disibot.server.ServerFactory;
import fakejda.FakeGuild;
import fakejda.FakeMessage;
import fakejda.FakePrivateChannel;
import fakejda.FakeTextChannel;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;

public class KeywordReactionCommandHandlerTest extends AbstractSlashCommandHandlerTester<KeywordReactionCommandHandler> {

    @AfterAll
    @BeforeAll
    static void deleteFiles() throws IOException {
        if (Files.exists(Path.of("Server.0.json"))) {
            Files.delete(Path.of("Server.0.json"));
        }
        if (Files.exists(Path.of("Server.2.json"))) {
            Files.delete(Path.of("Server.2.json"));
        }
    }

    @BeforeEach
    void setup() {
        handler = new KeywordReactionCommandHandler(new ContextStorage());
    }



    @Test
    void testNotOnServer() {
        FakePrivateChannel ch = new FakePrivateChannel();
        FakeMessage msg = new FakeMessage("!", ch);
        Map<String, String> args = new HashMap<>();
        args.put("phrase", "ZYRA");
        args.put("emote", "<:zyra:65984>");
        sendCommand("reaction", args, ch);
        assertReply("This command only works on servers.");
    }

    @Test
    void testSetCustom() {
        FakeGuild guild = new FakeGuild("KeywReCoHaTst-FG");
        FakeTextChannel ch = new FakeTextChannel(guild);
        Map<String, String> args = new HashMap<>();
        args.put("phrase", "ZYRA");
        args.put("emote", "<:zyra:65984>");
        sendCommand("reaction", args, ch);
        assertReply("Emote <:zyra:65984> set as reaction to zyra.");
        assertEquals("<:zyra:65984>", ServerFactory.get(guild).getKeywordReactionData().get("zyra"));
        assertFalse(ServerFactory.get(guild).getKeywordReactionData().containsKey("ZYRA"));
        testRemoveCustom(ch);
    }


    void testRemoveCustom(FakeTextChannel channel) {
        Map<String, String> args = new HashMap<>();
        args.put("phrase", "ZYRA");
        args.put("emote", "remove");
        sendCommand("reaction", args, channel);
        assertReply("Reaction to zyra removed.");
        assertFalse(ServerFactory.get(channel.getGuild()).getKeywordReactionData().containsKey("zyra"));
    }

    @Test
    void testSetUnicode() {
        FakeGuild guild = new FakeGuild("KeywReCoHaTst-FG");
        FakeTextChannel ch = new FakeTextChannel(guild);
        Map<String, String> args = new HashMap<>();
        args.put("phrase", "love");
        args.put("emote", "" + (char) 26 + (char) 65);
        sendCommand("reaction", args, ch);
        assertReply("Emote " + (char) 26 + (char) 65 + " set as reaction to love.");
        assertEquals("" + (char) 26 + (char) 65, ServerFactory.get(guild).getKeywordReactionData().get("love"));
        testRemoveUnicode(ch);
    }

    void testRemoveUnicode(FakeTextChannel ch) {
        Map<String, String> args = new HashMap<>();
        args.put("phrase", "love");
        args.put("emote", "remove");
        sendCommand("reaction", args, ch);
        assertReply("Reaction to love removed.");
        assertFalse(ServerFactory.get(ch.getGuild()).getKeywordReactionData().containsKey("love"));
    }

    @Test
    void testSetInvalid() {
        FakeGuild guild = new FakeGuild("KeywReCoHaTst2-FG");
        FakeTextChannel ch = new FakeTextChannel(guild);
        Map<String, String> args = new HashMap<>();
        args.put("phrase", "eee");
        args.put("emote", "E");
        sendCommand("reaction", args, ch);
        assertReply("E is not a valid reaction emote.");
        assertFalse(ServerFactory.get(guild).getKeywordReactionData().containsKey("eee"));
    }

    @Test
    void testRemoveInvalid() {
        FakeGuild guild = new FakeGuild("KeywReCoHaTst2-FG");
        FakeTextChannel ch = new FakeTextChannel(guild);
        Map<String, String> args = new HashMap<>();
        args.put("phrase", "no_exist");
        args.put("emote", "remove");
        sendCommand("reaction", args, ch);
        assertReply("Failed to remove reaction: Does not exist.");
    }
}

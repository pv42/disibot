package disibot.emote;

import disibot.UsageEvent;
import disibot.UsageStorage;
import fakejda.FakeEmote;
import fakejda.FakeGuild;
import fakejda.FakeJDA;
import fakejda.FakeTextChannel;
import org.junit.jupiter.api.Test;

import java.time.OffsetDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

class EmotePodiumCreatorTest {

    private final FakeEmote emote1 = new FakeEmote("emote1", "testres/emote1.png");
    private final FakeEmote emote2 = new FakeEmote("emote2", "testres/emote1.png");
    private final FakeEmote emote3 = new FakeEmote("emote3", "testres/emote1.png");
    private final FakeEmote emoteInvalid = new FakeEmote("emote4", "NULL");

    @Test
    void sendEmotePodiums() {
        FakeJDA jda = new FakeJDA();
        FakeGuild server = new FakeGuild("FG");
        FakeGuild channelLessServer = new FakeGuild("cls");
        FakeGuild twoEmoteServer = new FakeGuild("tes");
        jda.addGuild(server);
        jda.addGuild(channelLessServer);
        jda.addGuild(twoEmoteServer);
        FakeTextChannel channel = new FakeTextChannel();
        FakeTextChannel twoEmoteChannel = new FakeTextChannel();
        server.addChannel(channel);
        twoEmoteServer.addChannel(twoEmoteChannel);
        UsageStorage usage = new UsageStorage();
        usage.pushEvent(new UsageEvent(server, emote1, "channelName", OffsetDateTime.now(), 0));
        usage.pushEvent(new UsageEvent(server, emote2, "channelName", OffsetDateTime.now(), 1));
        usage.pushEvent(new UsageEvent(server, emote3, "channelName", OffsetDateTime.now(), 2));
        usage.pushEvent(new UsageEvent(server, emote3, "channelName", OffsetDateTime.now(), 3));
        usage.pushEvent(new UsageEvent(channelLessServer, emote1, "noneExistingChannel", OffsetDateTime.now(), 4));
        usage.pushEvent(new UsageEvent(channelLessServer, emote2, "noneExistingChannel", OffsetDateTime.now(), 5));
        usage.pushEvent(new UsageEvent(channelLessServer, emote3, "noneExistingChannel", OffsetDateTime.now(), 6));
        usage.pushEvent(new UsageEvent(twoEmoteServer, emote2, "tesChannelName", OffsetDateTime.now(), 7));
        usage.pushEvent(new UsageEvent(twoEmoteServer, emote1, "tesChannelName", OffsetDateTime.now(), 8));
        EmotePodiumCreator creator = new EmotePodiumCreator(usage, jda);
        creator.sendEmotePodiums();
        assertEquals("PNG", channel.getFiles().get(0).substring(1, 4));
        assertEquals(0, twoEmoteChannel.getFiles().size());

    }

    @Test
    void sendEmotePodiumsWrongUrl() {
        FakeJDA jda = new FakeJDA();
        FakeGuild server = new FakeGuild("FG");
        jda.addGuild(server);
        FakeTextChannel channel = new FakeTextChannel();
        server.addChannel(channel);
        UsageStorage usage = new UsageStorage();
        usage.pushEvent(new UsageEvent(server, emote1, "channelName", OffsetDateTime.now(), 0));
        usage.pushEvent(new UsageEvent(server, emote2, "channelName", OffsetDateTime.now(), 1));
        usage.pushEvent(new UsageEvent(server, emoteInvalid, "channelName", OffsetDateTime.now(), 2));
        EmotePodiumCreator creator = new EmotePodiumCreator(usage, jda);
        creator.sendEmotePodiums();
        assertEquals(0, channel.getFiles().size());

    }
}
package disibot.welcomesound;

import disibot.util.Util;
import fakejda.FakeGuild;
import fakejda.FakeJDA;
import fakejda.FakeMember;
import fakejda.FakeUser;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;


class WelcomeSoundStorageTest {
    private FakeJDA jda = new FakeJDA();

    @BeforeAll
    @AfterAll
    static void cleanUp()  {
        if(!Files.exists(Path.of("welcomesoundstorage.json"))) return;
        try {
            Files.delete(Path.of("welcomesoundstorage.json"));
            System.out.println("Deleted file");
        } catch (IOException e) {
            System.err.println("Could not delete test welcome storage ");
        }
    }
    @Test
    void testSave() throws IOException {
        FakeGuild guild = new FakeGuild(jda, "welcome_test_guild", 4);
        FakeGuild guild2 = new FakeGuild(jda, "welcome_test_guild2", 42);
        jda.addGuild(guild);
        jda.addGuild(guild2);
        FakeUser user = new FakeUser(68418);
        FakeMember member = new FakeMember(guild, user);
        FakeMember member2 = new FakeMember(guild2, user);
        guild.addMember(member);
        guild2.addMember(member2);
        WelcomeSoundStorage storage = new WelcomeSoundStorage(jda);
        storage.putMessage(member, "/dev/null");
        storage.putMessage(member2, "/dev/NULL");
    }
}
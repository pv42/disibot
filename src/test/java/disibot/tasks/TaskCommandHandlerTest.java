package disibot.tasks;

import disibot.command.AbstractCommandHandlerTester;
import disibot.server.ServerFactory;
import disibot.util.Command;
import fakejda.FakeMember;
import fakejda.FakeMessage;
import fakejda.FakeMessageEvent;
import fakejda.FakeUser;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class TaskCommandHandlerTest extends AbstractCommandHandlerTester {
    private final boolean[] testTaskRun = new boolean[1];

    @AfterAll
    @BeforeAll
    static void deleteFile() throws IOException {
        if (Files.exists(Path.of("Server.0.json"))) {
            Files.delete(Path.of("Server.0.json"));
        }
        if (Files.exists(Path.of("Server.2.json"))) {
            Files.delete(Path.of("Server.2.json"));
        }
    }

    TaskCommandHandlerTest() {
        TaskRunner runner = mock(TaskRunner.class);
        List<Task> tasks = new ArrayList<>();
        when(runner.getTasks()).thenReturn(tasks);
        doAnswer(invocation -> {
            tasks.add(invocation.getArgument(0));
            return null;
        }).when(runner).addTask(any());
        // add test task
        runner.addTask(new Task("TestTask", Duration.ofSeconds(3600), () -> testTaskRun[0] = true));
        runner.addTask(new Task("TestExTask", Duration.ofSeconds(3600), () -> {throw new RuntimeException();}));
    }

    @BeforeEach
    void setup() {
        channel.getMessages().clear();
    }

    private void assertPenultimateMessage(String expectedLastMessage) {
        assertTrue(channel.getMessages().size() > 1);
        assertEquals(expectedLastMessage, channel.getMessages().get(channel.getMessages().size() - 2).getContentRaw());
    }

    @Test
    void testList() {
        // todo fix for slash
        //sendCommand("tasks list");
        /*assertLastMessageMatch("\\`\\`\\`taskname                   \\| jobnumber \\|execution time \\(s\\)\\| next execution\\n" +
                "TestTask                   \\|         0 \\|  0[.,]00000 \\| \\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}.\\d+\\n" +
                "TestExTask                 \\|         0 \\|  0[.,]00000 \\| \\d{4}-\\d{2}-\\d{2}T\\d{2}:\\d{2}:\\d{2}.\\d+\\n" +
                "\\`\\`\\`");
    */}

    @Test
    void testRun() {
        // todo fix for slash
        /*sendCommand("tasks run TestTask");
        assertLastMessage("Permission denied.");
        // elevate permission
        ServerFactory.get(guild).elevate(member);
        sendCommand("tasks run not_a_task");
        assertLastMessage("No tasks matching the name 'not_a_task' found.");
        sendCommand("tasks run TestExTask");
        assertEquals("Exception occurred during the execution of TestExTask:", channel.getMessages().get(3).getContentRaw());
        assertFalse(testTaskRun[0]);
        sendCommand("tasks run TestTask");
        assertLastMessage("Executing a job of TestTask.");
        assertTrue(testTaskRun[0]);
        // remove elevation
        ServerFactory.get(guild).ground(member);*/
    }


    @Test
    void testInvalid() {
        //sendCommand("task i_dont_exist");
        //assertLastMessage("Unknown tasks command try `!tasks help`.");
    }
}
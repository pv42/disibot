package disibot.newcommand;

import fakejda.FakeGuild;
import fakejda.FakeMessageChannel;
import fakejda.FakeSlashCommandEvent;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.entities.User;

import javax.annotation.Nullable;

import java.util.Map;

import static junit.framework.Assert.assertEquals;

public class AbstractSlashCommandHandlerTester<T extends SlashCommandHandler> {
    protected T handler;
    private FakeSlashCommandEvent ev;
    protected MessageChannel channel;
    protected User sender;
    protected FakeGuild guild = new FakeGuild("ASCH_FG");
    protected void sendCommand(String command, @Nullable String subcommand, @Nullable Map<String, String> options, MessageChannel channel) {
        ev = new FakeSlashCommandEvent();
        ev.setSubCommand(subcommand);
        ev.setOptions(options);
        ev.setChannel(channel);
        ev.setSender(sender);
        handler.handle(ev);
    }

    protected void sendCommand(String command, @Nullable String subcommand, @Nullable Map<String, String> options) {
        sendCommand(command, subcommand, options, channel);
    }

    protected void sendCommand(String command, @Nullable Map<String, String> options, MessageChannel channel) {
        sendCommand(command, null, options, channel);
    }

    protected void sendCommand(String command) {
        sendCommand(command, null, null, channel);
    }


    protected void sendCommand(String command, Map<String, String> options) {
        sendCommand(command, null, options, channel);
    }

    protected void assertReply(String str) {
        assertEquals(str, ev.getReplyStr());
    }
}

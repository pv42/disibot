package disibot;


import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.entities.Activity;
import net.dv8tion.jda.api.entities.Guild;
import net.dv8tion.jda.api.entities.Member;
import net.dv8tion.jda.api.requests.GatewayIntent;

import javax.security.auth.login.LoginException;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDateTime;

@SuppressWarnings("deprecation")
public class POC {

    public static void main(String[] args) {
        System.out.println(String.format(">%-30s<","abc"));
        //time();
        //lol();
    }

    private static void time() {
        LocalDateTime t1 = LocalDateTime.of(2020, 2, 11, 18, 0, 0);
        LocalDateTime t0 = LocalDateTime.of(2020, 2, 3, 18, 0, 0);
        System.out.println(t1.minusDays(1).isAfter(t0));
    }

    private static void disi() {
        String TOKEN_FILE = "token";
        String token = null;
        try {
            FileReader fr = new FileReader(TOKEN_FILE);
            char[] buffer = new char[512];
            int r = fr.read(buffer);
            if (r < 0) throw new IOException("could not read file");
            token = String.valueOf(buffer);
            token = token.substring(0, r);
            fr.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        token = token.replaceAll("\n", "");
        JDABuilder builder = JDABuilder.create(token, GatewayIntent.GUILD_PRESENCES);
        JDA jda;
        try {
            jda = builder.build();
        } catch (LoginException e) {
            e.printStackTrace();
            return;
        }
        try {
            jda.awaitReady();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        Guild disi = jda.getGuildsByName("Der Disi", true).get(0);
        for (Member member : disi.getMembers()) {
            if (member.getActivities().size() > 0) {
                Activity activity = member.getActivities().get(0);
                System.out.println(member.getUser().getName() + ":" + activity.getType() + "," + activity.getName() + "," + activity.getUrl());
            }
        }
    }
}

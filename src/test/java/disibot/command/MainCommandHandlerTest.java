package disibot.command;

import disibot.DisiBot;
import disibot.UsageStorage;
import disibot.context.ContextStorage;
import disibot.server.PermissionCommandHandler;
import disibot.util.Command;
import fakejda.*;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class MainCommandHandlerTest extends MainCommandHandler {
    private static final String GENERAL_HELP = MainCommandHandler.GENERAL_HELP;

    private UsageStorage usageStorage;
    private FakeGuild guild;
    private FakeMessageEvent event;
    private FakeTextChannel channel;
    private FakeMessage message;
    private FakeUser user;

    public MainCommandHandlerTest() {
        super(new ContextStorage());
    }


    @BeforeEach
    void setUp() throws IOException {
        deleteSave();
        guild = new FakeGuild("FG",3);
        channel = new FakeTextChannel(guild);
        message = new FakeMessage("!sudo", channel);
        user = new FakeUser(7777);
        FakeMember member = new FakeMember(guild, user);
        event = new FakeMessageEvent(message, member);
    }


    @Test
    void testHandleCommand() {
        FakeGuild guild = new FakeGuild("Fg");
        FakeTextChannel channel = new FakeTextChannel(guild);
        FakeMessage message = new FakeMessage("!help", channel);
        FakeMessageEvent event = new FakeMessageEvent(message);
        handleCommand(new Command("help"), event);
        assertEquals(1, channel.getMessages().size());
        assertEquals(GENERAL_HELP, channel.getMessages().get(0).getContentRaw());
    }

    // todo fix sudo test
    /*@Test
    void testHandleSudo() {
        PermissionCommandHandler pch = new PermissionCommandHandler(null);
        addCommandHandler(pch);
        handleCommand(new Command("sudo"), event);
        assertEquals(1, channel.getMessages().size());
        assertEquals("FakeUser" + SUDO_NOT_ALLOWED, channel.getMessages().get(0).getContentRaw());
        FakeUser user = new FakeUser(332225171601620992L); // set user to a admin
        FakeMember member = new FakeMember(guild, user);
        event = new FakeMessageEvent(message, member);
        handleCommand(new Command("sudo"), event);
        handleCommand(new Command("sudo permission own"), event);
        assertEquals(3, channel.getMessages().size());
        assertEquals(GENERAL_UNKNOWN, channel.getMessages().get(1).getContentRaw());
        assertEquals("Your current permission power is 2147483647.", channel.getMessages().get(2).getContentRaw());
        removeCommandHandler(pch);
    }*/

    @Test
    void testHandleVersion() {
        handleCommand(new Command("version"), event);
        assertEquals("DisiBot " + DisiBot.VERSION + " by pv42#6061", channel.getMessages().get(0).getContentRaw());
    }

    @Test
    void testCanHandleCommand() {
        MessageReceivedEvent event = new FakeMessageReceivedEvent(null, 0, new FakeMessage("", new FakeTextChannel(), user));
        assertTrue(canHandleCommand(new Command("!help"),event));
        assertTrue(canHandleCommand(new Command("!rsehrcn2"),event));
        assertTrue(canHandleCommand(new Command("!"),event));
        assertTrue(canHandleCommand(new Command("!\0\n\r\t"),event));
        assertTrue(canHandleCommand(new Command("!\u1f4a9"),event));
        assertFalse(canHandleCommand(new Command("help"),event));
        assertFalse(canHandleCommand(new Command(" !dug"),event));
        assertFalse(canHandleCommand(new Command("\0!"),event));
        assertFalse(canHandleCommand(new Command("\n! tre zhthrjiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiugkxchgeiortsgi"),event));
    }

    @Test
    void testHandle() {

    }

    @AfterAll
    static void cleanUp() throws IOException {
        deleteSave();
    }

    static void deleteSave() throws IOException {
        if(Files.exists(Path.of("Server.3.json"))) {
            Files.delete(Path.of("Server.3.json"));
        }
    }
}
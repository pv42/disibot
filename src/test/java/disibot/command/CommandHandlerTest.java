package disibot.command;

import disibot.util.Command;
import fakejda.FakeMessage;
import fakejda.FakeMessageEvent;
import fakejda.FakeTextChannel;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.annotation.Nonnull;

import static org.junit.jupiter.api.Assertions.*;

class CommandHandlerTest extends CommandHandler {

    private int commandsHandled = 0;
    private boolean canHandle = false;
    private FakeTextChannel channel;

    public CommandHandlerTest() {
        super(null);
    }

    @BeforeEach
    void beforeEach() {
        channel = new FakeTextChannel();
        commandsHandled = 0;
    }

    @Test
    void testHandleCommand() {
        assertEquals(0,commandsHandled);
        handleCommand(new Command(""), new FakeMessageEvent(new FakeMessage("",channel)));
        assertEquals(1, commandsHandled);
        handleCommand(new Command(""), new FakeMessageEvent(new FakeMessage("",channel)));
        handleCommand(new Command(""), new FakeMessageEvent(new FakeMessage("",channel)));
        handleCommand(new Command(""), new FakeMessageEvent(new FakeMessage("",channel)));
        handleCommand(new Command(""), new FakeMessageEvent(new FakeMessage("",channel)));
        handleCommand(new Command(""), new FakeMessageEvent(new FakeMessage("",channel)));
        assertEquals(6, commandsHandled);
    }

    @Test
    void testReply() {
        handleCommand(new Command(""),new FakeMessageEvent(new FakeMessage("",channel)));
        reply("15234 ab");
        assertEquals(1, channel.getMessages().size());
        assertEquals("15234 ab", channel.getMessages().get(0).getContentRaw());
    }

    @Test
    void testCanHandle() {
        canHandle = false;
        assertFalse(canHandleCommand(new Command(""),null));
        canHandle = true;
        assertTrue(canHandleCommand(new Command(""),null));
    }

    @Test
    void testGetServer() {
    }

    @Test
    void testGetGuild() {
    }

    @Test
    void testGetSender() {
    }

    @Override
    public boolean canHandleCommand(@Nonnull Command command, @Nonnull MessageReceivedEvent ev) {
        return canHandle;
    }

    @Override
    protected void handle(@Nonnull Command command) {
        commandsHandled ++;
    }
}
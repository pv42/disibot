package disibot.coinflip;

import disibot.command.CommandHandler;
import disibot.modules.DisiBotModule;
import disibot.modules.DisiBotModuleInterface;
import disibot.newcommand.SlashCommandHandler;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.mock;

public class CoinFlipModuleTest {
    @Test
    void test() {
        DisiBotModule module = new CoinflipModuleFactory().initiateModule(null, null, null, null, null, null, null, null);
        DisiBotModuleInterface moduleInterface = mock(DisiBotModuleInterface.class);
        final boolean[] commandHandlerAdded = {false};
        doAnswer((arg) -> commandHandlerAdded[0] = true).when(moduleInterface).addSlashCommandHandler(eq(module), any(CoinflipCommandHandler.class));
        module.load(moduleInterface);
        assertTrue(commandHandlerAdded[0]);
    }
}

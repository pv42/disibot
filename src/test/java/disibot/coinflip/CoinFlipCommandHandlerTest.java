package disibot.coinflip;

import disibot.newcommand.AbstractSlashCommandHandlerTester;
import fakejda.FakeGuild;
import fakejda.FakeMember;
import fakejda.FakeMessage;
import fakejda.FakeMessageChannel;
import fakejda.FakeMessageEvent;
import fakejda.FakeSlashCommandEvent;
import fakejda.FakeTextChannel;
import fakejda.FakeUser;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class CoinFlipCommandHandlerTest extends AbstractSlashCommandHandlerTester<CoinflipCommandHandler> {
    private FakeGuild guild;
    private FakeMessageChannel channel;
    private FakeMessage message;
    private FakeMessageEvent event;

    public CoinFlipCommandHandlerTest() {
        handler = new CoinflipCommandHandler();
    }

    @BeforeEach
    void setUp() {
        guild = new FakeGuild("FG");
        channel = new FakeTextChannel(guild);
        message = new FakeMessage("!sudo", channel);
        FakeUser user = new FakeUser(7777);
        FakeMember member = new FakeMember(guild, user);
        event = new FakeMessageEvent(message, member);
        handler = new CoinflipCommandHandler();
    }

    @Test
    void testHandleCoinflip() {
        String replyMsg = null;
        int i = 0;
        while (!"Head".equals(replyMsg)) {
            FakeSlashCommandEvent ev = new FakeSlashCommandEvent();
            handler.handle(ev);
            replyMsg = ev.getReplyStr();
            assertTrue("Head".equals(replyMsg) || "Tail".equals(replyMsg));
            i++;
        }
        while (!"Tail".equals(replyMsg)) {
            FakeSlashCommandEvent ev = new FakeSlashCommandEvent();
            handler.handle(ev);
            replyMsg = ev.getReplyStr();
            assertTrue("Head".equals(replyMsg) || "Tail".equals(replyMsg));
            i++;
        }
    }
}

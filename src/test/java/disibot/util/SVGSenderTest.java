package disibot.util;


import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.AfterAll;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class SVGSenderTest {
    private static final String TMP_PNG_PATH = "tmp.png";

    @Test
    void testEncode() throws IOException, NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        //read svg
        File svg = new File("testres/top_example.svg");
        FileInputStream in = new FileInputStream(svg);
        byte[] svgData;
        svgData = new byte[(int) Files.size(svg.toPath())];
        in.read(svgData);
        in.close();
        // call private method
        Class<SVGSender> c = SVGSender.class;
        Method method = c.getDeclaredMethod("convertSVGtoPNG", byte[].class);
        method.setAccessible(true);
        byte[] pngData = (byte[]) method.invoke(null, svgData);
        // write png
        File png = new File(TMP_PNG_PATH);
        FileOutputStream out = new FileOutputStream(png);
        out.write(pngData);
        out.close();
        long size = Files.size(png.toPath());
        assertTrue(30000 < size,"expected to be bigger then 30000 but was " + size); // expect ~ 36k
        assertTrue(40000 > size, "expected to be smaller then 40000 but was " + size);
    }

    @AfterAll
    static void cleanUp() throws IOException {
        Files.delete(Paths.get(TMP_PNG_PATH));
    }

}

package disibot.util;

import fakejda.FakeJDA;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class CommandTest {
    @Test
    void testWhiteSpace() {
        Command c = new Command("!lol rank");
        assertTrue(c.hasNext());
        assertEquals("!lol", c.nextString());
        assertTrue(c.hasNext());
        assertEquals("rank", c.nextString());
        assertFalse(c.hasNext());
        assertEquals("", c.nextString());
        c = new Command("!lol rank -4985  0 \n 5\n   ");
        assertTrue(c.hasNext());
        assertEquals("!lol", c.nextString());
        assertTrue(c.hasNext());
        assertEquals("rank", c.nextString());
        assertTrue(c.hasNext());
        assertEquals(-4985, c.nextInt());
        assertTrue(c.hasNext());
        assertEquals("0", c.nextString());
        assertTrue(c.hasNext());
        assertEquals("5", c.nextString());
        assertFalse(c.hasNext());
        assertEquals("", c.nextString());
        assertEquals("", c.nextString());
        assertEquals("", c.nextString());
        assertFalse(c.hasNext());
    }

    @Test
    void testEscape() {
        Command c = new Command("hello\\ world\\  5\\\"\n4\" \\\"\"1\\");
        assertTrue(c.hasNext());
        assertEquals("hello world ", c.nextString());
        assertTrue(c.hasNext());
        assertEquals("5\"", c.nextString());
        assertTrue(c.hasNext());
        assertEquals("4 \"1", c.nextString());
        assertFalse(c.hasNext());
        assertEquals("", c.nextString());
        assertFalse(c.hasNext());
    }

    @Test
    void testEncapsulated() {
        Command c = new Command("\"Der Disi\n\" \\\"Der Disi\"5\" 8");
        assertTrue(c.hasNext());
        assertEquals("Der Disi\n", c.nextString());
        assertTrue(c.hasNext());
        assertEquals("\"Der", c.nextString());
        assertTrue(c.hasNext());
        assertEquals("Disi5", c.nextString());
        assertTrue(c.hasNext());
        assertEquals(8, c.nextInt());
        assertFalse(c.hasNext());
        assertEquals("", c.nextString());
        assertFalse(c.hasNext());
    }

    @Test
    void testRemainingAndConsumeChar() {
        Command c = new Command("!custom set knock knock knock who is there ?");
        assertFalse(c.consumeChar('?'));
        assertTrue(c.consumeChar('!'));
        assertFalse(c.consumeChar('!'));
        assertEquals("custom", c.peekString());
        assertEquals("custom", c.peekString());
        assertEquals("custom", c.nextString());
        assertEquals("set", c.nextString());
        assertEquals("knock", c.nextString());
        assertEquals("knock knock who is there ?", c.remaining());
        assertEquals("", c.remaining());
    }

    @Test
    void testToString() {
        Command c = new Command("fehitslfws sjvi  eeofhiw\"\\");
        assertEquals("fehitslfws sjvi  eeofhiw\"\\", c.toString());
        c.nextString();
        assertEquals("fehitslfws sjvi  eeofhiw\"\\", c.toString());
    }

    @Test
    void testCustomEmote() {
        FakeJDA jda = new FakeJDA();
        Command containsEmote = new Command("fehitslfws <:zyra:65984>");
        assertFalse(containsEmote.isNextCustomEmote());
        assertNull(containsEmote.nextEmote(jda));
        containsEmote.nextString();
        assertTrue(containsEmote.isNextCustomEmote());
        assertNotNull(containsEmote.nextEmote(jda));
    }
}

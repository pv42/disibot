package disibot.util;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class UtilTest {
    @Test
    void testReadTokenFile() throws IOException {
        assertEquals("TEST_TOKEN", Util.readTokenFile("testres/test_token_file"));
    }

    @Test
    void testReadTokenFileError() {
        assertThrows(IOException.class, () -> Util.readTokenFile("testres/no_exist_file"));
    }
}

package disibot.util;

import disibot.command.MainCommandHandler;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PairTest extends Pair<String, String> {

    public PairTest() {
        super("key", "value");
    }

    @Test
    void test() {
        assertEquals("key",getKey());
        assertEquals("value", getValue());
        setKey("yy");
        assertEquals("yy", getKey());
        setValue(null);
        assertNull(getValue());
    }
}
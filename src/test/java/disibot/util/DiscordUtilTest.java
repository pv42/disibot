package disibot.util;


import fakejda.FakeMessageChannel;
import fakejda.FakeTextChannel;
import org.junit.jupiter.api.Test;

import static junit.framework.Assert.assertFalse;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class DiscordUtilTest {

    @Test
    void testSendLongMessageWithLB() {
        FakeMessageChannel channel = new FakeTextChannel();
        String msgPart = "15 long string\n";
        int repeatCount = 150;
        DiscordUtil.sendLongMessage(msgPart.repeat(repeatCount), channel);
        assertEquals(2, channel.getMessages().size());
        String msg0 = channel.getMessages().get(0).getContentRaw();
        assertEquals(2000 - 2000 % msgPart.length(), msg0.length());
        assertEquals((repeatCount - ((2000 - 2000 % msgPart.length()) / msgPart.length())) * msgPart.length(), channel.getMessages().get(1).getContentRaw().length());
        assertEquals("\n", msg0.substring(msg0.length() - 1));
    }

    @Test
    void testSendLongMessageCode() {
        FakeMessageChannel channel = new FakeTextChannel();
        String msgPart = "15 long string\n";
        int repeatCount = 150;
        DiscordUtil.sendLongMessage("```" + msgPart.repeat(repeatCount) + "```", channel);
        assertEquals(2, channel.getMessages().size());
        String msg0 = channel.getMessages().get(0).getContentRaw();
        String msg1 = channel.getMessages().get(1).getContentRaw();
        assertTrue(msg0.startsWith("```"));
        assertTrue(msg0.endsWith("```"));
        assertTrue(msg1.startsWith("```"));
        assertTrue(msg1.endsWith("```"));
        assertEquals(2000 - (2000-6) % msgPart.length(), msg0.length());
        assertEquals(6 + (repeatCount - ((2000 - (2000-6) % msgPart.length()) / msgPart.length())) * msgPart.length(), msg1.length());
        assertEquals("\n", msg0.substring(msg0.length() - 1 - 3, msg0.length() -3));
    }

    @Test
    void testSendLongMessageOnlyStartCode() {
        FakeMessageChannel channel = new FakeTextChannel();
        String msgPart = "15 long string\n";
        int repeatCount = 150;
        DiscordUtil.sendLongMessage("```" + msgPart.repeat(repeatCount), channel);
        assertEquals(2, channel.getMessages().size());
        String msg0 = channel.getMessages().get(0).getContentRaw();
        String msg1 = channel.getMessages().get(1).getContentRaw();
        assertTrue(msg0.startsWith("```"));
        assertFalse(msg0.endsWith("```"));
        assertFalse(msg1.startsWith("```"));
        assertFalse(msg1.endsWith("```"));
        assertEquals(2000 - (2000-3) % msgPart.length(), msg0.length());
        assertEquals((repeatCount - ((2000 - (2000-3) % msgPart.length()) / msgPart.length())) * msgPart.length(), msg1.length());
        assertEquals("\n", msg0.substring(msg0.length() - 1));
    }

    @Test
    void testSendLongMessageNoLB() {
        FakeMessageChannel channel = new FakeTextChannel();
        String msgPart = "15-long-string-";
        int repeatCount = 150;
        DiscordUtil.sendLongMessage(msgPart.repeat(repeatCount), channel);
        assertEquals(2, channel.getMessages().size());
        String msg0 = channel.getMessages().get(0).getContentRaw();
        assertEquals(2000, msg0.length());
        assertEquals(repeatCount * msgPart.length() - 2000, channel.getMessages().get(1).getContentRaw().length());
        assertEquals("o", msg0.substring(msg0.length() - 1));
    }

    @Test
    void testSendException() {
        FakeMessageChannel channel = new FakeTextChannel();
        try {
            Object o = null;
            o.equals(null); // cause a null pointer exception
        } catch (NullPointerException e) {
            DiscordUtil.sendException(e, channel);
        }
        System.out.println(channel.getMessages().get(0).getContentRaw());
        assertTrue(channel.getMessages().get(0).getContentRaw().startsWith("java.lang.NullPointerException" + String.format("%n") +
                "\tat disibot.util.DiscordUtilTest.testSendException(DiscordUtilTest.java:")); // rest is env dependent
    }

    @Test
    void testIsCustomEmote() {
        assertTrue(DiscordUtil.isStringCustomEmote("<:zyra:65984>"));
        assertTrue(DiscordUtil.isStringCustomEmote("<a:moving_zyra:58493>"));
        assertFalse(DiscordUtil.isStringCustomEmote("<:zyra:>"));
        assertFalse(DiscordUtil.isStringCustomEmote(":zyra:65984>"));
        assertFalse(DiscordUtil.isStringCustomEmote("<::65984>"));
        assertFalse(DiscordUtil.isStringCustomEmote("<65984>"));
    }
}

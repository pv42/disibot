package disibot.custom;

import disibot.command.CommandHandler;
import disibot.modules.DisiBotModule;
import disibot.modules.DisiBotModuleInterface;
import disibot.newcommand.SlashCommandHandler;
import fakejda.FakeJDA;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;

public class CustomModuleTest {
    @Test
    void test() {
        FakeJDA jda = new FakeJDA();
        DisiBotModule module = new CustomCommandModuleFactory().initiateModule(null, null, null, null, jda, null, null, null);
        DisiBotModuleInterface moduleInterface = mock(DisiBotModuleInterface.class);
        final boolean[] commandHandlerAdded = {false};
        doAnswer((arg) -> commandHandlerAdded[0] = true).when(moduleInterface).
                addSlashCommandHandler(eq(module), any(SlashCommandHandler.class));
        module.load(moduleInterface);
        assertTrue(commandHandlerAdded[0]);
    }
}

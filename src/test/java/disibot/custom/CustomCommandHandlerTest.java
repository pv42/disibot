package disibot.custom;

import disibot.command.AbstractCommandHandlerTester;
import disibot.util.Command;
import fakejda.FakeJDA;
import fakejda.FakeMember;
import fakejda.FakeMessage;
import fakejda.FakeMessageEvent;
import fakejda.FakeMessageReceivedEvent;
import fakejda.FakeUser;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

//todo fix for slash test
public class CustomCommandHandlerTest /*extends AbstractCommandHandlerTester<CustomCommandHandler>*/ {
    //private final FakeMember owner;

    public CustomCommandHandlerTest() {
        //handler = new CustomCommandHandler(null, null);
        FakeUser ownerUser = new FakeUser(32);
        //owner = new FakeMember(guild, ownerUser);
        ownerUser.setName("FakeOwner");
        //guild.setOwner(owner);

    }

    private void sendOwnerCommand(String command) {
        //FakeMessage message = new FakeMessage("!" + command, channel);
    }

    @Test
    void testHandleSetUseListDelete() {
        //sendCommand("custom set");
        //assertLastMessage("Syntax error! See !custom help");
        //sendCommand("custom set abc");
        //assertLastMessage("Syntax error! See !custom help");
        //sendCommand("custom set abc abcdef");
        //assertLastMessage("Permission denied.");
        //sendOwnerCommand("custom set abc Abcdef");
        //assertLastMessage("Custom command abc set.");
        //sendCommand("abc");
        //assertLastMessage("Abcdef");
        //sendOwnerCommand("custom list");
        //assertLastMessage("Custom commands are:\n" +
        //        "abc");
        //sendOwnerCommand("custom remove abc");
        //assertLastMessage("Removed custom command abc.");
        //sendCommand("abc");
        //assertLastMessage("Removed custom command abc."); // nothing new
    }

    @Test // todo fix for slash commands
    void testHandleInvalid() {
        //sendCommand("custom abc");
        //assertLastMessage("Syntax error! See !custom help");
        //sendCommand("custom abc abcd");
        //assertLastMessage("Did you mean `!custom set abc abcd` ?");
        //sendOwnerCommand("custom set \"ab c\"   AB C");
        //assertLastMessage("Could not set custom command: Command can't contain whitespaces");
    }

    @Test // todo fix for slash commands
    void testInvalidRemove() {
        //sendOwnerCommand("custom remove command_does_not_exist");
        //assertLastMessage("Could not remove custom command: Command does not exist");
        //sendCommand("custom remove");
        //assertLastMessage("Syntax error! See !custom help");
    }

    @Test // todo fix for slash commands
    void testCanHandle() {
        //noinspection ConstantConditions
        //assertTrue(handler.canHandleCommand(new Command("custom"), null));
        Command cmd = new Command("cxdavm");
        FakeJDA jda = new FakeJDA();
        //MessageReceivedEvent ev = new FakeMessageReceivedEvent(jda, 0, new FakeMessage(cmd.toString(), channel));
        //assertFalse(handler.canHandleCommand(cmd, ev));
    }
}

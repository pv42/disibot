package disibot.config;

import disibot.command.AbstractCommandHandlerTester;
import disibot.util.Command;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class ConfigCommandHandlerTest extends AbstractCommandHandlerTester<ConfigCommandHandler> {
    public ConfigCommandHandlerTest() {
        handler = new ConfigCommandHandler(null, null);
    }

    @AfterAll
    @BeforeAll
    static void deleteFile() throws IOException {
        if (Files.exists(Path.of("Server.0.json"))) {
            Files.delete(Path.of("Server.0.json"));
        }
        if (Files.exists(Path.of("Server.2.json"))) {
            Files.delete(Path.of("Server.2.json"));
        }
    }

    @Test
    void testCanHandle() {
        assertTrue(handler.canHandleCommand(new Command("config"), null));
        assertFalse(handler.canHandleCommand(new Command(""), null));
        assertFalse(handler.canHandleCommand(new Command("configef"), null));
        assertFalse(handler.canHandleCommand(new Command("sdconfig"), null));
    }
}

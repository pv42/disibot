package disibot.reminder;

import fakejda.FakeJDA;
import fakejda.FakeUser;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Calendar;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class ReminderStorageTest extends ReminderStorage {

    private FakeUser user;

    public ReminderStorageTest() {
        super(new FakeJDA());
    }

    @BeforeAll
    void beforeAll() throws IOException {
        user = new FakeUser(3);
        clear();
    }

    @AfterEach
    void cleanUp() throws IOException {
        clear();
        if(Files.exists(Paths.get("Server.0.json"))) {
            Files.delete(Paths.get("Server.0.json"));
        }
    }

    @Test
    void testAddReminder() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(2030, Calendar.JANUARY, 0); // 1.1.2030
        Date date = new Date();
        addReminder(new Reminder(user, calendar.getTime(),date, "", "tesxt"));
        assertEquals(1, getReminders().size());
        Reminder reminder = getReminders().get(0);
        assertEquals(new Reminder(user, calendar.getTime(),date, "", "tesxt"), reminder);
        assertEquals(user, reminder.getUser());
        assertEquals(calendar.getTime(), reminder.getReminderTimestamp());
        assertEquals("tesxt", reminder.getMessage());
    }

    @Test
    void testGetAndRemoveDueReminders() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(1970, Calendar.JANUARY, 9);
        addReminder(new Reminder(user, calendar.getTime(), new Date(), ""));
        calendar.set(2100,Calendar.JANUARY,1);
        addReminder(new Reminder(user, calendar.getTime(), new Date(), ""));
        assertEquals(1, getAndRemoveDueReminders(new Date()).size());
        assertEquals(1, getReminders().size());
        assertEquals(0, getAndRemoveDueReminders(new Date()).size());
        assertEquals(1, getReminders().size());

    }

    @Test
    void testGetRemindersByUser() {
        Date date = new Date();
        addReminder(new Reminder(user, date, new Date(), ""));
        addReminder(new Reminder(new FakeUser(99), date, new Date(), ""));
        assertEquals(1, getRemindersByUser(user).size());
    }

    @Test
    void testSaveLoad() {
        //if (0==0) return;
        Calendar calendar = Calendar.getInstance();
        calendar.set(2030, Calendar.JANUARY, 0); // 1.1.2030
        Date date = new Date();
        addReminder(new Reminder(user, calendar.getTime(), date, "", "tesxt"));
        ReminderStorage otherStorage = new ReminderStorage(new FakeJDA()); // todo
        assertEquals(1, otherStorage.getReminders().size());
        Reminder reminder = otherStorage.getReminders().get(0);
        assertEquals(new Reminder(user, calendar.getTime(),date, "", "tesxt"), reminder);
    }
}
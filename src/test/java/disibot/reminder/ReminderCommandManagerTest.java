package disibot.reminder;

import disibot.command.AbstractCommandHandlerTester;
import disibot.context.ContextStorage;
import fakejda.FakeJDA;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

// todo this whole file -> new slash commands
public class ReminderCommandManagerTest extends AbstractCommandHandlerTester {
    private FakeJDA jda;

    @AfterAll
    @BeforeAll
    static void deleteFiles() throws IOException {
        if (Files.exists(Path.of("Server.0.json"))) {
            Files.delete(Path.of("Server.0.json"));
        }
        if (Files.exists(Path.of("Server.2.json"))) {
            Files.delete(Path.of("Server.2.json"));
        }
    }

    @BeforeEach
    void prepare() {
        try {
            Files.delete(Paths.get("reminders.json"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        jda = new FakeJDA();

        //handler = new ReminderCommandManager(new ContextStorage(), jda, new ReminderStorage(jda));
    }


    void testInvalidTime() {
        sendCommand("remindme sdfkdhg");
        assertLastMessage("The time 'sdfkdhg' is not supported.");
    }


    void testValid() {
        sendCommand("remindme now");
        assertLastMessageMatch("You will be reminded on ([A-Z][a-z]{2} ){2}\\d{2} \\d{2}:\\d{2}:\\d{2} [A-Z]+ \\d{4}\\.");
    }

    void testListReminders() {
        sendCommand("reminders list");
        assertLastMessage("You have 0 reminders.");
        sendCommand("remindme 1h msg");
        sendCommand("remindme 2h");
        sendCommand("reminders list");
        assertLastMessageMatch("You have 2 reminders:\n" +
                "``` {2}#\\|Date {25}\\|Message {4}\\|Type\n" +
                "  0\\|[A-z]{3} [A-z]{3} \\d\\d (\\d\\d:){2}\\d\\d CES?T \\d{4}\\|msg {8}\\|private\n" +
                "  1\\|[A-z]{3} [A-z]{3} \\d\\d (\\d\\d:){2}\\d\\d CES?T \\d{4}\\| {11}\\|private\n" +
                "```");
    }
}

package disibot.reminder;

import com.google.gson.JsonSyntaxException;
import fakejda.FakeJDA;
import net.dv8tion.jda.api.JDA;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.fail;

class ReminderStorageDataTest {

    private JDA jda = new FakeJDA();
    private ReminderStorageData data;

    private static void assertStorageEquals(ReminderStorageData expected, ReminderStorageData actual) {
        assertNotNull(expected);
        assertNotNull(actual);
        assertEquals(expected.getReminders().size(), actual.getReminders().size());
        for (int i = 0; i < expected.getReminders().size(); i++) {
            Reminder expReminder = expected.getReminders().get(i);
            Reminder actReminder = actual.getReminders().get(i);
            assertEquals(expReminder, actReminder);
            assertEquals(expReminder.getMessage(), actReminder.getMessage());
            assertEquals(expReminder.getUser(), actReminder.getUser());
            assertEquals(expReminder.getReminderTimestamp(), actReminder.getReminderTimestamp());
        }
    }

    @BeforeEach
    void prepare() {
        data = new ReminderStorageData();
    }

    @Test
    void saveAndLoad() {
        Date date = new Date();
        Reminder reminder = new Reminder(jda.getUserById(5), date, date, "dsgh");
        Reminder reminder2 = new Reminder(jda.getUserById(3), date, date, "");
        data.getReminders().add(reminder);
        ReminderStorageData.save("test_rmd.json", data, jda);
        ReminderStorageData other = ReminderStorageData.load("test_rmd.json", jda);
        assertStorageEquals(data, other);
        // 2 rems
        data.getReminders().add(reminder2);
        ReminderStorageData.save("test_rmd.json", data, jda);
        other = ReminderStorageData.load("test_rmd.json", jda);
        assertStorageEquals(data, other);
        Reminder reminder3 = new Reminder(jda.getUserById(2), new Date(), new Date(), "");
        for (int i = 0; i < 20; i++) {
            data.getReminders().add(new Reminder(jda.getUserById(8), new Date(), new Date(), "", "__asm__"));
        }
        data.getReminders().add(reminder3);
        ReminderStorageData.save("test_rmd.json", data, jda);
        other = ReminderStorageData.load("test_rmd.json", jda);
        assertStorageEquals(data, other);
    }

    @Test
    void testCreate() {
        data = ReminderStorageData.load("this_file_does_not_exist.json", null);
        assertEquals(0, data.getReminders().size());
        Files.exists(Paths.get("this_file_does_not_exist.json"));
        try {
            Files.delete(Paths.get("this_file_does_not_exist.json"));
        } catch (IOException e) {
            fail(e);
        }
    }

    @Test
    void testCreatePathIsDir() {
        try {
            Files.createDirectories(Paths.get("dir.json"));
        } catch (IOException e) {
            fail(e);
        }
        assertThrows(IllegalStateException.class, () -> ReminderStorageData.load("dir.json",null));
        try {
            Files.delete(Paths.get("dir.json"));
        } catch (IOException e) {
            fail(e);
        }
    }

    @Test
    void loadNoJDA() {
        Date date = new Date();
        Reminder reminder = new Reminder(jda.getUserById(5), date, date, "dsgh");
        data.getReminders().add(reminder);
        ReminderStorageData.save("test_rmd.json", data, jda);
        assertThrows(JsonSyntaxException.class, () -> ReminderStorageData.load("test_rmd.json", null));
        // JsonSyntaxException caused by IOException: Can't parse user without a jda
    }
}
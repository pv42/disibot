package disibot.reminder;

import disibot.modules.DisiBotModule;
import disibot.modules.DisiBotModuleInterface;
import disibot.tasks.Task;
import disibot.tasks.TaskRunner;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;

public class ReminderModuleTest {
    //todo update for new version @Test
    void test() {
        TaskRunner runner = mock(TaskRunner.class);
        DisiBotModule module = new ReminderModuleFactory().initiateModule(null, null, null, null, null, null, null, runner);
        DisiBotModuleInterface moduleInterface = mock(DisiBotModuleInterface.class);
        final boolean[] commandHandlerAdded = {false};
        final boolean[] taskAddOk = {false};
        // todo check for new command handler
        doAnswer((arg) -> taskAddOk[0] = true).when(moduleInterface)
                .addTask(eq(module), any(Task.class));
        module.load(moduleInterface);
        assertTrue(commandHandlerAdded[0]);
        assertTrue(taskAddOk[0]);
    }
}

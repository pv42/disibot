package disibot.reminder;

import org.junit.jupiter.api.Test;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.UnknownFormatConversionException;

import static java.util.Calendar.DAY_OF_MONTH;
import static java.util.Calendar.DAY_OF_WEEK;
import static java.util.Calendar.DECEMBER;
import static java.util.Calendar.HOUR;
import static java.util.Calendar.JANUARY;
import static java.util.Calendar.MINUTE;
import static java.util.Calendar.MONTH;
import static java.util.Calendar.SECOND;
import static java.util.Calendar.YEAR;
import static org.junit.jupiter.api.Assertions.*;

public class DateFormaterTester {
    @Test
    void testOverflows() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(1999, DECEMBER, 31);
        calendar.add(DAY_OF_WEEK, 1);
        assertEquals(1, calendar.get(DAY_OF_MONTH));
        assertEquals(JANUARY, calendar.get(MONTH));
        assertEquals(2000, calendar.get(YEAR));
    }

    @Test
    void testRegex() {
        assertTrue("2days 2edgli'".matches("^[0-9]+\\s?[dD]ays?(\\s.*)?"));
    }

    @Test
    void testHHMM() {
        //HHMMSS
        Calendar older = Calendar.getInstance();
        Calendar expected = Calendar.getInstance();
        Calendar actual = Calendar.getInstance();
        Date date = null;
        expected.set(HOUR, 12);
        expected.set(MINUTE, 8);
        expected.set(SECOND, 42);
        if (expected.before(older)) expected.add(DAY_OF_MONTH, 1);
        try {
            date = RemindMeCommandHandler.parseTimeString("12:08:42");
        } catch (ParseException e) {
            fail();
        }
        actual.setTime(date);
        assertEquals(expected.get(HOUR), actual.get(HOUR));
        assertEquals(expected.get(MINUTE), actual.get(MINUTE));
        assertEquals(expected.get(SECOND), actual.get(SECOND));
        // HHMM
        older = Calendar.getInstance();
        expected = Calendar.getInstance();
        expected.set(HOUR, 13);
        expected.set(MINUTE, 19);
        expected.set(SECOND, 0);
        if (expected.before(older)) expected.add(DAY_OF_MONTH, 1);
        expected.set(SECOND, 0);
        try {
            date = RemindMeCommandHandler.parseTimeString("13:19");
        } catch (ParseException e) {
            fail();
        }
        actual.setTime(date);
        assertEquals(expected.get(HOUR), actual.get(HOUR));
        assertEquals(expected.get(MINUTE), actual.get(MINUTE));
        assertEquals(expected.get(SECOND), actual.get(SECOND));
        //HMMSS
        older = Calendar.getInstance();
        expected = Calendar.getInstance();
        expected.set(HOUR, 2);
        expected.set(MINUTE, 8);
        expected.set(SECOND, 42);
        if (expected.before(older)) expected.add(DAY_OF_MONTH, 1);
        try {
            date = RemindMeCommandHandler.parseTimeString("2:08:42");
        } catch (ParseException e) {
            fail();
        }
        actual.setTime(date);
        assertEquals(expected.get(HOUR), actual.get(HOUR));
        assertEquals(expected.get(MINUTE), actual.get(MINUTE));
        assertEquals(expected.get(SECOND), actual.get(SECOND));
        // HMM
        older = Calendar.getInstance();
        expected = Calendar.getInstance();
        expected.set(HOUR, 3);
        expected.set(MINUTE, 19);
        expected.set(SECOND, 0);
        if (expected.before(older)) expected.add(DAY_OF_MONTH, 1);
        expected.set(SECOND, 0);
        try {
            date = RemindMeCommandHandler.parseTimeString("3:19");
        } catch (ParseException e) {
            fail();
        }
        actual.setTime(date);
        assertEquals(expected.get(HOUR), actual.get(HOUR));
        assertEquals(expected.get(MINUTE), actual.get(MINUTE));
        assertEquals(expected.get(SECOND), actual.get(SECOND));
    }

    @Test
    void testParse() {
        Calendar expected = Calendar.getInstance();
        Calendar actual = Calendar.getInstance();
        Date date = null;
        //now
        try {
            date = RemindMeCommandHandler.parseTimeString("now");
        } catch (ParseException e) {
            fail();
        }
        actual.setTime(date);
        assertTrue(actual.getTime().getTime() - expected.getTime().getTime() < 1e3); // less then 1000 ms
        assertTrue(actual.getTime().getTime() - expected.getTime().getTime() >= 0); // positive
        // fails
        assertThrows(UnknownFormatConversionException.class, () -> RemindMeCommandHandler.parseTimeString("null"));
        assertThrows(UnknownFormatConversionException.class, () -> RemindMeCommandHandler.parseTimeString("32.02.2017"));
        assertThrows(UnknownFormatConversionException.class, () -> RemindMeCommandHandler.parseTimeString("55:32:18"));
        assertThrows(Exception.class, () -> RemindMeCommandHandler.parseTimeString(null));
        // ddmmyyyy
        expected.set(2001, JANUARY, 29);
        try {
            date = RemindMeCommandHandler.parseTimeString("29.1.2001");
        } catch (ParseException e) {
            fail();
        }
        actual.setTime(date);
        assertEquals(expected.get(DAY_OF_MONTH), actual.get(DAY_OF_MONTH));
        assertEquals(expected.get(MONTH), actual.get(MONTH));
        assertEquals(expected.get(YEAR), actual.get(YEAR));
        // ddmm
        expected.set(2023, JANUARY, 29); // todo
        try {
            date = RemindMeCommandHandler.parseTimeString("29.1.");
        } catch (ParseException e) {
            fail();
        }
        actual.setTime(date);
        assertEquals(expected.get(DAY_OF_MONTH), actual.get(DAY_OF_MONTH));
        assertEquals(expected.get(MONTH), actual.get(MONTH));
        assertEquals(expected.get(YEAR), actual.get(YEAR));
    }

    @Test
    void testRelatives() {
        Date actual = null;
        //yeers
        Calendar expected = Calendar.getInstance();
        expected.add(YEAR, 5);
        try {
            actual = RemindMeCommandHandler.parseTimeString("5years 3fdgt");
        } catch (ParseException e) {
            fail();
        }
        assertSlightlyBefore(expected.getTime(), actual);
        //months
        expected = Calendar.getInstance();
        expected.add(MONTH, 1);
        try {
            actual = RemindMeCommandHandler.parseTimeString("1 Month 3fdgt");
        } catch (ParseException e) {
            fail();
        }
        assertSlightlyBefore(expected.getTime(), actual);
        //weeks
        expected = Calendar.getInstance();
        expected.add(Calendar.WEEK_OF_YEAR, 4);
        try {
            actual = RemindMeCommandHandler.parseTimeString("4 weeks 3fdgt");
        } catch (ParseException e) {
            fail();
        }
        assertSlightlyBefore(expected.getTime(), actual);
        //days
        expected = Calendar.getInstance();
        expected.add(DAY_OF_MONTH, 50);
        try {
            actual = RemindMeCommandHandler.parseTimeString("50 Days");
        } catch (ParseException e) {
            fail();
        }
        assertSlightlyBefore(expected.getTime(), actual);
        // hour
        expected = Calendar.getInstance();
        expected.add(HOUR, 13);
        try {
            actual = RemindMeCommandHandler.parseTimeString("13h 3fdgt");
        } catch (ParseException e) {
            fail();
        }
        assertSlightlyBefore(expected.getTime(), actual);

        // mins
        expected = Calendar.getInstance();
        expected.add(MINUTE, 3);
        try {
            actual = RemindMeCommandHandler.parseTimeString("3 Minutes 3fdgt");
        } catch (ParseException e) {
            fail();
        }
        assertSlightlyBefore(expected.getTime(), actual);
    }

    void assertBeforeOrEqual(Date expectedBefore, Date expectedAfter) {
        assertFalse(expectedBefore.after(expectedAfter), "Expected " + expectedBefore + " to be before " + expectedAfter + ", but was after.");
    }

    void assertSlightlyBefore(Date expectedBefore, Date expectedAfter) {
        assertBeforeOrEqual(expectedBefore, expectedAfter);
        Date slightlyBeforeAfter = new Date(expectedAfter.getTime() - 1000);
        assertBeforeOrEqual(slightlyBeforeAfter, expectedBefore);
    }

}

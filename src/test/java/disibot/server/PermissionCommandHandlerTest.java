package disibot.server;

import disibot.command.AbstractCommandHandlerTester;
import disibot.context.ContextStorage;
import disibot.newcommand.AbstractSlashCommandHandlerTester;
import disibot.util.Command;
import fakejda.FakeGuild;
import fakejda.FakeMember;
import fakejda.FakeMessage;
import fakejda.FakeMessageEvent;
import fakejda.FakePrivateChannel;
import fakejda.FakeRole;
import fakejda.FakeSlashCommandEvent;
import fakejda.FakeTextChannel;
import fakejda.FakeUser;
import net.dv8tion.jda.api.entities.User;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;


public class PermissionCommandHandlerTest extends AbstractSlashCommandHandlerTester<PermissionCommandHandler> {

    private final FakeMember owner;

    public PermissionCommandHandlerTest() {

        handler = new PermissionCommandHandler(new ContextStorage());


        guild = new FakeGuild("PCHT_FG");
        FakeUser ownerUser = new FakeUser(32);
        FakeUser user1 = new FakeUser(33);
        guild.addMember(user1);
        owner = new FakeMember(guild, ownerUser);
        ownerUser.setName("FakeOwner");
        guild.setOwner(owner);
        channel = new FakeTextChannel(guild);
        ServerFactory.reset();
    }

    @AfterAll
    @BeforeAll
    static void deleteFile() throws IOException {
        if (Files.exists(Path.of("Server.0.json"))) {
            Files.delete(Path.of("Server.0.json"));
            System.out.println("Deleted server 0 data");
        }
        if (Files.exists(Path.of("Server.2.json"))) {
            Files.delete(Path.of("Server.2.json"));
            System.out.println("Deleted server 2 data");
        }
        if (Files.exists(Path.of("Server.1.json"))) {
            Files.delete(Path.of("Server.1.json"));
            System.out.println("Deleted server 1 data");
        }
    }

    @BeforeEach
    void setUp() throws IOException {
        if (Files.exists(Path.of("Server.2.json"))) {
            Files.delete(Path.of("Server.2.json"));
            System.out.println("Deleted server 2 data _");
        }
    }

    @Test
    void testHelp() {
        sendCommand("permission");
        assertReply("Internal Error");
        Map<String, String> args = new HashMap<>();
        args.put("role", "dsf");
        sendCommand("permission","setrole", args);
        assertReply("Internal Error");
        args.clear();
        args.put("permission", "dsf");
        sendCommand("permission","setpermission", args);
        assertReply("Internal Error");
        sendCommand("permission","see", null);
        assertReply("Internal Error");
    }

    @Test
    void testSetRole() {
        Map<String, String> args = new HashMap<>();
        args.put("role", "NotARole");
        args.put("power", "0");
        sendOwnerCommand("permission", "setrole", args);
        assertReply("This role does not exist on this server.");
        FakeRole role = new FakeRole(5, "notofintrest");
        guild.addRole(role);
        role = new FakeRole(3, "ARole");
        guild.addRole(role);
        args.put("role", "ARole");
        args.put("power", "23");
        sendOwnerCommand("permission","setrole",args);
        assertReply("Permission power of ARole set to 23.");
        args.put("power", "ewt");
        sendOwnerCommand("permission","setrole",args);
        assertReply("```Invalid Syntax! The syntax must be:\n" +
                "/permission setrole ROLE POWER \n" +
                "ROLE must be a role on the Server, if the role contains spaces it must be encapsulated by \"\n" +
                "POWER must be a whole number between 0 and 100```");
        args.put("power", "200");
        sendOwnerCommand("permission","setrole", args);
        assertReply("```Invalid Syntax! The syntax must be:\n" +
                "/permission setrole ROLE POWER \n" +
                "ROLE must be a role on the Server, if the role contains spaces it must be encapsulated by \"\n" +
                "POWER must be a whole number between 0 and 100```");
    }

    @Test
    void testSetPermission() {
        Map<String, String> args = new HashMap<>();
        args.put("permission", "invalid_permission");
        args.put("power", "5");
        sendOwnerCommand("permission","setpermission",args);
        assertReply("invalid_permission is not a valid permission");
        args.put("permission", "seeownpower");
        args.put("power", "a");
        sendOwnerCommand("permission","setpermission" ,args);
        assertReply("Not a valid number");
        args.put("permission", "seeownpower");
        args.put("power", "999");
        sendOwnerCommand("permission","setpermission", args);
        assertReply("Power must be between 0 and 100");
        args.put("permission", "seeownpower");
        args.put("power", "13");
        sendOwnerCommand("permission","setpermission" ,args);
        assertReply("Required permission power for seeownpower set to 13.");
    }

    @Test
    void testList() {
        sendCommand("permission","list", null);
        assertReply("Permission denied.");
        sendOwnerCommand("permission","list",null);
        assertReply("Available permissions:\n" +
                "autoremove(50), seeownpower(0), seeotherpower(20), seepermissionpower(10), editcustomcommands(30), listcustomcommands(40), useascontext(5), playaudio(30)");
    }

    @Test
    void testSee() {
        Map<String,String> args = new HashMap<>();
        args.put("name","NotAMember");
        sendOwnerCommand("permission","see", args);
        assertReply("NotAMember is not a member of this server.");
        args.put("name", "FakeUser");
        sendOwnerCommand("permission","see",args);
        assertReply("FakeUser's current permission power is 0.");
        args.put("name", "FakeOwner");
        sendOwnerCommand("permission","see",args);
        assertReply("FakeOwner's current permission power is 100.");
    }

    private void sendOwnerCommand(String command, String subcommand, Map<String,String> args) {
        User tmp = sender;
        sender = owner.getUser();
        sendCommand(command, subcommand, args);
        sender = tmp;
    }
}


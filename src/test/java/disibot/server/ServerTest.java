package disibot.server;

import disibot.command.InsufficientPowerException;
import fakejda.FakeGuild;
import fakejda.FakeMember;
import fakejda.FakeRole;
import fakejda.FakeUser;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static disibot.server.Server.PERMISSION_SEE_OTHER_POWER;
import static disibot.server.Server.PERMISSION_SEE_OWN_POWER;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;


public class ServerTest {
    FakeGuild guild;
    FakeUser user;
    FakeMember owner;
    FakeMember member;
    Server server;
    FakeRole role;

    @BeforeEach
    void init() {
        guild = new FakeGuild("ServerTestGuild");
        FakeUser ownerUser = new FakeUser(5);
        owner = (FakeMember) guild.getMember(user);
        guild.setOwner(owner);
        //guild.addRole(role);
        user = new FakeUser(98);
        member = (FakeMember) guild.getMember(user);

        role = new FakeRole(4);
        guild.addRoleToMember(member, role).complete();
        server = new Server(guild);
    }

    @Test
    void testPermissions() throws InsufficientPowerException {
        server.setRolePowerLevel(owner, role, 0);
        server.setPermissionsPowerRequirement(owner, PERMISSION_SEE_OWN_POWER,1);
        assertFalse(server.canChangePermissions(member));
        assertFalse(server.canAutoremoveEmote(member));
        assertFalse(server.canSeeOwnPower(member));
        assertFalse(server.canSeeOtherPower(member));
        assertFalse(server.canSeePermissionPower(member));
        assertFalse(server.canEditCustomCommands(member));
        //assertFalse(server.canListCustomCommands(member));
        assertFalse(server.canUseAsContext(member));
        assertFalse(server.canPlayAudio(member));
        server.setRolePowerLevel(owner, role, 100);
        assertTrue(server.canChangePermissions(member));
        assertTrue(server.canAutoremoveEmote(member));
        assertTrue(server.canSeeOwnPower(member));
        assertTrue(server.canSeeOtherPower(member));
        assertTrue(server.canSeePermissionPower(member));
        assertTrue(server.canEditCustomCommands(member));
        //assertTrue(server.canListCustomCommands(member));
        assertTrue(server.canUseAsContext(member));
        assertTrue(server.canPlayAudio(member));
    }

    @Test
    void testSetRolePwr() throws InsufficientPowerException {
        server.setRolePowerLevel(owner, role, 100);
        server.setRolePowerLevel(owner, role, 0);
        assertThrows( IllegalArgumentException.class, () -> server.setRolePowerLevel(owner, role, -1));
        assertThrows( IllegalArgumentException.class, () -> server.setRolePowerLevel(owner, role, 101));
        assertThrows( InsufficientPowerException.class, () -> server.setRolePowerLevel(member, role, -1));
    }

    @Test
    void testGetOwnPwr() throws InsufficientPowerException {
        server.setPermissionsPowerRequirement(owner, PERMISSION_SEE_OWN_POWER,4);
        server.setRolePowerLevel(owner, role, 5);
        assertEquals(5,server.getOwnPower(member));
        server.setPermissionsPowerRequirement(owner, PERMISSION_SEE_OWN_POWER,6);
        assertThrows(InsufficientPowerException.class, () -> server.getOwnPower(member));
    }

    @Test
    void testGetOtherPwr() throws InsufficientPowerException {
        server.setRolePowerLevel(owner, role, 3);
        server.setPermissionsPowerRequirement(owner, PERMISSION_SEE_OTHER_POWER,50);
        assertThrows(InsufficientPowerException.class, () -> server.getMemberPower(member, owner));
        assertEquals(3, server.getMemberPower(owner, member));
    }
}

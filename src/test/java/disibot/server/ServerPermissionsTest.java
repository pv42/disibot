package disibot.server;

import disibot.command.InsufficientPowerException;
import fakejda.FakeGuild;
import fakejda.FakeMember;
import fakejda.FakeUser;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.fail;

public class ServerPermissionsTest {
    FakeMember privileged;
    FakeMember noRights;
    Server server;
    final static String[] permissions = new String[]{
            "autoremove",
            "seeownpower",
            "seeotherpower",
            "seepermissionpower",
            "editcustomcommands",
            "listcustomcommands",
            "useascontext",
            "playaudio",
    };

    @BeforeEach
    void setup() {
        FakeGuild guild = new FakeGuild("testsetgetpermission");
        server = new Server(guild);
        FakeUser user = new FakeUser(0);
        FakeUser user2 = new FakeUser(996);
        privileged = new FakeMember(guild, user);
        noRights = new FakeMember(guild, user2);
    }

    @Test
    void testFailSetGet() {
        assertThrows(IllegalArgumentException.class, () -> server.setPermissionsPowerRequirement(privileged, "doesnotexist", 0));
        assertThrows(InsufficientPowerException.class, () -> server.setPermissionsPowerRequirement(noRights, "autoremove", 0));
        assertThrows(IllegalArgumentException.class, () -> server.setPermissionsPowerRequirement(privileged, "autoremove", -1));
        assertThrows(IllegalArgumentException.class, () -> server.setPermissionsPowerRequirement(privileged, "autoremove", 200));
        assertThrows(IllegalArgumentException.class, () -> server.getPermissionPower(privileged,"doesnotexist"));
        assertThrows(InsufficientPowerException.class, () -> server.getPermissionPower(noRights,"autoremove"));
    }

    @Test
    void testSuccessfulSetGet() {
        try {
            int pwr = 99;
            for (String permission : permissions) {
                server.setPermissionsPowerRequirement(privileged, permission, pwr);
                assertEquals(pwr, server.getPermissionPower(privileged, permission));
                pwr--;
            }
        } catch (InsufficientPowerException e) {
            fail(e);
        }
    }

    @Test
    void testGetList() {
        assertArrayEquals(permissions, server.getPermissionList());
    }
}

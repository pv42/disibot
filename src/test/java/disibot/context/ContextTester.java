package disibot.context;

import disibot.util.Command;
import fakejda.FakeGuild;
import fakejda.FakeJDA;
import fakejda.FakeMember;
import fakejda.FakeMessage;
import fakejda.FakeMessageEvent;
import fakejda.FakePrivateChannel;
import fakejda.FakeUser;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import static org.junit.jupiter.api.Assertions.*;

public class ContextTester {
    private ContextStorage storage;
    private ContextCommandHandler handler;
    private FakeJDA jda;

    private static final String HELP = "```!context get|help|set [SERVERNAME]\n" +
            "get    gets the current channels server context\n" +
            "help   displays this help\n" +
            "set [SERVERNAME]\n" +
            "       sets the current context to the server specified, the sender must be a member of this server and " +
            "have the permission to set context in primate channels, if no servername is specified the context is " +
            "removed from the current channel```";


    @BeforeEach
    void prepare() throws IOException {
        if (Files.exists(Path.of("Server.0.json"))) {
            Files.delete(Path.of("Server.0.json"));
        }
        if (Files.exists(Path.of("Server.1.json"))) {
            Files.delete(Path.of("Server.1.json"));
        }
        storage = new ContextStorage();
        jda = new FakeJDA();
        handler = new ContextCommandHandler(storage, jda);
    }

    @AfterAll
    static void cleanUp() throws IOException {
        if (Files.exists(Path.of("Server.0.json"))) {
            Files.delete(Path.of("Server.0.json"));
        }
        if (Files.exists(Path.of("Server.1.json"))) {
            Files.delete(Path.of("Server.1.json"));
        }
        if (Files.exists(Path.of("Server.2.json"))) {
            Files.delete(Path.of("Server.2.json"));
        }
    }

    @Test
    void testHandle() {
        // setup
        FakePrivateChannel channel = new FakePrivateChannel();
        FakeMessage message = new FakeMessage("!context set Server01", channel);
        FakeGuild server01 = new FakeGuild("Server01", 2);
        FakeGuild otherServer01 = new FakeGuild("Server01", 1);
        FakeMember member = new FakeMember(server01, new FakeUser(10));
        server01.setOwner(member);
        MessageReceivedEvent event = new FakeMessageEvent(message, member);
        // handle get (not set)
        int msgIndex = 0;
        handler.handleCommand(new Command("context get"), event);
        assertEquals("There is not context set for this channel.", channel.getMessages().get(msgIndex++).getContentRaw());
        // handle context set invalid
        handler.handleCommand(new Command("context set Server01"), event);
        assertEquals("You don't seem to be a member of a server called 'Server01'.", channel.getMessages().get(msgIndex++).getContentRaw());
        // handle context remove (not set)
        handler.handleCommand(new Command("context set"), event);
        assertEquals("Failed to remove the context of this channel: no context defined", channel.getMessages().get(msgIndex++).getContentRaw());
        // handle context set
        jda.addGuild(server01);
        assertFalse(storage.getData().containsKey(channel));
        handler.handleCommand(new Command("context set Server01"), event);
        assertEquals("Context of this channel set to 'Server01'.", channel.getMessages().get(msgIndex++).getContentRaw());
        assertEquals(server01, storage.getData().get(channel));
        // handle get (set)
        handler.handleCommand(new Command("context get"), event);
        assertEquals("The context of this channel is set to the server 'Server01'.", channel.getMessages().get(msgIndex++).getContentRaw());
        // handle context remove (set)
        handler.handleCommand(new Command("context set"), event);
        assertEquals("The context of this channel has been removed.", channel.getMessages().get(msgIndex++).getContentRaw());
        assertFalse(storage.getData().containsKey(channel));
        // set with multiple matches
        jda.addGuild(otherServer01);
        handler.handleCommand(new Command("context set Server01"), event);
        assertEquals("You seem to be a member of multiple servers called 'Server01'.", channel.getMessages().get(msgIndex++).getContentRaw());
        assertFalse(storage.getData().containsKey(channel));
        // handle no subcommand
        handler.handleCommand(new Command("context "), event);
        assertEquals("Invalid command syntax try !context help.", channel.getMessages().get(msgIndex++).getContentRaw());
        // handle invalid subcommand
        handler.handleCommand(new Command("context error_sub_cmd"), event);
        assertEquals("Invalid command syntax try !context help.", channel.getMessages().get(msgIndex++).getContentRaw());
        // handle help
        String helpStrExpected = "```!context get|help|set [SERVERNAME]\nget    gets the current channels server context\nhelp   displays this help\nset [SERVERNAME]\n       sets the current context to the server specified, the sender must be a member of this server and have the permission to set context in primate channels, if no servername is specified the context is removed from the current channel```";
        handler.handleCommand(new Command("context help"), event);
        assertEquals(helpStrExpected, channel.getMessages().get(msgIndex++).getContentRaw());
    }

    @Test
    void testHandleHelp() {
        FakePrivateChannel channel = new FakePrivateChannel();
        FakeMessage message = new FakeMessage("!context help", channel);
        FakeGuild server01 = new FakeGuild("Server01");
        FakeMember member = new FakeMember(server01, new FakeUser(0));
        MessageReceivedEvent event = new FakeMessageEvent(message, member);
        jda.addGuild(server01);
        handler.handleCommand(new Command("context help"), event);
        assertEquals(HELP, channel.getMessages().get(0).getContentRaw());
    }

    @Test
    void testCanHandle() {
        assertTrue(handler.canHandleCommand(new Command("context"), null));
        assertTrue(handler.canHandleCommand(new Command(" context"), null));
        assertTrue(handler.canHandleCommand(new Command("context set "), null));
        assertFalse(handler.canHandleCommand(new Command("contert"), null));
        assertFalse(handler.canHandleCommand(new Command(""), null));
        assertTrue(handler.canHandleCommand(new Command("\ncontext"), null));
    }
}

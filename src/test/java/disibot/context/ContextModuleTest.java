package disibot.context;

import disibot.command.CommandHandler;
import disibot.modules.DisiBotModule;
import disibot.modules.DisiBotModuleInterface;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.eq;


public class ContextModuleTest {
    @Test
    void test() {
        DisiBotModule module = new ContextModuleFactory().initiateModule(null, null, null, null, null, null, null, null);
        DisiBotModuleInterface moduleInterface = mock(DisiBotModuleInterface.class);
        final boolean[] commandHandlerAdded = {false};
        doAnswer((arg) -> commandHandlerAdded[0] = true).when(moduleInterface).
                addCommandHandler(eq(module), any(CommandHandler.class));
        module.load(moduleInterface);
        assertTrue(commandHandlerAdded[0]);
    }
}

package disibot.antidel;

import fakejda.*;
import net.dv8tion.jda.api.entities.MessageReaction;
import net.dv8tion.jda.api.events.message.guild.react.GuildMessageReactionRemoveEmoteEvent;
import net.dv8tion.jda.api.events.message.guild.react.GuildMessageReactionRemoveEvent;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class AntiDelListenerTest {

    private AntiDelEventListener listener = new AntiDelEventListener();
    private FakeJDA jda;
    private FakeGuild guild;
    private long userId;
    private FakeUser user;
    private FakeMember member;
    private FakeTextChannel channel;
    private FakeMessage message;
    @BeforeEach
    void setup() {
        jda = new FakeJDA();
        guild = new FakeGuild(jda,"fg0", 0);
        userId= 0x10;
        user = new FakeUser(userId);
        member = new FakeMember(guild, user);
        channel = new FakeTextChannel(guild);
        guild.setSelfMember(member);
        message = new FakeMessage("fake_test_message", channel);
        channel.sendMessage(message).complete();
    }
    @Test
    void testEmote() {
        FakeEmote emote = new FakeEmote("fakeemote", "/dev/null");
        MessageReaction.ReactionEmote reactionEmote = mock(MessageReaction.ReactionEmote.class);
        when(reactionEmote.isEmote()).thenReturn(true);
        when(reactionEmote.getEmote()).thenReturn(emote);
        MessageReaction messageReaction = new MessageReaction(channel, reactionEmote, message.getIdLong(), true, 1);
        GuildMessageReactionRemoveEvent ev =
                new GuildMessageReactionRemoveEvent(jda, 0, member, messageReaction, userId);
        listener.onGuildMessageReactionRemove(ev);
        List<MessageReaction> reactions = message.getReactions();
        assertEquals(1, reactions.size());
        assertEquals(emote, reactions.get(0).getReactionEmote().getEmote());
    }

    @Test
    void testEmoji() {
        String emoji = "\uFE0F";
        MessageReaction.ReactionEmote reactionEmote = mock(MessageReaction.ReactionEmote.class);
        when(reactionEmote.isEmoji()).thenReturn(true);
        when(reactionEmote.getEmoji()).thenReturn(emoji);
        MessageReaction messageReaction = new MessageReaction(channel, reactionEmote, message.getIdLong(), true, 1);
        GuildMessageReactionRemoveEvent ev =
                new GuildMessageReactionRemoveEvent(jda, 0, member, messageReaction, userId);
        listener.onGuildMessageReactionRemove(ev);
        List<MessageReaction> reactions = message.getReactions();
        assertEquals(1, reactions.size());
        assertEquals(emoji, reactions.get(0).getReactionEmote().getEmoji());
    }

    @Test
    void testOthersReaction() {
        String emoji = "\uFE0F";
        MessageReaction.ReactionEmote reactionEmote = mock(MessageReaction.ReactionEmote.class);
        when(reactionEmote.isEmoji()).thenReturn(true);
        when(reactionEmote.getEmoji()).thenReturn(emoji);
        MessageReaction messageReaction = new MessageReaction(channel, reactionEmote, message.getIdLong(), false, 1);
        GuildMessageReactionRemoveEvent ev =
                new GuildMessageReactionRemoveEvent(jda, 0, member, messageReaction, userId);
        listener.onGuildMessageReactionRemove(ev);
        List<MessageReaction> reactions = message.getReactions();
        assertEquals(0, reactions.size());
    }
}

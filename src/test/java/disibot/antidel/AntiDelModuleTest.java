package disibot.antidel;

import disibot.coinflip.CoinflipModuleFactory;
import disibot.command.CommandHandler;
import disibot.modules.DisiBotModule;
import disibot.modules.DisiBotModuleInterface;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;

public class AntiDelModuleTest {
    @Test
    void test() {
        DisiBotModule module = new AntiDelModuleFactory().initiateModule(null, null, null, null, null, null, null, null);
        DisiBotModuleInterface moduleInterface = mock(DisiBotModuleInterface.class);
        final boolean[] eventListenerOk = {false};
        doAnswer((arg) -> eventListenerOk[0] = true).when(moduleInterface).addDiscordListenerAdapter(eq(module), any(AntiDelEventListener.class));
        module.load(moduleInterface);
        assertTrue(eventListenerOk[0]);
    }
}
